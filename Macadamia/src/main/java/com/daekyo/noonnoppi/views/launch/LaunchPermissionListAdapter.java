package com.daekyo.noonnoppi.views.launch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.daekyo.noonnoppi.R;

import java.util.ArrayList;

public class LaunchPermissionListAdapter extends BaseAdapter {

    private static final String TAG = LaunchPermissionListAdapter.class.getSimpleName();

    private static final int ITEM_VIEW_TYPE_REQUIRED = 0;
    private static final int ITEM_VIEW_TYPE_OPTIONAL = 1;

    private final Context context;
    private final ArrayList<String> permissionCells;
    private final LayoutInflater layoutInflater;

    public LaunchPermissionListAdapter(Context context, ArrayList<String> list) {
        this.context = context;
        this.permissionCells = list;
        this.layoutInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return permissionCells.size();
    }

    @Override
    public Object getItem(int position) {
        return permissionCells.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Context context = parent.getContext();

        if (convertView == null) {
            // Data Set(listViewItemList)에서 position 에 위치한 데이터 참조 획득
            String listViewItem = permissionCells.get(position);

            switch (position) {
                case ITEM_VIEW_TYPE_REQUIRED:
                    convertView = layoutInflater.inflate(R.layout.layout_listview_cell_required, parent, false);
                    break;

                case ITEM_VIEW_TYPE_OPTIONAL:
                    convertView = layoutInflater.inflate(R.layout.layout_listview_cell_optional, parent, false);
                    break;
            }
        }
        return convertView;
    }
}
