package com.daekyo.noonnoppi.views.orangedot;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.daekyo.noonnoppi.common.api.MacadamiaApi;
import com.daekyo.noonnoppi.common.api.MacadamiaRequestType;
import com.daekyo.noonnoppi.common.dialog.MacadamiaAlertDialog;
import com.daekyo.noonnoppi.common.ui.MacadamiaWebView;
import com.daekyo.noonnoppi.common.utils.MacadamiaScreenUtils;
import com.daekyo.noonnoppi.views.MacadamiaActivity;
import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.nets.sso.appagent.android.AuthCheck;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrangeDotActivity extends MacadamiaActivity {

    // region 100.Variables
    private static final String TAG = OrangeDotActivity.class.getSimpleName();

    private static final int MSG_GET_ORANGEDOT = 0x0100;
    private static final int MSG_ORANGEDOT_PROCESS_DEEP_LINK = 0x0200;

    @BindView(R.id.webview_orangedot)
    MacadamiaWebView orangeDotWebView;
    @BindView(R.id.button_big_orangedot)
    ImageButton buttonBigOrangeDot;
    @BindView(R.id.recycler_orangedot_items)
    RecyclerView orangeDotRecyclerView;
    @BindInt(R.integer.delay_0)
    int DELAY_0;

    private SnapHelper snapHelper;
    private OrangeDotAdapter orangeDotAdapter;
    private ArrayList<JSONObject> orangeDotItems;
    private int requestCode = 0;

    private static final int[] MSG_ORANGEDOT_LIST = {
            MSG_GET_ORANGEDOT
    };

    private int addItemCount = 1;

    // endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orangedot);
        // set ApplicationContext
        ctx = getApplicationContext();
        // bind ButterKnife
        unbinder = ButterKnife.bind(this);
        // Screen 초기화
        initScreenView();
        // SSO
        authCheck = new AuthCheck();
        // orangedot arraylist
        orangeDotItems = new ArrayList<>();
        orangeDotItems.add(getAddItem());
        // get RequestCode
        requestCode = getIntent().getIntExtra(Macadamia.Request.REQ_CODE, 0);

        orangeDotAdapter = new OrangeDotAdapter(OrangeDotActivity.this, orangeDotItems);
        orangeDotRecyclerView.setHasFixedSize(true);
        orangeDotRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        orangeDotRecyclerView.setAdapter(orangeDotAdapter);

        orangeDotWebView.getSettings().setUserAgentString(Macadamia.Constants.USER_AGENT);
        orangeDotWebView.setWebViewClient(webViewClient);
        orangeDotWebView.setWebChromeClient(webChromeClient);
        orangeDotWebView.setOnLongClickListener(v -> (true));
        orangeDotWebView.loadUrl(Macadamia.Urls.WEB_URL_SEARCH);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.t(TAG).d("[onStart]");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.t(TAG).d("[onResume]");
        // reload data
        orangeDotAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.t(TAG).d("[onDestroy]");

        // ButterKnife Unbind
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    /**
     * 화면 재설정
     */
    private void initScreenView() {
        // StatusBar Background Color
//        MacadamiaScreenUtils.changeStatusBarBackgroundColor(MainActivity.this,
//                getResources().getColor(getResources().getIdentifier("colorFFFFFF_100.0", "color", getPackageName()), null));

        // StatusBarTextColor -> Dark
        MacadamiaScreenUtils.changeStatusBarTextColor(OrangeDotActivity.this, true);
        // NavigationBar 있는 경우 S10+
        if (MacadamiaScreenUtils.hasNavigationBar(ctx)) {
            // NavigationBar Icon Color -> Dark
            MacadamiaScreenUtils.changeNavigationBarIconColor(ctx, OrangeDotActivity.this, false);
            // NavigationBar Color
            MacadamiaScreenUtils.changeNavigationBarBackgroundColor(OrangeDotActivity.this,
                    getResources().getColor(getResources().getIdentifier("color373737_100.0", "color", getPackageName()), null));
        }
    }

    @Override
    public void handleEventMessage(@NonNull Message msg) {
        super.handleEventMessage(msg);

        switch (msg.what) {
            case MSG_GET_ORANGEDOT:
                getOrangeDot();
                break;

            case MSG_ORANGEDOT_PROCESS_DEEP_LINK:
                Logger.t(TAG).d("MSG_MAIN_PROCESS_DEEP_LINK %s", msg);
                Bundle data = msg.getData();
                String linkUrl = data.getString(Macadamia.Keys.LINK_URL);
                int targetId = data.getInt(Macadamia.Keys.PARAM_KEY_TARGET, orangeDotWebView.getId());
                Uri uri = Uri.parse(linkUrl);
                String path = uri.getPath();
                String query = uri.getQuery();
                JSONObject json = JSON.parseObject(query);

                if (path.contains(Macadamia.Urls.URI_PATH_CLOSE)) {
                    boolean reload = json.getBoolean(Macadamia.Keys.PARAM_KEY_RELOAD);
                    String url = StringUtils.defaultIfEmpty(json.getString(Macadamia.Keys.PARAM_KEY_URL), Macadamia.Constants.STRING_BLANK);

                    Intent intent = new Intent();
                    intent.putExtra(Macadamia.Keys.PARAM_KEY_RELOAD, reload);
                    intent.putExtra(Macadamia.Keys.PARAM_KEY_URL, url);
                    setResult(Macadamia.Result.RES_HIDE_ORANGEDOT, intent);
                    finish();
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected void initActivity() {
        super.initActivity();
        Logger.t(TAG).d("- initialize Activity");
        // Activity Event Handler
        eventHandler = new MacaEventHandler(this, MSG_ORANGEDOT_LIST);
        eventHandler.startEvent(null);
        // MacadamiaAPI
        macadamiaApi = new MacadamiaApi(ctx);
    }

    @OnClick(R.id.button_big_orangedot)
    public void onClickButtonBigOrangeDot(View view) {
        setResult(Macadamia.Result.RES_HIDE_ORANGEDOT);
        finish();
        overridePendingTransition(R.anim.view_nothing, R.anim.view_orangedot_hide);
    }

    /**
     * WebViewClient
     */
    private final WebViewClient webViewClient = new WebViewClient() {
        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Logger.t(TAG).d("WebViewClient shouldOverrideUrlLoading request : %s", request.getUrl());
            return handleUrlLoading(view, request.getUrl());
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Logger.t(TAG).d("WebViewClient shouldOverrideUrlLoading url : %s", url);
            return handleUrlLoading(view, Uri.parse(url));
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Logger.t(TAG).d("WebViewClient onPageStarted : %s", url);
            // show loading view
            startLoading(OrangeDotActivity.this, null);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Logger.t(TAG).d("webview onPageFinished %s", url);
            stopLoading();
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
            Logger.t(TAG).d("WebViewClient onLoadResource : %s [%d]", url, url.getBytes().length);
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            Logger.t(TAG).d("WebViewClient onPageCommitVisible : %s", url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                Logger.t(TAG).d("WebViewClient onReceivedError : %d", error.getErrorCode());
            }
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);
            Logger.t(TAG).d("WebViewClient onReceivedError : %d", errorResponse.getStatusCode());
        }

        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            super.doUpdateVisitedHistory(view, url, isReload);
            Logger.t(TAG).d("WebViewClient doUpdateVisitedHistory %s %s", url, isReload);
        }

        @Override
        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
            super.onFormResubmission(view, dontResend, resend);
            Logger.t(TAG).d("WebViewClient onFormResubmission %s %s", dontResend, resend);
        }

        @Override
        public void onScaleChanged(WebView view, float oldScale, float newScale) {
            super.onScaleChanged(view, oldScale, newScale);
            Logger.t(TAG).d("WebViewClient onScaleChanged %f %f", oldScale, newScale);
        }
    };

    // region 500.WebChromeClient
    /**
     * WebChromeClient
     */
    private final WebChromeClient webChromeClient = new WebChromeClient() {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            Logger.t(TAG).d("WebChromeClient onProgressChanged %d", newProgress);
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            Logger.t(TAG).d("WebChromeClient onCreateWindow");
            return false;
        }

        @Override
        public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
            Logger.t(TAG).d("WebChromeClient onJsAlert %s %s", url, message);
            MacadamiaAlertDialog.showAlert(OrangeDotActivity.this,
                    false,
                    getString(R.string.dialog_title_01),
                    message,
                    getString(R.string.dialog_button_ok), result::confirm);
            return true;
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
            Logger.t(TAG).d("WebChromeClient onJsConfirm %s %s", url, message);
            MacadamiaAlertDialog.showConfirm(OrangeDotActivity.this,
                    false,
                    getString(R.string.dialog_title_01),
                    message,
                    getString(R.string.dialog_button_ok), result::confirm,
                    getString(R.string.dialog_button_cancel), result::cancel);
            return true;
        }
    };
    // endregion

    private boolean handleUrlLoading(WebView view, Uri uri) {
        Logger.t(TAG).d("WebViewClient handleUrlLoading %s", uri);
        Logger.t(TAG).d("URI_SCHEME : %s", uri);
        Logger.t(TAG).d("URI_SCHEME Scheme : %s", uri.getScheme());
        Logger.t(TAG).d("URI_SCHEME Host : %s", uri.getHost());
        Logger.t(TAG).d("URI_SCHEME Port : %d", uri.getPort());

        Logger.t(TAG).d("URI_SCHEME LastPathSegment : %s", uri.getLastPathSegment());
        Logger.t(TAG).d("URI_SCHEME PathSegments : %s", uri.getPathSegments());

        Logger.t(TAG).d("URI_SCHEME EncodedAuthority : %s", uri.getEncodedAuthority());
        Logger.t(TAG).d("URI_SCHEME EncodedFragment : %s", uri.getEncodedFragment());
        Logger.t(TAG).d("URI_SCHEME EncodedPath : %s", uri.getEncodedPath());
        Logger.t(TAG).d("URI_SCHEME EncodedQuery : %s", uri.getEncodedQuery());
        Logger.t(TAG).d("URI_SCHEME EncodedSchemeSpecificPart : %s", uri.getEncodedSchemeSpecificPart());
        Logger.t(TAG).d("URI_SCHEME EncodedUserInfo : %s", uri.getEncodedUserInfo());

        Logger.t(TAG).d("URI_SCHEME Authority : %s", uri.getAuthority());
        Logger.t(TAG).d("URI_SCHEME Fragment : %s", uri.getFragment());
        Logger.t(TAG).d("URI_SCHEME Path : %s", uri.getPath());
        Logger.t(TAG).d("URI_SCHEME Query : %s", uri.getQuery());
        Logger.t(TAG).d("URI_SCHEME SchemeSpecificPart : %s", uri.getSchemeSpecificPart());
        Logger.t(TAG).d("URI_SCHEME UserInfo : %s", uri.getUserInfo());

        if (uri != null) {
            final String URI_SCHEME = uri.getScheme();
            if (URI_SCHEME.equals("http") || URI_SCHEME.equals("https") || URI_SCHEME.equals("javascript")) {
                view.loadUrl(uri.toString());
                return false;
            } else if (URI_SCHEME.equals("macadamia")) {
                return handleMacadamiaUrlScheme(view, uri);
            }
        } else {
            return true;
        }
        return false;
    }

    /**
     * Custom URL Scheme 처리
     *
     * @param view WebView
     * @param uri  Uri
     * @return true / false
     */
    private boolean handleMacadamiaUrlScheme(@NonNull WebView view, @NonNull Uri uri) {
        Bundle urlSchemeBundle = new Bundle();
        urlSchemeBundle.putString(Macadamia.Keys.LINK_URL, uri.toString());
        urlSchemeBundle.putInt(Macadamia.Keys.PARAM_KEY_TARGET, view.getId());

        Message urlSchemeMessage = new Message();
        urlSchemeMessage.setData(urlSchemeBundle);
        eventHandler.processEventById(MSG_ORANGEDOT_PROCESS_DEEP_LINK, urlSchemeMessage, DELAY_0);
        return true;
    }
    // endregion

    /**
     * return result
     *
     * @param resultCode Result Code
     * @param data       Result Data
     */
    public void setResultToMainActivity(int resultCode, Intent data) {
        Logger.t(TAG).d("close %d %s", resultCode, data);
        setResult(resultCode, data);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Logger.t(TAG).d("onActivityResult %X %X %s", requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Macadamia.Request.REQ_SHOW_ORANGEDOT_SETTING) {
            if (resultCode == Macadamia.Result.RES_HIDE_ORANGEDOT_SETTING) {
                Logger.t(TAG).d("HIDE ORANGEDOT SETTING");
            } else if (resultCode == Macadamia.Result.RES_RELOAD_ITEMS) {
                getOrangeDot();
            }
        }
    }

    public void openOrangeDotSetting() {
        if (Macadamia.Constants.LOGGED && authCheck != null) {
            Intent intent = new Intent();
            intent.setClass(OrangeDotActivity.this, OrangeDotSettingActivity.class);
            intent.putExtra(Macadamia.Request.REQ_CODE, Macadamia.Request.REQ_SHOW_ORANGEDOT_SETTING);
            startActivityForResult(intent, Macadamia.Request.REQ_SHOW_ORANGEDOT_SETTING);
            overridePendingTransition(R.anim.view_orangedot_show, R.anim.view_nothing);
        } else {
            MacadamiaAlertDialog.showAlert(OrangeDotActivity.this,
                    false,
                    getString(R.string.dialog_title_01),
                    getString(R.string.dialog_message_orangedot_need_to_login),
                    getString(R.string.dialog_button_ok),
                    () -> {
                    });
        }
    }


    @OnClick(R.id.button_orangedot_home)
    public void onClickButtonMain(View view) {
        Intent param = new Intent();
        param.putExtra(Macadamia.Keys.PARAM_KEY_URL, Macadamia.Urls.WEB_URL_MAIN);
        setResultToMainActivity(Macadamia.Result.RES_LOAD_URL, param);
    }

    @OnClick(R.id.button_orangedot_myservice)
    public void onClickButtonMyService(View view) {
        Intent param = new Intent();
        param.putExtra(Macadamia.Keys.PARAM_KEY_URL, Macadamia.Urls.WEB_URL_MY_SERVICE);
        setResultToMainActivity(Macadamia.Result.RES_LOAD_URL, param);
    }

    /**
     * API get OrangeDot
     */
    public void getOrangeDot() {
        orangeDotItems = new ArrayList<>();
        // 로그인한 경우에만 추가
        if (Macadamia.Constants.LOGGED) {
            orangeDotItems.add(getAddItem());
        }

        try {
            HashMap<String, String> headerMap = new HashMap<>();
            headerMap.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
            headerMap.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);
            headerMap.putAll(getSSOHeader(getDeviceHeader(authCheck)));

            JSONObject param = new JSONObject();
            param.put(Macadamia.Keys.PARAM_KEY_APP_ID, Macadamia.Constants.APP_ID);
            param.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
            param.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);

            macadamiaApi.request(MacadamiaRequestType.REQUEST_ORANGE_DOT,
                    macadamiaApi.getHeaderMap(headerMap),
                    param,
                    orangeDotCallback);
        } catch (Exception e) {
            Logger.t(TAG).e("Request User Info Exception", e);
            networkErrorAlertDialog(OrangeDotActivity.this,
                    getString(R.string.dialog_button_ok),
                    () -> {
                    });
        }
    }

    /**
     * API OrangeDot Callback
     */
    private final Callback<JSONObject> orangeDotCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(@NonNull Call<JSONObject> call, @NonNull Response<JSONObject> response) {
            Logger.t(TAG).d("API GET_ORANGE_DOT RESP %s", response);
            if (response.isSuccessful()) {
                JSONObject data = response.body().getJSONObject(Macadamia.Keys.PARAM_KEY_DATA);
                if (data != null) {
                    JSONObject orangeDot = data.getJSONObject(Macadamia.Keys.PARAM_KEY_ORANGEDOT);
                    JSONArray menu = orangeDot.getJSONArray(Macadamia.Keys.PARAM_KEY_MENU);
                    JSONArray myservice = orangeDot.getJSONArray(Macadamia.Keys.PARAM_KEY_MYSERVICE);
                    // 로그인한 경우에만 추가 버튼 생성
                    addItemCount = Macadamia.Constants.LOGGED ? 1 : 0;

                    // menu
                    for (Object obj : menu) {
                        if (obj instanceof JSONObject) {
                            JSONObject item = (JSONObject) obj;
                            if (item.getBoolean(Macadamia.Keys.PARAM_KEY_ADDED)) {
                                orangeDotItems.add(orangeDotItems.size() - addItemCount, item);
                            }
                        }
                    }
                    // myservice
                    for (Object obj : myservice) {
                        if (obj instanceof JSONObject) {
                            JSONObject item = (JSONObject) obj;
                            if (item.getBoolean(Macadamia.Keys.PARAM_KEY_ADDED)) {
                                orangeDotItems.add(orangeDotItems.size() - addItemCount, item);
                            }
                        }
                    }
                }
                // sort by myorder ascending
                Collections.sort(orangeDotItems, new OrangeDotOrderComparator(Macadamia.Keys.PARAM_KEY_MY_ORDER));
                // 퀵메뉴 0개인 경우
                if (orangeDotItems.size() == 1) {
                    MacadamiaAlertDialog.showConfirm(OrangeDotActivity.this,
                            false,
                            getString(R.string.dialog_title_03),
                            getString(R.string.dialog_message_orangedot_no_data),
                            getString(R.string.dialog_button_default_quick),
                            () -> orangeDotAdapter.update(orangeDotItems),
                            getString(R.string.dialog_button_close),
                            () -> {
                            });
                } else {
                    orangeDotAdapter.update(orangeDotItems);
                }
            } else {
                Logger.t(TAG).e("API GET_ORANGE_DOT ERR");
                dataLoadingErrorAlertDialog(OrangeDotActivity.this,
                        getString(R.string.dialog_button_ok),
                        () -> {
                        });
            }
        }

        @Override
        public void onFailure(@NonNull Call<JSONObject> call, Throwable t) {
            Logger.t(TAG).e("[FAILURE] %s", t.getMessage());
            networkErrorAlertDialog(OrangeDotActivity.this,
                    getString(R.string.dialog_button_close),
                    () -> {
                    });
        }
    };

    /**
     * 추가하기 버튼
     *
     * @return add button json data
     */
    private JSONObject getAddItem() {
        JSONObject item = new JSONObject();
        item.put(Macadamia.Keys.PARAM_KEY_ORDER, 999);
        item.put(Macadamia.Keys.PARAM_KEY_CODE, "add");
        item.put(Macadamia.Keys.PARAM_KEY_TITLE, "추가하기");
        item.put(Macadamia.Keys.PARAM_KEY_MY_ORDER, 999);
        item.put(Macadamia.Keys.PARAM_KEY_ADDED, true);
        item.put(Macadamia.Keys.PARAM_KEY_URL, null);
        item.put(Macadamia.Keys.PARAM_KEY_ICON, "icon_add");
        return item;
    }
}
