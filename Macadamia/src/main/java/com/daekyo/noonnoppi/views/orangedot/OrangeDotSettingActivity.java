package com.daekyo.noonnoppi.views.orangedot;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.daekyo.noonnoppi.common.api.MacadamiaApi;
import com.daekyo.noonnoppi.common.api.MacadamiaRequestType;
import com.daekyo.noonnoppi.common.dialog.MacadamiaAlertDialog;
import com.daekyo.noonnoppi.common.utils.MacadamiaScreenUtils;
import com.daekyo.noonnoppi.views.MacadamiaActivity;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.HashMap;

import kr.co.nets.sso.appagent.android.AuthCheck;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrangeDotSettingActivity extends MacadamiaActivity {

    // region 100.Variables
    private static final String TAG = OrangeDotSettingActivity.class.getSimpleName();

    private static final int MSG_ORANGEDOT_SETTING_GET_ORANGEDOT = 0x0401;
    private static final int MSG_ORANGEDOT_SETTING_UPDATE = 0x0402;
    private static final int MSG_ORANGEDOT_SETTING_UPDATE_FINISH = 0x0403;
    private static final int MSG_ORANGEDOT_SETTING_FINISH = 0x0500;
    // StatusBar ConstraintLayout Height
    private ConstraintLayout statusBarLayout;

    private static final int HEADER_COLUMN_SIZE = 1;
    private static final int ITEM_COLUMN_SIZE = 2;

    private int requestCode = 0;
    private JSONObject data;
    private static int itemAddedCount = 0;

    private int DELAY_10;
    private RecyclerView orangeDotSettingRecyclerView;
    private ImageButton buttonClose;
    private Button buttonSave;

    private SnapHelper snapHelper;
    private OrangeDotSettingAdapter orangeDotSettingAdapter;
    private ArrayList<JSONObject> orangeDotSettingItems;
    private ArrayList<JSONObject> tempOrangeDotSettingItems;

    private final int[] MSG_ORANGE_DOT_SETTING_LIST = {
            MSG_ORANGEDOT_SETTING_GET_ORANGEDOT
    };

    private JSONArray myOrangeDots;
    private JSONArray menu;
    private JSONArray myservice;
    // endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orangedot_setting);
        // set ApplicationContext
        ctx = getApplicationContext();
        // Screen 초기화
        initScreenView();
        orangeDotSettingRecyclerView = findViewById(R.id.recycler_orangedot_setting_items);
        buttonClose = findViewById(R.id.button_orangedot_setting_close);
        buttonSave = findViewById(R.id.button_orangedot_setting_save);
        buttonClose.setOnClickListener(buttonListener);
        buttonSave.setOnClickListener(buttonListener);

        DELAY_10 = getResources().getInteger(R.integer.delay_10);
        // SSO
        authCheck = new AuthCheck();
        // get RequestCode
        requestCode = getIntent().getIntExtra(Macadamia.Request.REQ_CODE, 0);
        data = JSONObject.parseObject(getIntent().getStringExtra(Macadamia.Keys.PARAM_KEY_DATA));

        orangeDotSettingItems = new ArrayList<>();
        orangeDotSettingAdapter = new OrangeDotSettingAdapter(OrangeDotSettingActivity.this, orangeDotSettingItems);
        orangeDotSettingRecyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(ctx, ITEM_COLUMN_SIZE);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return orangeDotSettingAdapter.isHeader(position) ? ITEM_COLUMN_SIZE : HEADER_COLUMN_SIZE;
            }
        });
        orangeDotSettingRecyclerView.setLayoutManager(gridLayoutManager);
        orangeDotSettingRecyclerView.setAdapter(orangeDotSettingAdapter);
        // snap helper
        snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(orangeDotSettingRecyclerView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.t(TAG).d("[onStart]");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.t(TAG).d("[onResume]");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.t(TAG).d("[onDestroy]");
    }

    @Override
    public void handleEventMessage(@NonNull Message msg) {
        switch (msg.what) {
            case MSG_ORANGEDOT_SETTING_GET_ORANGEDOT:
                startLoading(this, null);
                getOrangeDot();
                break;

            case MSG_ORANGEDOT_SETTING_UPDATE:
                updateOrangeDot();
                break;

            case MSG_ORANGEDOT_SETTING_UPDATE_FINISH:
                setResult(Macadamia.Result.RES_RELOAD_ITEMS, null);
                finish();
                break;

            case MSG_ORANGEDOT_SETTING_FINISH:
                setResult(MSG_RELOAD_ORANGEDOT_ITEMS);
                finish();
                break;

            default:
                break;
        }
    }

    // region 300.Activity Delegate
    @Override
    public void initActivity() {
        Logger.t(TAG).d("- initialize Activity");
        // Activity Event Handler
        eventHandler = new MacaEventHandler(this, MSG_ORANGE_DOT_SETTING_LIST);
        eventHandler.startEvent(null);
        // MacadamiaAPI
        macadamiaApi = new MacadamiaApi(ctx);
    }
    // endregion

    /**
     * 화면 재설정
     */
    private void initScreenView() {
        // StatusBar Background Color
//        MacadamiaScreenUtils.changeStatusBarBackgroundColor(MainActivity.this,
//                getResources().getColor(getResources().getIdentifier("colorFFFFFF_100.0", "color", getPackageName()), null));

        // StatusBarTextColor -> Dark
        MacadamiaScreenUtils.changeStatusBarTextColor(OrangeDotSettingActivity.this, true);
        // NavigationBar 있는 경우 S10+
        if (MacadamiaScreenUtils.hasNavigationBar(ctx)) {
            // NavigationBar Icon Color -> Dark
            MacadamiaScreenUtils.changeNavigationBarIconColor(ctx, OrangeDotSettingActivity.this, false);
            // NavigationBar Color
            MacadamiaScreenUtils.changeNavigationBarBackgroundColor(OrangeDotSettingActivity.this,
                    getResources().getColor(getResources().getIdentifier("color373737_100.0", "color", getPackageName()), null));
        }
    }

    final View.OnClickListener buttonListener = (v -> {

        if (v.getId() == R.id.button_orangedot_setting_close) {
            eventHandler.processEventById(MSG_ORANGEDOT_SETTING_FINISH, null, DELAY_10);
        } else if (v.getId() == R.id.button_orangedot_setting_save) {
            if (getItemAddedCount() < 14) {
                eventHandler.processEventById(MSG_ORANGEDOT_SETTING_UPDATE, null, DELAY_10);
            } else {
                MacadamiaAlertDialog.showAlert(OrangeDotSettingActivity.this,
                        false,
                        getString(R.string.dialog_title_03),
                        getString(R.string.dialog_message_orangedot_limit),
                        getString(R.string.dialog_button_ok),
                        () -> {
                        });
            }
        }
    });

    /**
     * API get OrangeDot
     */
    private void getOrangeDot() {
        try {
            HashMap<String, String> headerMap = new HashMap<>();
            headerMap.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
            headerMap.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);
            headerMap.putAll(getSSOHeader(getDeviceHeader(authCheck)));

            JSONObject param = new JSONObject();
            param.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
            param.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);
            param.put(Macadamia.Keys.PARAM_KEY_APP_ID, Macadamia.Constants.APP_ID);

            macadamiaApi.request(MacadamiaRequestType.REQUEST_ORANGE_DOT,
                    macadamiaApi.getHeaderMap(headerMap),
                    param,
                    orangeDotCallback);

            myOrangeDots = new JSONArray();
            tempOrangeDotSettingItems = new ArrayList<>();
        } catch (Exception e) {
            Logger.t(TAG).e("Request User Info Exception", e);
            networkErrorAlertDialog(OrangeDotSettingActivity.this,
                    getString(R.string.dialog_button_ok),
                    () -> {
                    });
        }
    }

    /**
     * API OrangeDot Callback
     */
    private final Callback<JSONObject> orangeDotCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(@NonNull Call<JSONObject> call, @NonNull Response<JSONObject> response) {
            Logger.t(TAG).d("API GET_ORANGE_DOT RESP %s", response);
            if (response.isSuccessful()) {
                JSONObject data = response.body().getJSONObject(Macadamia.Keys.PARAM_KEY_DATA);
                int menuCount = data.getInteger(Macadamia.Keys.PARAM_KEY_MENU_ADDED_COUNT);
                int myServiceCount = data.getInteger(Macadamia.Keys.PARAM_KEY_MYSERVICE_ADDED_COUNT);

                if (data != null) {
                    JSONObject orangeDot = data.getJSONObject(Macadamia.Keys.PARAM_KEY_ORANGEDOT);
                    menu = orangeDot.getJSONArray(Macadamia.Keys.PARAM_KEY_MENU);
                    myservice = orangeDot.getJSONArray(Macadamia.Keys.PARAM_KEY_MYSERVICE);

                    orangeDotSettingItems = new ArrayList<>();
                    // menu
                    orangeDotSettingItems.add(getSectionHeader(OrangeDotSettingHeaderType.MENU));
                    for (Object obj : menu) {
                        if (obj instanceof JSONObject) {
                            JSONObject item = (JSONObject) obj;
                            if (item.getBoolean(Macadamia.Keys.PARAM_KEY_ADDED)) {
                                itemAddedCount++;
                            }
                            item.put(Macadamia.Keys.PARAM_KEY_TYPE, OrangeDotSettingHeaderType.MENU);
                            orangeDotSettingItems.add(item);
                        }
                    }
                    // my service
                    orangeDotSettingItems.add(getSectionHeader(OrangeDotSettingHeaderType.MY_SERVICE));
                    for (Object obj : myservice) {
                        if (obj instanceof JSONObject) {
                            JSONObject item = (JSONObject) obj;
                            if (item.getBoolean(Macadamia.Keys.PARAM_KEY_ADDED)) {
                                itemAddedCount++;
                            }
                            item.put(Macadamia.Keys.PARAM_KEY_TYPE, OrangeDotSettingHeaderType.MY_SERVICE);
                            orangeDotSettingItems.add(item);
                        }
                    }
                    tempOrangeDotSettingItems = orangeDotSettingItems;
                }
            } else {
                Logger.t(TAG).e("API GET_ORANGE_DOT ERR");
                dataLoadingErrorAlertDialog(OrangeDotSettingActivity.this,
                        getString(R.string.dialog_button_ok),
                        () -> {
                        });
            }

            ArrayList<JSONObject> list = new ArrayList<>();
            orangeDotSettingAdapter.update(orangeDotSettingItems);
            stopLoading();
        }

        @Override
        public void onFailure(@NonNull Call<JSONObject> call, Throwable t) {
            Logger.t(TAG).e("[FAILURE] %s", t.getMessage());
            networkErrorAlertDialog(OrangeDotSettingActivity.this,
                    getString(R.string.dialog_button_close),
                    () -> {
                    });
        }
    };

    private void updateOrangeDot() {
        try {
            HashMap<String, String> headerMap = new HashMap<>();
            headerMap.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
            headerMap.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);
            headerMap.putAll(getSSOHeader(getDeviceHeader(authCheck)));

            JSONObject orangedot = new JSONObject();
            JSONArray menu = new JSONArray();
            JSONArray myservice = new JSONArray();
            int menuCount = 0;
            int myServiceCount = 0;

            for (JSONObject item : tempOrangeDotSettingItems) {
                if (item.containsKey(Macadamia.Keys.PARAM_KEY_TYPE)) {
                    OrangeDotSettingHeaderType type = (OrangeDotSettingHeaderType) item.get(Macadamia.Keys.PARAM_KEY_TYPE);

                    if (type == OrangeDotSettingHeaderType.MENU) {
                        menu.add(item);
                        if (item.getBoolean(Macadamia.Keys.PARAM_KEY_ADDED)) {
                            menuCount++;
                        }
                    } else if (type == OrangeDotSettingHeaderType.MY_SERVICE) {
                        myservice.add(item);
                        if (item.getBoolean(Macadamia.Keys.PARAM_KEY_ADDED)) {
                            myServiceCount++;
                        }
                    }
                }
            }

            orangedot.put(Macadamia.Keys.PARAM_KEY_MENU_ADDED_COUNT, menuCount);
            orangedot.put(Macadamia.Keys.PARAM_KEY_MYSERVICE_ADDED_COUNT, myServiceCount);
            orangedot.put(Macadamia.Keys.PARAM_KEY_MENU, menu);
            orangedot.put(Macadamia.Keys.PARAM_KEY_MYSERVICE, myservice);

            JSONObject param = new JSONObject();
            param.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
            param.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);
            param.put(Macadamia.Keys.PARAM_KEY_ORANGEDOT, orangedot);

            macadamiaApi.request(MacadamiaRequestType.UPDATE_ORANGE_DOT,
                    macadamiaApi.getHeaderMap(headerMap),
                    param,
                    updateOrangeDotCallback);
        } catch (Exception e) {
            Logger.t(TAG).e("Update OrangeDot Exception", e);
            networkErrorAlertDialog(OrangeDotSettingActivity.this,
                    getString(R.string.dialog_button_ok),
                    () -> {
                    });
        }
    }

    private final Callback<JSONObject> updateOrangeDotCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(@NonNull Call<JSONObject> call, Response<JSONObject> response) {
            if (response.isSuccessful()) {
                JSONObject data = response.body().getJSONObject(Macadamia.Keys.PARAM_KEY_DATA);
                eventHandler.processEventById(MSG_ORANGEDOT_SETTING_UPDATE_FINISH, null, DELAY_10);
            } else {
                Logger.t(TAG).e("API UPDATE_ORANGE_DOT ERR");
                dataLoadingErrorAlertDialog(OrangeDotSettingActivity.this,
                        getString(R.string.dialog_button_ok),
                        () -> {
                        });
            }
        }

        @Override
        public void onFailure(@NonNull Call<JSONObject> call, Throwable t) {
            Logger.t(TAG).e("[FAILURE] %s", t.getMessage());
            networkErrorAlertDialog(OrangeDotSettingActivity.this,
                    getString(R.string.dialog_button_close),
                    () -> {
                    });
        }
    };

    private JSONObject getSectionHeader(OrangeDotSettingHeaderType type) {
        JSONObject header = new JSONObject();
        String title = (OrangeDotSettingHeaderType.MENU == type) ? "Menu" : "My Service";
        header.put(Macadamia.Keys.PARAM_KEY_TITLE, title);
        header.put(Macadamia.Keys.PARAM_KEY_HEADER, true);
        return header;
    }

    public void addItem(OrangeDotSettingHeaderType type, String itemCode, boolean checked) {
        Logger.t(TAG).d("item code : %s %s %s", type, itemCode, checked);
        int idx = 0;
        for (JSONObject item : orangeDotSettingItems) {
            if (!item.containsKey(Macadamia.Keys.PARAM_KEY_HEADER)) {
                if (item.getString(Macadamia.Keys.PARAM_KEY_CODE).equals(itemCode)) {
                    item.put(Macadamia.Keys.PARAM_KEY_ADDED, checked);
                    if (checked) {
                        item.put(Macadamia.Keys.PARAM_KEY_MY_ORDER, ++itemAddedCount);
                    } else {
                        item.put(Macadamia.Keys.PARAM_KEY_MY_ORDER, 0);
                    }
                }
            }
            tempOrangeDotSettingItems.set(idx, item);
            idx++;
        }
    }

    private int getItemAddedCount() {
        int count = 0;
        for (JSONObject item : tempOrangeDotSettingItems) {
            if (item.containsKey(Macadamia.Keys.PARAM_KEY_ADDED)) {
                if (item.getBoolean(Macadamia.Keys.PARAM_KEY_ADDED)) {
                    count++;
                }
            }
        }
        return count;
    }
}
