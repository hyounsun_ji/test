package com.daekyo.noonnoppi.views;

import android.os.Message;

public interface MacaEventDelegate {
    /**
     * Handle Event Message
     *
     * @param msg Message
     */
    void handleEventMessage(Message msg);
}
