package com.daekyo.noonnoppi.views.orangedot;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONObject;
import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;

public class OrangeDotSettingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String TAG = OrangeDotSettingAdapter.class.getSimpleName();
    private ArrayList<JSONObject> items;
    private final Context context;

    private final int TYPE_HEADER = 0;
    private final int TYPE_ITEM = 1;
    private boolean showVerticalLine = false;

    public OrangeDotSettingAdapter(@NonNull Context context, @NonNull ArrayList<JSONObject> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder holder;
        if (viewType == TYPE_HEADER) {
            // header
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_orangedot_setting_header, parent, false);
            // RecyclerView item width
            // https://stackoverflow.com/questions/30691150/match-parent-width-does-not-work-in-recyclerview
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            view.setLayoutParams(lp);
            holder = new OrangeDotSettingHeaderHolder(view);
        } else {
            // item
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_orangedot_setting_item, parent, false);
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            view.setLayoutParams(lp);
            holder = new OrangeDotSettingItemHolder(view);
        }
        return holder;
    }

    /**
     * whether item is header or not
     *
     * @param position item poistion
     * @return if item is header true or false
     */
    public boolean isHeader(int position) {
        JSONObject item = items.get(position);
        return item.containsKey(Macadamia.Keys.PARAM_KEY_HEADER) && item.getBoolean(Macadamia.Keys.PARAM_KEY_HEADER);
    }

    /**
     * update Adapter
     */
    public void update(ArrayList<JSONObject> list) {
        Logger.t(TAG).d("adapter update : %s", list);
        this.items = list;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        JSONObject item = items.get(position);
        Logger.t(TAG).e("item setting : %d %s", position, item);

        if (holder instanceof OrangeDotSettingItemHolder) {
            ((OrangeDotSettingItemHolder) holder).order = item.getInteger(Macadamia.Keys.PARAM_KEY_ORDER);
            ((OrangeDotSettingItemHolder) holder).code = item.getString(Macadamia.Keys.PARAM_KEY_CODE);
            ((OrangeDotSettingItemHolder) holder).itemTitle.setText(item.getString(Macadamia.Keys.PARAM_KEY_TITLE));
            ((OrangeDotSettingItemHolder) holder).url = item.getString(Macadamia.Keys.PARAM_KEY_URL);
            ((OrangeDotSettingItemHolder) holder).iconUrl = item.getString(Macadamia.Keys.PARAM_KEY_ICON);
            ((OrangeDotSettingItemHolder) holder).itemChecked = item.getBoolean(Macadamia.Keys.PARAM_KEY_ADDED);
            ((OrangeDotSettingItemHolder) holder).myorder = item.getInteger(Macadamia.Keys.PARAM_KEY_MY_ORDER);
            ((OrangeDotSettingItemHolder) holder).checkBoxItem.setChecked(item.getBoolean(Macadamia.Keys.PARAM_KEY_ADDED));
            showVerticalLine = !showVerticalLine;
            ((OrangeDotSettingItemHolder) holder).vLine.setVisibility(showVerticalLine ? View.VISIBLE : View.INVISIBLE);
            ((OrangeDotSettingItemHolder) holder).position = position;
            ((OrangeDotSettingItemHolder) holder).type = (OrangeDotSettingHeaderType) item.get(Macadamia.Keys.PARAM_KEY_TYPE);
        } else if (holder instanceof OrangeDotSettingHeaderHolder) {
            showVerticalLine = false;
            String headerTitle = item.getString(Macadamia.Keys.PARAM_KEY_TITLE);
            Drawable backgroundDrawable = ContextCompat.getDrawable(context, R.drawable.shape_radius_top_f36f21);
            if (headerTitle.equals("My Service")) {
                backgroundDrawable = ContextCompat.getDrawable(context, R.drawable.shape_radius_top_fbb81d);
            }

            ((OrangeDotSettingHeaderHolder) holder).headerLayout.setBackground(backgroundDrawable);
            ((OrangeDotSettingHeaderHolder) holder).headerTitle.setText(headerTitle);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeader(position)) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * Item Holder
     */
    public class OrangeDotSettingItemHolder extends RecyclerView.ViewHolder {
        protected final RecyclerView recyclerViewList;
        protected final ConstraintLayout itemView;
        protected final ConstraintLayout vLine;
        protected final Button buttonItem;
        protected final CheckBox checkBoxItem;
        protected final TextView itemTitle;
        protected int order;
        protected int myorder;
        protected String code;
        protected String url;
        protected String iconUrl;
        protected OrangeDotSettingHeaderType type;
        protected int position;
        protected boolean itemChecked = false;
        protected JSONObject item;

        public OrangeDotSettingItemHolder(View view) {
            super(view);
            this.recyclerViewList = view.findViewById(R.id.recycler_orangedot_setting_items);
            this.itemView = view.findViewById(R.id.layout_orangedot_setting_item);
            this.vLine = view.findViewById(R.id.layout_orangedot_setting_item_vline);
            this.itemTitle = view.findViewById(R.id.textview_orangedot_setting_item_title);
            this.buttonItem = view.findViewById(R.id.button_orangedot_setting_item_add);
            this.checkBoxItem = view.findViewById(R.id.checkbox_orangedot_setting_item_add);

            this.buttonItem.setOnClickListener((v) -> {
                itemChecked = !itemChecked;
                this.checkBoxItem.setChecked(itemChecked);
                ((OrangeDotSettingActivity) context).addItem(type, code, itemChecked);
            });
        }
    }

    /**
     * Header Holder
     */
    public static class OrangeDotSettingHeaderHolder extends RecyclerView.ViewHolder {
        protected final ConstraintLayout headerLayout;
        protected final TextView headerTitle;

        public OrangeDotSettingHeaderHolder(View view) {
            super(view);
            this.headerLayout = view.findViewById(R.id.layout_orangedot_header);
            this.headerTitle = view.findViewById(R.id.text_orangedot_setting_title);
        }
    }
}
