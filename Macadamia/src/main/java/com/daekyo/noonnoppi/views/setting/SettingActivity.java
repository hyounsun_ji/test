package com.daekyo.noonnoppi.views.setting;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.Settings;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import com.alibaba.fastjson.JSONObject;
import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.daekyo.noonnoppi.common.api.MacadamiaApi;
import com.daekyo.noonnoppi.common.api.MacadamiaRequestType;
import com.daekyo.noonnoppi.common.dialog.MacadamiaAlertDialog;
import com.daekyo.noonnoppi.common.ui.MacadamiaWebView;
import com.daekyo.noonnoppi.common.utils.MacadamiaUtils;
import com.daekyo.noonnoppi.views.MacadamiaActivity;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;

import kr.co.nets.sso.appagent.android.AuthCheck;
import kr.co.nets.sso.appagent.android.AuthSuccessResult;
import kr.co.nets.sso.appagent.android.SSOStatus;
import kr.co.nets.sso.appagent.android.UserCredentialType;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingActivity extends MacadamiaActivity {

    private static final String TAG = SettingActivity.class.getSimpleName();

    // region 100.Variables
    private ListView settingListView;
    private ImageButton buttonBack;
    private ImageButton buttonNotification;
    private ImageButton buttonMenu;
    private int DELAY_10;
    private int DELAY_100;
    private MacadamiaWebView settingWebView;

    private SettingListAdapter settingListAdapter = null;
    private ArrayList<String> settingList = null;
    // StatusBar ConstraintLayout Height
    private ConstraintLayout statusBarLayout;

    private static final int MSG_SETTING_REQUEST_APP_INFO = 0x0100;
    private static final int MSG_SETTING_REQUEST_USER_INFO = 0x0200;

    private static final int[] MSG_SETTING_LIST = {
            MSG_SETTING_REQUEST_APP_INFO,
            MSG_SETTING_REQUEST_USER_INFO
    };

    private static boolean pushAccept = false;
    private int requestCode = 0;

    // endregion

    // region
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        // set ApplicationContext
        ctx = getApplicationContext();

        settingWebView = findViewById(R.id.webview_setting);
        buttonBack = findViewById(R.id.button_setting_back);
        buttonBack.setOnClickListener((v) -> {
            Logger.t(TAG).d("onClick Back");
            finish();
        });
        buttonNotification = findViewById(R.id.button_setting_notification);
        buttonMenu = findViewById(R.id.button_setting_menu);
        DELAY_10 = getResources().getInteger(R.integer.delay_10);
        DELAY_100 = getResources().getInteger(R.integer.delay_100);
        // bindView
        statusBarLayout = findViewById(R.id.layout_setting_00);
        // SSO
        authCheck = new AuthCheck();

        // 로그인이 되어있는 경우 sso login 처리
        if (Macadamia.Constants.LOGGED) {
            processSSOLogin();
        }

        requestCode = getIntent().getIntExtra(Macadamia.Request.REQ_CODE, 0);

        settingWebView.getSettings().setUserAgentString(Macadamia.Constants.USER_AGENT);
        settingWebView.setWebViewClient(webViewClient);
        settingWebView.loadUrl(Macadamia.Urls.WEB_URL_MAIN);
    }

    private void processSSOLogin() {
        try {
            SSOStatus ssoStatus = authCheck.encLogon(UserCredentialType.EncryptedBasicCredential, getSSOCredential());
            if (ssoStatus == SSOStatus.SSOSuccess) {
                AuthSuccessResult ssoResult = (AuthSuccessResult) authCheck.getResult();
//                if (ssoResult.getServerErrorCode() != ServerErrorCode.NoError) {
//                    ;
//                }
            } else if (ssoStatus == SSOStatus.SSOFail) {
                setSSOCredential(Macadamia.Constants.STRING_BLANK);
            }
        } catch (Exception e) {
            Logger.t(TAG).e("auto login Exception %s", e.getMessage());
            setSSOCredential(Macadamia.Constants.STRING_BLANK);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.t(TAG).d("- onResume %s", Macadamia.Constants.LOGGED);

        settingList = new ArrayList<>();
        settingList.add("basic");
        settingList.add("permission");
        settingList.add("license");
        settingListView = findViewById(R.id.listview_setting);
        settingListAdapter = new SettingListAdapter(this, settingList, authCheck);
        settingListView.setAdapter(settingListAdapter);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            MacadamiaAlertDialog.showAlert(SettingActivity.this,
                    false,
                    getString(R.string.dialog_title_01),
                    getString(R.string.app_permission_setting_error),
                    getString(R.string.dialog_button_ok),
                    () -> {
                        resetUserInfo(authCheck);
                        finish();
                    });
        } else {
            getAppInfo();
        }

        if (Macadamia.Constants.APP_BACKGROUND_STAYING_TIME > Macadamia.Constants.EXPIRE_SECONDS) {
            if (Macadamia.Constants.LOGGED) {
                if (Macadamia.Functions.getAutoLogin()) {
                    try {
                        SSOStatus status = authCheck.encLogon(UserCredentialType.EncryptedBasicCredential, getSSOCredential());
                        if (status == SSOStatus.SSOSuccess) {
                            Macadamia.Constants.LOGGED = true;
                            this.settingListAdapter.update(settingList);
                        } else {
                            MacadamiaAlertDialog.showAlert(SettingActivity.this,
                                    false,
                                    getString(R.string.dialog_title_01),
                                    getString(R.string.dialog_message_login_fail_02),
                                    getString(R.string.dialog_button_ok),
                                    () -> {
                                        logOff(authCheck);
                                        this.settingListAdapter.update(settingList);
                                        setResult(Macadamia.Result.RES_LOGOFF);
                                        finish();
                                    });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        logOff(authCheck);
                        this.settingListAdapter.update(settingList);
                        setResult(Macadamia.Result.RES_LOGOFF);
                        finish();
                    }
                } else {
                    MacadamiaAlertDialog.showAlert(SettingActivity.this,
                            false,
                            getString(R.string.dialog_title_01),
                            getString(R.string.dialog_message_login_expired),
                            getString(R.string.dialog_button_ok),
                            () -> {
                                logOff(authCheck);
                                this.settingListAdapter.update(settingList);
                                setResult(Macadamia.Result.RES_LOGOFF);
                                finish();
                            });
                }
            } else {
                logOff(authCheck);
                this.settingListAdapter.update(settingList);
            }

            Macadamia.Constants.APP_BACKGROUND_STAYING_TIME = 0;
        }
    }

    @Override
    public void initActivity() {
        Logger.t(TAG).d("- initialize Activity");
        // Activity Event Handler
        eventHandler = new MacaEventHandler(this, MSG_SETTING_LIST);
        eventHandler.startEvent(null);
        // Api Call
        macadamiaApi = new MacadamiaApi(ctx);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    // endregion

    @Override
    public void handleEventMessage(@NonNull Message msg) {
        switch (msg.what) {
            case MSG_SETTING_REQUEST_APP_INFO:
                Logger.t(TAG).d("- MSG_SETTING_REQUEST_APP_INFO");
                getAppInfo();
                break;

            case MSG_SETTING_REQUEST_USER_INFO:
                Logger.t(TAG).d("- MSG_SETTING_REQUEST_USER_INFO");
                getUserInfo();
                break;

            default:
                break;
        }
    }

    public void onClickTogglePermission(String type, boolean checked) {
        String message1 = "";
        String message2 = "";

        if (type.equals("camera")) {
            message1 = getString(R.string.setting_permission_camera_disallow);
            message2 = getString(R.string.setting_permission_camera_allow);
        } else if (type.equals("location")) {
            message1 = getString(R.string.setting_permission_location_disallow);
            message2 = getString(R.string.setting_permission_location_allow);
        }

        // confirm dialog
        MacadamiaAlertDialog.showConfirm(SettingActivity.this,
                false,
                getString(R.string.dialog_title_01),
                (checked ? message1 : message2),
                getString(R.string.dialog_button_setting_now),
                () -> {
                    Intent appDetail = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:".concat(getPackageName())));
                    appDetail.addCategory(Intent.CATEGORY_DEFAULT);
                    appDetail.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(appDetail);
                },
                getString(R.string.dialog_button_setting_later),
                () -> {
                }
        );
        settingListAdapter.update(settingList);
    }

    public void oClickButtonVersionUpdate() {
        MacadamiaUtils.goPlayStore(SettingActivity.this, getPackageName());
    }

    private void getAppInfo() {
        try {
            JSONObject param = new JSONObject();
            param.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
            param.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);
            param.put(Macadamia.Keys.PARAM_KEY_OS_VERSION, Macadamia.Constants.DEVICE_OS_VERSION);
            param.put(Macadamia.Keys.PARAM_KEY_DEVICE_MODEL, Macadamia.Constants.DEVICE_MODEL);

            macadamiaApi.request(MacadamiaRequestType.REQUEST_APP_INFO,
                    param,
                    requestAppInfoCallback);
        } catch (Exception e) {
            Logger.t(TAG).e("MSG_SETTING_REQUEST_APP_INFO : %s", e.getMessage());
            networkErrorAlertDialog(SettingActivity.this,
                    getString(R.string.dialog_button_finish),
                    this::finish);
        }
    }

    /**
     * APP 정보 조회 Callback
     */
    private final Callback<JSONObject> requestAppInfoCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(@NonNull Call<JSONObject> call, @NonNull Response<JSONObject> response) {
            Logger.t(TAG).d("API GET_APP_INFO RESP %s", response);
            if (response.isSuccessful()) {
                JSONObject data = response.body().getJSONObject(Macadamia.Keys.PARAM_KEY_DATA);
                Logger.t(TAG).d("API GET_APP_INFO RESP data : %s", data);
                JSONObject appVersion = data.getJSONObject(Macadamia.Keys.PARAM_KEY_VERSION);
                settingListAdapter.setVersion(settingList, appVersion);

                eventHandler.processNextEvent(null, DELAY_10);
            } else {
                dataLoadingErrorAlertDialog(SettingActivity.this,
                        getString(R.string.dialog_button_ok),
                        () -> {
                        });
            }
        }

        @Override
        public void onFailure(@NonNull Call<JSONObject> call, Throwable t) {
            Logger.t(TAG).e("[FAILURE] %s", t.getMessage());
            networkErrorAlertDialog(SettingActivity.this,
                    getString(R.string.dialog_button_ok),
                    () -> {
                    });
        }
    };

    /**
     * 사용자 정보 조회
     */
    private void getUserInfo() {
        Logger.t(TAG).d("MacadamiaApi getUserInfo");
        try {
            JSONObject param = new JSONObject();
            param.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
            param.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);

            macadamiaApi.request(MacadamiaRequestType.REQUEST_USER_INFO,
                    macadamiaApi.getHeaderMap(getSSOHeader(getDeviceHeader(authCheck))),
                    param,
                    requestUserInfoCallback);
        } catch (Exception e) {
            Logger.t(TAG).e("API GET_USR_INFO Exception", e);
            networkErrorAlertDialog(SettingActivity.this,
                    getString(R.string.dialog_button_ok),
                    () -> {
                    });
        }
    }

    /**
     * 사용자 정보 조회 Callback
     */
    private final Callback<JSONObject> requestUserInfoCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(@NonNull Call<JSONObject> call, @NonNull Response<JSONObject> response) {
            Logger.t(TAG).d("API GET_USR_INFO RESP %s", response);
            if (response.isSuccessful()) {
                JSONObject data = response.body().getJSONObject(Macadamia.Keys.PARAM_KEY_DATA);
                Logger.t(TAG).d("API GET_USR_INFO RESP data : %s", data);
                if (data != null) {
                    Macadamia.Functions.setUserName(data.getString(Macadamia.Keys.PARAM_KEY_NAME));
                    Macadamia.Functions.setUserEmail(data.getString(Macadamia.Keys.PARAM_KEY_EMAIL));
                    int notificationCount = data.getInteger(Macadamia.Keys.PARAM_KEY_NOTIFICATION_COUNT);
                    Macadamia.Functions.setPushReceive(data.getJSONObject(Macadamia.Keys.PARAM_KEY_PUSH).getBoolean(Macadamia.Keys.PARAM_KEY_PUSH_RECEIVE));
                } else {
                    resetUserInfo(authCheck);
                }
            } else {
                Logger.t(TAG).e("API GET_USR_INFO ERR");
                dataLoadingErrorAlertDialog(SettingActivity.this,
                        getString(R.string.dialog_button_ok),
                        () -> {
                        });
            }
            settingListAdapter.update(settingList);
        }

        @Override
        public void onFailure(@NonNull Call<JSONObject> call, Throwable t) {
            Logger.t(TAG).e("[FAILURE] %s", t.getMessage());
            networkErrorAlertDialog(SettingActivity.this,
                    getString(R.string.dialog_button_close),
                    () -> {
                    });
        }
    };

    /**
     * PUSH 토큰 업데이트
     */
    private void updatePushToken(boolean receivePush) {
        Logger.t(TAG).d("MacadamiaApi updatePushToken");
        try {
            JSONObject param = new JSONObject();
            param.put(Macadamia.Keys.PARAM_KEY_TOKEN, Macadamia.Functions.getPushToken());
            param.put("receivePush", receivePush);
            param.put(Macadamia.Keys.PARAM_KEY_APP_ID, Macadamia.Constants.APP_ID);
            param.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
            param.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);

            macadamiaApi.request(MacadamiaRequestType.UPDATE_PUSH_TOKEN,
                    macadamiaApi.getHeaderMap(getSSOHeader(getDeviceHeader(authCheck))),
                    param,
                    updatePushTokenCallback);
        } catch (Exception e) {
            Logger.t(TAG).e("API UPDATE_PUSH_TOKEN Exception", e);
            networkErrorAlertDialog(SettingActivity.this,
                    getString(R.string.dialog_button_ok),
                    () -> {
                    });
        }
    }

    /**
     * PUSH TOKEN Update Callback
     */
    private final Callback<JSONObject> updatePushTokenCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(@NonNull Call<JSONObject> call, @NonNull Response<JSONObject> response) {
            Logger.t(TAG).d("API UPDATE_PUSH_TOKEN RESP %s", response);
            if (response.isSuccessful()) {
                JSONObject data = response.body().getJSONObject(Macadamia.Keys.PARAM_KEY_DATA);
                Logger.t(TAG).d("API UPDATE_PUSH_TOKEN RESP data : %s", data);
                String message = String.format(getString(R.string.dialog_message_01),
                        pushAccept ? "동의" : "거부",
                        MacadamiaUtils.getCurrentDateTime("yyyy-MM-dd HH:mm:ss"));
                MacadamiaAlertDialog.showAlert(SettingActivity.this,
                        false,
                        getString(R.string.dialog_title_01),
                        message,
                        getString(R.string.dialog_button_ok),
                        () -> {
                            Macadamia.Functions.setPushReceive(pushAccept);
                            settingListAdapter.update(settingList);
                        });
            } else {
                Logger.t(TAG).e("API UPDATE_PUSH_TOKEN ERR");
                dataLoadingErrorAlertDialog(SettingActivity.this,
                        getString(R.string.dialog_button_ok),
                        () -> {
                        });
            }
        }

        @Override
        public void onFailure(@NonNull Call<JSONObject> call, Throwable t) {
            Logger.t(TAG).e("[FAILURE] %s", t.getMessage());
            networkErrorAlertDialog(SettingActivity.this,
                    getString(R.string.dialog_button_close),
                    () -> {
                    });
        }
    };

    public void onClickLogin(View view) {
        //Macadamia.Functions.setMainLoadUrl(Macadamia.Urls.WEB_URL_LOGIN);
        setResult(Macadamia.Result.RES_LOGIN);
        finish();
    }

    public void onClickLogout(View view) {
        MacadamiaAlertDialog.showConfirm(SettingActivity.this,
                false,
                getString(R.string.dialog_title_01),
                getString(R.string.dialog_message_logout),
                getString(R.string.dialog_button_ok),
                () -> {
                    try {
                        SSOStatus ssoStatus = authCheck.logOff();
                        if (ssoStatus == SSOStatus.SSOSuccess) {
                            settingListAdapter.update(settingList);
                            Macadamia.Functions.setLogoutStatus(true);
                            Macadamia.Functions.setUserCn(0);
                            // 로그아웃 플래그
                            logOff(authCheck);
                            setSSOCredential(Macadamia.Constants.STRING_BLANK);
                            Macadamia.Functions.setAutoLogin(false);
                            String logoffUrl = Macadamia.Functions.getLogoffUrl();
//                            Logger.t(TAG).d("WebViewClient Logout %s", logoffUrl);
                            settingWebView.loadUrl(logoffUrl);
                            setResult(Macadamia.Result.RES_LOGOFF);
                            finish();
                        } else {
                            Macadamia.Functions.setLogoutStatus(false);
                            // 로그아웃 플래그
                            Macadamia.Constants.LOGGED = true;
                            MacadamiaAlertDialog.showAlert(SettingActivity.this,
                                    false,
                                    getString(R.string.dialog_title_01),
                                    getString(R.string.dialog_message_sso_logoff_failed),
                                    getString(R.string.dialog_button_ok),
                                    () -> settingListAdapter.update(settingList));
                        }
                    } catch (Exception e) {
                        Logger.t(TAG).e("LogOff Exception : %s", e.getMessage());
                    }
                },
                getString(R.string.dialog_button_cancel),
                () -> {
                });
    }

    public void onCheckPush(View v, boolean checked) {
        updatePushToken(checked);
        pushAccept = checked;
    }

    public void onClickModifyInfo(View view) {
        Macadamia.Functions.setMainLoadUrl(Macadamia.Urls.WEB_URL_MODIFY_INFO);
        finish();
    }


    private final WebViewClient webViewClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Logger.t(TAG).d("WebViewClient Logout shouldOverrideUrlLoading request : %s", request.getUrl());
            return false;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Logger.t(TAG).d("WebViewClient Logout shouldOverrideUrlLoading url : %s", url);
            return false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Logger.t(TAG).d("WebViewClient Logout onPageStarted : %s", url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                Logger.t(TAG).e("WebViewClient onReceivedError( %d ) : %s", error.getErrorCode(), view.getUrl());
            }
            String errorPage = "file:///android_asset/html/400.html";
            // 2020.09.28 error page 제외
            //view.loadUrl(errorPage);
        }
    };
}