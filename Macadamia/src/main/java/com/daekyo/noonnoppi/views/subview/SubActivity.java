package com.daekyo.noonnoppi.views.subview;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.daekyo.noonnoppi.common.dialog.MacadamiaAlertDialog;
import com.daekyo.noonnoppi.views.MacadamiaActivity;
import com.orhanobut.logger.Logger;

import butterknife.ButterKnife;

public class SubActivity extends MacadamiaActivity {

    private static final String TAG = SubActivity.class.getSimpleName();

    private TextView textViewTitle;
    private WebView subWebView;
    private ImageButton buttonClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.t(TAG).d("[onCreate]");
        setContentView(R.layout.activity_sub);
        textViewTitle = findViewById(R.id.textview_sub_title);
        subWebView = findViewById(R.id.webview_sub);
        buttonClose = findViewById(R.id.button_sub_close);
        buttonClose.setOnClickListener((v) -> {
            setResult(Macadamia.Result.RES_RELOAD, null);
            finish();
        });
        // set ApplicationContext
        ctx = getApplicationContext();
        // bind ButterKnife
        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        String snsLoginUrl = intent.getStringExtra(Macadamia.Keys.PARAM_KEY_URL);
        String userAgent = subWebView.getSettings().getUserAgentString();
        if (snsLoginUrl.contains("callback") && snsLoginUrl.contains("google")) {
            userAgent = userAgent.replaceAll("; wv", "");
        }

        // title
        String titleString = "카카오";
        if (snsLoginUrl.contains("kakao")) {
            titleString = "카카오";
        } else if (snsLoginUrl.contains("naver")) {
            titleString = "네이버";
        } else if (snsLoginUrl.contains("google")) {
            titleString = "Google";
        } else if (snsLoginUrl.contains("apple")) {
            titleString = "Apple";
        }
        textViewTitle.setText(String.format("%s로 로그인", titleString));
        // webview
        subWebView.getSettings().setJavaScriptEnabled(true);
        subWebView.getSettings().setDomStorageEnabled(true);
        subWebView.getSettings().setSupportMultipleWindows(true);
        subWebView.getSettings().setDisplayZoomControls(false);
        subWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        subWebView.setVerticalScrollBarEnabled(true);
        subWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        subWebView.getSettings().setUserAgentString(userAgent);
        subWebView.setWebChromeClient(chromeClient);
        subWebView.setWebViewClient(webViewClient);
        subWebView.loadUrl(snsLoginUrl, null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private boolean handleUrlLoading(WebView view, Uri uri) {
        // region
        Logger.t(TAG).d("WebViewClient handleUrlLoading %s", uri);
        Logger.t(TAG).d("URI_SCHEME : %s", uri);
        Logger.t(TAG).d("URI_SCHEME Scheme : %s", uri.getScheme());
        Logger.t(TAG).d("URI_SCHEME Host : %s", uri.getHost());
        Logger.t(TAG).d("URI_SCHEME Port : %d", uri.getPort());

        Logger.t(TAG).d("URI_SCHEME LastPathSegment : %s", uri.getLastPathSegment());
        Logger.t(TAG).d("URI_SCHEME PathSegments : %s", uri.getPathSegments());

        Logger.t(TAG).d("URI_SCHEME EncodedAuthority : %s", uri.getEncodedAuthority());
        Logger.t(TAG).d("URI_SCHEME EncodedFragment : %s", uri.getEncodedFragment());
        Logger.t(TAG).d("URI_SCHEME EncodedPath : %s", uri.getEncodedPath());
        Logger.t(TAG).d("URI_SCHEME EncodedQuery : %s", uri.getEncodedQuery());
        Logger.t(TAG).d("URI_SCHEME EncodedSchemeSpecificPart : %s", uri.getEncodedSchemeSpecificPart());
        Logger.t(TAG).d("URI_SCHEME EncodedUserInfo : %s", uri.getEncodedUserInfo());

        Logger.t(TAG).d("URI_SCHEME Authority : %s", uri.getAuthority());
        Logger.t(TAG).d("URI_SCHEME Fragment : %s", uri.getFragment());
        Logger.t(TAG).d("URI_SCHEME Path : %s", uri.getPath());
        Logger.t(TAG).d("URI_SCHEME Query : %s", uri.getQuery());
        Logger.t(TAG).d("URI_SCHEME SchemeSpecificPart : %s", uri.getSchemeSpecificPart());
        Logger.t(TAG).d("URI_SCHEME UserInfo : %s", uri.getUserInfo());
        // endregion

        if (uri != null) {
            final String URI_SCHEME = uri.getScheme();
            final String URI_HOST = uri.getHost();
            final String URI_SCHEME_SPECIFIC_PART = uri.getSchemeSpecificPart();
            final String URI_FRAGMENT = uri.getFragment();
            final String URI_PATH = uri.getPath();

            if (URI_SCHEME.equals("http") || URI_SCHEME.equals("https") || URI_SCHEME.equals("javascript")) {
                if (URI_PATH.contains("/user/callback/") || URI_PATH.contains("/sns-join ")) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(Macadamia.Keys.PARAM_KEY_URL, uri.toString());
                    setResult(Macadamia.Result.RES_LOAD_URL, returnIntent);
                    finish();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private final WebViewClient webViewClient = new WebViewClient() {
        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Logger.t(TAG).d("WebViewClient shouldOverrideUrlLoading request : %s", request.getUrl());
            return handleUrlLoading(view, request.getUrl());
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Logger.t(TAG).d("WebViewClient shouldOverrideUrlLoading url : %s", url);
            return handleUrlLoading(view, Uri.parse(url));
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Logger.t(TAG).d("WebViewClient onPageStarted : %s", url);
            // apple login 의 경우 POST callback 호출되서 onPageStart 시점에 현재창 닫도록 처리
            if (url.contains("/user/callback/apple")) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(Macadamia.Keys.PARAM_KEY_URL, url);
                setResult(Macadamia.Result.RES_LOAD_URL, returnIntent);
                finish();
            }
            startLoading(SubActivity.this, null);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Logger.t(TAG).d("webview onPageFinished %s", url);
            stopLoading();
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
            Logger.t(TAG).d("WebViewClient onLoadResource : %s [%d]", url, url.getBytes().length);
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            Logger.t(TAG).d("WebViewClient onPageCommitVisible : %s", url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                Logger.t(TAG).e("WebViewClient onReceivedError( %d ) : %s %s", error.getErrorCode(), error.getDescription(), view.getUrl());
            }
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);
            Logger.t(TAG).e("WebViewClient onReceivedHttpError( %d ) : %s", errorResponse.getStatusCode(), view.getUrl());
        }

        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            super.doUpdateVisitedHistory(view, url, isReload);
            Logger.t(TAG).d("WebViewClient doUpdateVisitedHistory %s %s", url, isReload);
        }

        @Override
        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
            super.onFormResubmission(view, dontResend, resend);
            Logger.t(TAG).d("WebViewClient onFormResubmission %s %s", dontResend, resend);
        }

        @Override
        public void onScaleChanged(WebView view, float oldScale, float newScale) {
            super.onScaleChanged(view, oldScale, newScale);
            Logger.t(TAG).d("WebViewClient onScaleChanged %f %f", oldScale, newScale);
        }
    };

    private final WebChromeClient chromeClient = new WebChromeClient() {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            Logger.t(TAG).d("WebChromeClient onProgressChanged %d", newProgress);
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            Logger.t(TAG).d("WebChromeClient onCreateWindow");
            return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
        }

        @Override
        public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
            Logger.t(TAG).d("WebChromeClient onJsAlert %s %s", url, message);
            MacadamiaAlertDialog.showAlert(SubActivity.this,
                    false,
                    getString(R.string.dialog_title_01),
                    message,
                    getString(R.string.dialog_button_ok), result::confirm);
            return true;
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
            Logger.t(TAG).d("WebChromeClient onJsConfirm %s %s", url, message);
            MacadamiaAlertDialog.showConfirm(SubActivity.this,
                    false,
                    getString(R.string.dialog_title_01),
                    message,
                    getString(R.string.dialog_button_ok), result::confirm,
                    getString(R.string.dialog_button_cancel), result::cancel);
            return true;
        }
    };


}
