package com.daekyo.noonnoppi.views.orangedot;

import com.alibaba.fastjson.JSONObject;

import java.util.Comparator;

public class OrangeDotOrderComparator implements Comparator<JSONObject> {

    private final String key;

    public OrangeDotOrderComparator(String key) {
        this.key = key;
    }

    @Override
    public int compare(JSONObject o1, JSONObject o2) {
        int order1 = o1.getInteger(key);
        int order2 = o2.getInteger(key);
        return Integer.compare(order1, order2);
    }
}
