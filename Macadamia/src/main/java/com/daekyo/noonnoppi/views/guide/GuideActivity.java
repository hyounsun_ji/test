package com.daekyo.noonnoppi.views.guide;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager2.widget.ViewPager2;

import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.views.MacadamiaActivity;
import com.daekyo.noonnoppi.views.main.MainActivity;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;

public class GuideActivity extends MacadamiaActivity {

    private static final String TAG = GuideActivity.class.getSimpleName();
    private static final int[] MSG_GUIDE_LIST = {
            0
    };
    private ArrayList<Drawable> guides;
    private ViewPager2 guideViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        // set ApplicationContext
        ctx = getApplicationContext();
        guideViewPager = findViewById(R.id.pager_guide);
        guides = new ArrayList<>();
        guides.add(ResourcesCompat.getDrawable(getResources(), R.drawable.guide_01, null));
        guides.add(ResourcesCompat.getDrawable(getResources(), R.drawable.guide_02, null));
        guides.add(ResourcesCompat.getDrawable(getResources(), R.drawable.guide_03, null));
        guides.add(ResourcesCompat.getDrawable(getResources(), R.drawable.guide_04, null));
        guides.add(ResourcesCompat.getDrawable(getResources(), R.drawable.guide_05, null));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // ReCyclerViewAdapter
        GuidePagerAdapter adapter = new GuidePagerAdapter(ctx, guides);
        guideViewPager.setAdapter(adapter);
        guideViewPager.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        guideViewPager.registerOnPageChangeCallback(onPageChangeCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (guideViewPager != null) {
            guideViewPager.unregisterOnPageChangeCallback(onPageChangeCallback);
        }
    }

    private final ViewPager2.OnPageChangeCallback onPageChangeCallback = new ViewPager2.OnPageChangeCallback() {
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            Logger.t(TAG).d("onPageSelected %d", position);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            Logger.t(TAG).d("onPageScrolled %d %f %d", position, positionOffset, positionOffsetPixels);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            super.onPageScrollStateChanged(state);
            Logger.t(TAG).d("onPageScrollStateChanged %d", state);
        }
    };

    @Override
    public void initActivity() {
        Logger.t(TAG).d("- initialize Activity");
        // Activity Event Handler
        eventHandler = new MacaEventHandler(this, MSG_GUIDE_LIST);
        eventHandler.startEvent(null);
    }

    /**
     * 시작하기
     *
     * @param view StartButton
     */
    public void onClickButtonStart(View view) {
        Intent intent = getIntent();
        intent.setClass(ctx, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        startActivity(intent);
        finish();
    }

    /**
     * 닫기
     *
     * @param view View
     */
    public void onClickButtonClose(View view) {
        this.onClickButtonStart(view);
    }

    public void onClickButtonPrev(View view, int position) {
        guideViewPager.setCurrentItem(position - 1, true);
    }

    public void onClickButtonNext(View view, int position) {
        guideViewPager.setCurrentItem(position + 1, true);
    }
}