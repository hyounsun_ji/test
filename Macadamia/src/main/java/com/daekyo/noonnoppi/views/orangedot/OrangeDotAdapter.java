package com.daekyo.noonnoppi.views.orangedot;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

public class OrangeDotAdapter extends RecyclerView.Adapter<OrangeDotAdapter.OrangeDotItemHolder> {

    private static final String TAG = OrangeDotAdapter.class.getSimpleName();
    private ArrayList<JSONObject> items;
    private final Context context;

    public OrangeDotAdapter(@NonNull Context context, @NonNull ArrayList<JSONObject> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public OrangeDotItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_orangedot_item, null, false);
        return new OrangeDotItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrangeDotItemHolder holder, int position) {
        JSONObject item = items.get(position);
        holder.itemTitle.setText(item.getString(Macadamia.Keys.PARAM_KEY_TITLE));
        holder.order = item.getInteger(Macadamia.Keys.PARAM_KEY_ORDER);
        holder.url = item.getString(Macadamia.Keys.PARAM_KEY_URL);
        String iconUrl = item.getString(Macadamia.Keys.PARAM_KEY_ICON);
        holder.iconUrl = iconUrl;
        holder.myorder = item.getInteger(Macadamia.Keys.PARAM_KEY_MY_ORDER);
        holder.itemAdded = item.getBoolean(Macadamia.Keys.PARAM_KEY_ADDED);

        if (StringUtils.isNotEmpty(iconUrl) && iconUrl.startsWith("http")) {
            Glide.with(context).load(iconUrl).into(holder.buttonIcon);
        } else {
            holder.buttonIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_add));
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * update Adapter
     *
     * @param list my orangedot
     */
    public void update(ArrayList<JSONObject> list) {
        Logger.t(TAG).d("adapter update : %s", list);
        this.items = list;
        notifyDataSetChanged();
    }

    /**
     * OrangeDotItemHolder
     */
    public class OrangeDotItemHolder extends RecyclerView.ViewHolder {
        protected final RecyclerView recyclerViewList;
        protected final ImageButton buttonIcon;
        protected final TextView itemTitle;
        protected int order;
        protected int myorder;
        protected String url;
        protected String iconUrl;
        protected boolean itemAdded = false;

        public OrangeDotItemHolder(View view) {
            super(view);
            this.recyclerViewList = view.findViewById(R.id.recycler_orangedot_items);
            this.itemTitle = view.findViewById(R.id.text_orangedot_item);
            this.buttonIcon = view.findViewById(R.id.button_orangedot_item);

            // set contentDescription
            this.buttonIcon.setContentDescription(StringUtils.defaultIfEmpty(this.itemTitle.getText(), "icon"));

            this.buttonIcon.setOnClickListener((v) -> {
                Intent intent = new Intent();
                if (this.order != 999) {
                    intent.putExtra(Macadamia.Keys.PARAM_KEY_URL, this.url);
                    ((OrangeDotActivity) context).setResultToMainActivity(Macadamia.Result.RES_LOAD_URL, intent);
                } else {
//                    intent.setClass(context, OrangeDotSettingActivity.class);
//                    intent.putExtra(Macadamia.Request.REQ_CODE, Macadamia.Request.REQ_SHOW_ORANGEDOT_SETTING);
//                    ((OrangeDotActivity) context).startActivityForResult(intent, Macadamia.Request.REQ_SHOW_ORANGEDOT_SETTING);
//                    ((OrangeDotActivity) context).overridePendingTransition(R.anim.view_orangedot_show, R.anim.view_nothing);
                    // orangedotsetting open
                    ((OrangeDotActivity) context).openOrangeDotSetting();
                }
            });
        }
    }
}
