package com.daekyo.noonnoppi.views.launch;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.daekyo.noonnoppi.BuildConfig;
import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.daekyo.noonnoppi.common.api.MacadamiaApi;
import com.daekyo.noonnoppi.common.api.MacadamiaRequestType;
import com.daekyo.noonnoppi.common.dialog.MacadamiaAlertDialog;
import com.daekyo.noonnoppi.common.ui.MacadamiaWebView;
import com.daekyo.noonnoppi.common.utils.MacadamiaScreenUtils;
import com.daekyo.noonnoppi.common.utils.MacadamiaSecurityUtils;
import com.daekyo.noonnoppi.common.utils.MacadamiaUtils;
import com.daekyo.noonnoppi.views.MacadamiaActivity;
import com.daekyo.noonnoppi.views.guide.GuideActivity;
import com.daekyo.noonnoppi.views.main.MainActivity;
import com.github.florent37.runtimepermission.PermissionResult;
import com.github.florent37.runtimepermission.callbacks.PermissionListener;
import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import kr.co.nets.sso.appagent.android.AppException;
import kr.co.nets.sso.appagent.android.AuthCheck;
import kr.co.nets.sso.appagent.android.AuthFailResult;
import kr.co.nets.sso.appagent.android.AuthSuccessResult;
import kr.co.nets.sso.appagent.android.SSOConfig;
import kr.co.nets.sso.appagent.android.SSOStatus;
import kr.co.nets.sso.appagent.android.ServerErrorCode;
import kr.co.nets.sso.appagent.android.UserCredentialType;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;

/**
 * 1. Rooting check
 * 2. Connect Logging I/F
 * 3. Permission Check
 * 4. PUSH 권한 체크
 * 5. Update Check I/F
 * 6. POPUP Check
 * 7. Move to Main
 */
public class LaunchActivity extends MacadamiaActivity {
    // region 100.Variables
    private static final String TAG = LaunchActivity.class.getSimpleName();
    private static final String TAG_INTENT = TAG.concat("_INTENT");
    // rooting 체크
    private static final int MSG_LAUNCH_CHECK_ROOTING = 0x0100;
    // 권한 VIEW
    private static final int MSG_LAUNCH_SHOW_PERMISSION_VIEW = 0x0900;
    // 권한 체크
    private static final int MSG_LAUNCH_CHECK_PERMISSIONS = 0x0401;
    // 권한 체크 - 필수
    private static final int MSG_LAUNCH_CHECK_PERMISSIONS_MANDATORY = 0x0410;
    // 권한 체크 - 선택
    private static final int MSG_LAUNCH_CHECK_PERMISSIONS_OPTIONAL = 0x0420;
    // PUSH 수신 설정
    private static final int MSG_LAUNCH_SHOW_PUSH_DIALOG = 0x0920;
    // App Info
    private static final int MSG_LAUNCH_REQUEST_APP_INFO = 0x0300;
    // 메뉴
    private static final int MSG_LAUNCH_REQUEST_MENUS = 0x0310;
    // SSO init
    private static final int MSG_LAUNCH_SSO_INIT = 0x0480;
    // SSO
    private static final int MSG_LAUNCH_SSO_LOGIN = 0x0490;
    // 버전 체크
    private static final int MSG_LAUNCH_CHECK_APP_VERSION = 0x0600;
    // 버전 체크 - 강제 업데이트
    private static final int MSG_LAUNCH_FORCED_UPDATE = 0x0610;
    // 버전 체크 - 선택 업데이트
    private static final int MSG_LAUNCH_OPTIONAL_UPDATE = 0x0620;
    // 팝업 체크
    private static final int MSG_LAUNCH_CHECK_POPUP = 0x0700;
    // 메인 이동
    private static final int MSG_LAUNCH_MOVE_TO_MAIN = 0x0800;
    // PUSH token update
    private static final int MSG_LAUNCH_UPDATE_PUSH_TOKEN = 0x0830;

    private static final int MSG_LAUNCH_CHECK_SSO = 0x0820;

    private static final String PERMISSION_VIEW_REQUIRED = "required";
    private static final String PERMISSION_VIEW_OPTIONAL = "optional";

    private static final int[] MSG_LAUNCH_LIST = {
            MSG_LAUNCH_CHECK_ROOTING,
            MSG_LAUNCH_SHOW_PERMISSION_VIEW,
            MSG_LAUNCH_CHECK_PERMISSIONS,
            MSG_LAUNCH_SHOW_PUSH_DIALOG,
            MSG_LAUNCH_SSO_INIT,
            MSG_LAUNCH_SSO_LOGIN,
            MSG_LAUNCH_REQUEST_APP_INFO,
            MSG_LAUNCH_REQUEST_MENUS,
            MSG_LAUNCH_CHECK_POPUP,
            MSG_LAUNCH_CHECK_APP_VERSION,
            MSG_LAUNCH_MOVE_TO_MAIN
    };

    @BindString(R.string.app_id)
    String APP_ID;
    @BindInt(R.integer.delay_0)
    int DELAY_0;
    @BindInt(R.integer.delay_10)
    int DELAY_10;
    @BindInt(R.integer.delay_100)
    int DELAY_100;
    @BindView(R.id.text_launch_mode)
    TextView textLaunchMode;
    @BindView(R.id.layout_launch_permission)
    ConstraintLayout layoutPermission;
    @BindView(R.id.button_launch_permission_ok)
    Button buttonPermissionOk;

    private JSONArray popups;
    private JSONObject appVersion;
    private JSONArray outlinks;

    // PlayStore 업데이트 이동 여부
    private static boolean MOVE_TO_PLAYSTORE = false;
    // endregion

    // region 200.Activity Life Cycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        Logger.t(TAG).d("[onCreate] %s", APP_ID);
        // set ApplicationContext
        ctx = getApplicationContext();
        // bindView
        // StatusBar ConstraintLayout Height
        ConstraintLayout statusBarLayout = findViewById(R.id.layout_launch_00);
        // bind ButterKnife
        unbinder = ButterKnife.bind(this);
        // UI stretch
        MacadamiaScreenUtils.stretchScreenWithPadding(ctx, this, true, statusBarLayout);
        // navigationBar hide
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        // debug ?? stage ?? release
        if (BuildConfig.IS_DEV) {
            textLaunchMode.setText(String.format(getString(R.string.app_version_info_debug),
                    "DEBUG",
                    MacadamiaUtils.getVersionName(ctx),
                    MacadamiaUtils.getVersionCode(ctx)));
            textLaunchMode.setVisibility(View.VISIBLE);
        } else {
            textLaunchMode.setText(String.format(getString(R.string.app_version_info),
                    MacadamiaUtils.getVersionName(ctx),
                    MacadamiaUtils.getVersionCode(ctx)));
        }

        // SSO
        MacadamiaWebView launchWebView = findViewById(R.id.webview_launch);
        launchWebView.getSettings().setUserAgentString(Macadamia.Constants.USER_AGENT);
        launchWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                Logger.t(TAG).d("WebViewClient Logout shouldOverrideUrlLoading request : %s", request.getUrl());
                return false;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Logger.t(TAG).d("WebViewClient Logout shouldOverrideUrlLoading url : %s", url);
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Logger.t(TAG).d("WebViewClient Logout onPageStarted : %s", url);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                    Logger.t(TAG).e("WebViewClient onReceivedError( %d ) : %s", error.getErrorCode(), view.getUrl());
                }
            }
        });

        // 자동 로그인이 아닌 경우 강제 로그아웃 처리
        if (!Macadamia.Functions.getAutoLogin()) {
            launchWebView.loadUrl(Macadamia.Urls.WEB_URL_LOGOFF);
        }

        // init permission view
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) layoutPermission.getLayoutParams();
        params.topMargin += MacadamiaScreenUtils.getStatusBarHeight(this);
        params.bottomMargin += MacadamiaScreenUtils.getNavigationBarHeight(this);
        layoutPermission.setLayoutParams(params);

        ArrayList<String> permissionList = new ArrayList<>();
        permissionList.add(PERMISSION_VIEW_REQUIRED);
        permissionList.add(PERMISSION_VIEW_OPTIONAL);
        ListView permissionListView = findViewById(R.id.listview_launch_permission);
        LaunchPermissionListAdapter launchPermissionListAdapter = new LaunchPermissionListAdapter(this, permissionList);
        permissionListView.setAdapter(launchPermissionListAdapter);
        // 권한 VIEW Button ClickListener
        buttonPermissionOk.setOnClickListener(buttonClickListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.t(TAG).d("[onStart]");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.t(TAG).d("[onResume]");
        // PlayStore 이동후 업데이트 여부 체크
        if (MOVE_TO_PLAYSTORE && eventHandler.getCurrentEvent() == MSG_LAUNCH_CHECK_APP_VERSION) {
            MOVE_TO_PLAYSTORE = false;
            eventHandler.processNextEvent(null, DELAY_10);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.t(TAG).d("[onDestroy]");
        // ButterKnife Unbind
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Logger.t(TAG).d("[onPause]");
    }
    // endregion

    @Override
    public void initActivity() {
        Logger.t(TAG).d("- initialize Activity");
        // Activity Event Handler
        eventHandler = new MacaEventHandler(this, MSG_LAUNCH_LIST);
        eventHandler.startEvent(null);
        // Api Call
        macadamiaApi = new MacadamiaApi(ctx);
    }

    @Override
    public void handleEventMessage(@NonNull Message msg) {
        switch (msg.what) {
            case MSG_LAUNCH_CHECK_ROOTING:
                Logger.t(TAG).d("- MSG_LAUNCH_CHECK_ROOTING");
                if (!MacadamiaSecurityUtils.isRooted()) {
                    eventHandler.processNextEvent(null, DELAY_100);
                } else {
                    MacadamiaAlertDialog.showAlert(LaunchActivity.this,
                            false,
                            getString(R.string.dialog_title_02),
                            getString(R.string.dialog_message_rooted),
                            getString(R.string.dialog_button_finish),
                            this::finish);
                }
                break;

            case MSG_LAUNCH_SHOW_PERMISSION_VIEW:
                Logger.t(TAG).d("- MSG_LAUNCH_SHOW_PERMISSION_VIEW : %s", Macadamia.Functions.getPermissionViewChecked());
                if (!Macadamia.Functions.getPermissionViewChecked()) {
                    this.displayPermissionView(true);
                } else {
                    this.displayPermissionView(false);
                    eventHandler.processNextEvent(null, DELAY_10);
                }
                break;

            case MSG_LAUNCH_CHECK_PERMISSIONS:
                if (Macadamia.Functions.getPermissionViewChecked()) {
                    eventHandler.processEventById(MSG_LAUNCH_CHECK_PERMISSIONS_MANDATORY, null, DELAY_10);
                } else {
                    eventHandler.processNextEvent(null, DELAY_10);
                }
                break;

            case MSG_LAUNCH_CHECK_PERMISSIONS_MANDATORY:
                Logger.t(TAG).d("- MSG_LAUNCH_CHECK_PERMISSIONS_MANDATORY");
                requestMandatoryPermission();
                break;

            case MSG_LAUNCH_CHECK_PERMISSIONS_OPTIONAL:
                Logger.t(TAG).d("- MSG_LAUNCH_CHECK_PERMISSIONS_OPTIONAL");
                requestOptionalPermission();
                break;

            case MSG_LAUNCH_SHOW_PUSH_DIALOG:
                Logger.t(TAG).d("- MSG_LAUNCH_SHOW_PUSH_DIALOG %s", Macadamia.Functions.getLaunchPushChecked());
                if (!Macadamia.Functions.getLaunchPushChecked()) {
                    MacadamiaAlertDialog.showConfirm(LaunchActivity.this,
                            false,
                            getString(R.string.dialog_title_push),
                            getString(R.string.dialog_message_push),
                            getString(R.string.dialog_button_ok),
                            () -> MacadamiaAlertDialog.showAlert(LaunchActivity.this,
                                    false,
                                    getString(R.string.dialog_title_01),
                                    String.format(getString(R.string.dialog_message_01),
                                            getString(R.string.agree),
                                            MacadamiaUtils.getCurrentDateTime("yyyy-MM-dd HH:mm:ss")),
                                    getString(R.string.dialog_button_ok),
                                    () -> {
                                        Macadamia.Functions.setLaunchPushChecked(true);
                                        Macadamia.Functions.setPushReceive(true);
                                        eventHandler.processNextEvent(null, DELAY_10);
                                    }),
                            getString(R.string.dialog_button_setting_later),
                            () -> {
                                Macadamia.Functions.setLaunchPushChecked(true);
                                Macadamia.Functions.setPushReceive(false);
                                eventHandler.processNextEvent(null, DELAY_10);
                            });
                } else {
                    eventHandler.processNextEvent(null, DELAY_10);
                }
                break;

            case MSG_LAUNCH_SSO_INIT:
                Logger.t(TAG).d("- MSG_LAUNCH_SSO_INIT");
                this.initSSO();
                break;

            case MSG_LAUNCH_REQUEST_APP_INFO:
                Logger.t(TAG).d("- MSG_LAUNCH_REQUEST_APP_INFO");
                this.getAppInfo();
                break;

            case MSG_LAUNCH_REQUEST_MENUS:
                Logger.t(TAG).d("- MSG_LAUNCH_REQUEST_MENUS");
                this.getMenus();
                break;

            case MSG_LAUNCH_UPDATE_PUSH_TOKEN:
                Logger.t(TAG).d("- MSG_LANNCH_UPDATE_PUSH_TOKEN");
                if (Macadamia.Functions.getAutoLogin()) {
                    this.updatePushToken();
                }
                break;

            case MSG_LAUNCH_SSO_LOGIN:
                Logger.t(TAG).d("- MSG_LAUNCH_SSO_LOGIN %s", Macadamia.Functions.getAutoLogin());
                if (Macadamia.Functions.getAutoLogin()) {
                    this.processSSOLogin();
                } else {
                    this.resetUserInfo(authCheck);
                    eventHandler.processNextEvent(null, DELAY_10);
                }
                break;

            case MSG_LAUNCH_FORCED_UPDATE:
                Logger.t(TAG).d("- MSG_LAUNCH_FORCED_UPDATE");
                Bundle forcedBundle = msg.getData();
                String forcedMessage = StringUtils.defaultIfEmpty(forcedBundle.getString(Macadamia.Keys.PARAM_KEY_MESSAGE),
                        getString(R.string.dialog_message_03));
                MacadamiaAlertDialog.showAlert(LaunchActivity.this,
                        false,
                        getString(R.string.dialog_title_app_update),
                        forcedMessage,
                        getString(R.string.dialog_button_update),
                        () -> {
                            MOVE_TO_PLAYSTORE = true;
                            MacadamiaUtils.goPlayStore(LaunchActivity.this,
                                    getPackageName());
                        });
                break;

            case MSG_LAUNCH_OPTIONAL_UPDATE:
                Logger.t(TAG).d("- MSG_LAUNCH_OPTIONAL_UPDATE");
                Bundle optionalBundle = msg.getData();
                String optionalMessage = StringUtils.defaultIfEmpty(optionalBundle.getString(Macadamia.Keys.PARAM_KEY_MESSAGE),
                        getString(R.string.dialog_message_03));

                MacadamiaAlertDialog.showConfirm(LaunchActivity.this,
                        false,
                        getString(R.string.dialog_title_app_update),
                        optionalMessage,
                        getString(R.string.dialog_button_update_now),
                        () -> {
                            MOVE_TO_PLAYSTORE = true;
                            MacadamiaUtils.goPlayStore(LaunchActivity.this,
                                    getPackageName());
                        },
                        getString(R.string.dialog_button_update_later),
                        () -> {
                            MOVE_TO_PLAYSTORE = false;
                            eventHandler.processNextEvent(null, DELAY_10);
                        });
                break;

            case MSG_LAUNCH_CHECK_POPUP:
                Logger.t(TAG).d("- MSG_LAUNCH_CHECK_POPUP");
                if (popups.size() > 0) {
                    JSONObject popup = popups.getJSONObject(0);
                    MacadamiaAlertDialog.showAlert(LaunchActivity.this,
                            false,
                            popup.getString(Macadamia.Keys.PARAM_KEY_TITLE),
                            popup.getString(Macadamia.Keys.PARAM_KEY_CONTENTS),
                            getString(R.string.dialog_button_ok),
                            () -> {
                                Logger.t(TAG).d("popup close");
                                eventHandler.processEventById(MSG_LAUNCH_CHECK_APP_VERSION, null, DELAY_10);
                            });
                } else {
                    eventHandler.processNextEvent(null, DELAY_10);
                }
                break;

            case MSG_LAUNCH_CHECK_APP_VERSION:
                Logger.t(TAG).d("- MSG_LAUNCH_CHECK_APP_VERSION");
                JSONObject aos = appVersion.getJSONObject(Macadamia.Keys.PARAM_KEY_AOS);
                String currentVersion = aos.getString(Macadamia.Keys.PARAM_KEY_CURRENT);
                String newerVersion = aos.getString(Macadamia.Keys.PARAM_KEY_NEWER);
                String localVersion = Macadamia.Constants.APP_VERSION;
                int currentVersionInt = Integer.parseInt(currentVersion.replaceAll("\\.", Macadamia.Constants.STRING_BLANK));
                int newerVersionInt = Integer.parseInt(newerVersion.replaceAll("\\.", Macadamia.Constants.STRING_BLANK));
                int localVersionInt = Integer.parseInt(localVersion.replaceAll("\\.", Macadamia.Constants.STRING_BLANK));
                boolean forced = aos.getBoolean(Macadamia.Keys.PARAM_KEY_FORCED);
                String appUpdateMessage = aos.getString(Macadamia.Keys.PARAM_KEY_MESSAGE);

                Bundle updateBundle = new Bundle();
                updateBundle.putString(Macadamia.Keys.PARAM_KEY_MESSAGE, appUpdateMessage);
                Message updateMessage = new Message();
                updateMessage.setData(updateBundle);

                if (localVersionInt < newerVersionInt) {
                    if (forced) {
                        eventHandler.processEventById(MSG_LAUNCH_FORCED_UPDATE, updateMessage, DELAY_10);
                    } else {
                        eventHandler.processEventById(MSG_LAUNCH_OPTIONAL_UPDATE, updateMessage, DELAY_10);
                    }
                } else if (localVersionInt < currentVersionInt) {
                    eventHandler.processEventById(MSG_LAUNCH_FORCED_UPDATE, updateMessage, DELAY_10);
                } else {
                    eventHandler.processNextEvent(null, DELAY_10);
                }
                break;

            case MSG_LAUNCH_MOVE_TO_MAIN:
                Logger.t(TAG).d("- MSG_LAUNCH_MOVE_TO_MAIN");
                startMain();
                break;

            default:
                break;
        }
    }

    /**
     * initialize SSO
     */
    private void initSSO() {
        // SSO
        try {
            ssoConfig = SSOConfig.getInstance(getApplicationContext(), getFilesDir().getAbsolutePath());
            Logger.t(TAG).d("SSO %s", ssoConfig.getAppID());
            Logger.t(TAG).d("SSO %s", ssoConfig.getAppServiceURL());
        } catch (AppException e) {
            Logger.t(TAG_SSO).e("%s", e.getMessage());
        }
        authCheck = new AuthCheck();

        eventHandler.processNextEvent(null, DELAY_0);
    }

    /**
     * start MainActivity or GuideActivity
     */
    private void startMain() {
        Intent intent = new Intent();
        if (Macadamia.Functions.getGuideChecked()) {
            intent.setClass(ctx, MainActivity.class);
        } else {
            intent.setClass(ctx, GuideActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        startActivity(putExtraLinkUrl(intent));
        finish();
    }

    /**
     * putExtraLinkUrl
     *
     * @param mainIntent Intent
     * @return Intent
     */
    private Intent putExtraLinkUrl(Intent mainIntent) {
        Intent intent = getIntent();
        String action = intent.getAction();
        String urlLink = intent.hasExtra(Macadamia.Intent.KEY_LINK_URL) ? intent.getStringExtra(Macadamia.Intent.KEY_LINK_URL) : null;

        Logger.t(TAG_INTENT).e("INTENT : %s %s %s", intent, action, urlLink);
        Logger.t(TAG_INTENT).e("INTENT : %s", intent.getExtras());

        if (StringUtils.isNotEmpty(action)) {
            Uri uri = intent.getData();
            // view action
            if (action.equals(Intent.ACTION_VIEW)) {
                String host = uri.getHost();
                String path = uri.getPath();
                String query = uri.getQuery();

                if (uri != null && StringUtils.isNotEmpty(host)) {
                    if (host.equals(Macadamia.Keys.PARAM_KEY_APP)) {
                        if (path.contains(Macadamia.Urls.URI_PATH_RELOAD)) {
                            JSONObject json = JSON.parseObject(query);
                            String url = StringUtils.defaultIfEmpty(json.getString(Macadamia.Keys.PARAM_KEY_URL), Macadamia.Urls.WEB_URL_MAIN);
                            mainIntent.putExtra(Macadamia.Intent.KEY_LINK_URL, url);
                        } else {
                            mainIntent.putExtra(Macadamia.Intent.KEY_LINK_URL, uri.getQueryParameter(Macadamia.Keys.PARAM_KEY_SID));
                        }
                    }
                }
            }
            // App Shortcut 처리
            else if (action.equals(Macadamia.Intent.INTENT_ACTION_SHORTCUT)) {
                if (uri != null && uri.getHost() != null) {
                    if (uri.getHost().equals(Macadamia.Keys.PARAM_KEY_APP)) {
                        String strQueryParam = uri.getQueryParameter(Macadamia.Keys.PARAM_KEY_SID);
                        String strURL = Macadamia.Urls.WEB_URL_MAIN;
                        if (StringUtils.isNotEmpty(strQueryParam)) {
                            if (strQueryParam.equals(Macadamia.Shortcuts.SHORTCUT_NOONNOPPI)) {
                                strURL = Macadamia.Urls.WEB_URL_NOONNOPPI;
                            } else if (strQueryParam.equals(Macadamia.Shortcuts.SHORTCUT_SUMMIT_MATH)) {
                                strURL = Macadamia.Urls.WEB_URL_SUMMIT_MATH;
                            } else if (strQueryParam.equals(Macadamia.Shortcuts.SHORTCUT_CAIHONG)) {
                                strURL = Macadamia.Urls.WEB_URL_CAIHONG;
                            } else if (strQueryParam.equals(Macadamia.Shortcuts.SHORTCUT_SOLUNY)) {
                                strURL = Macadamia.Urls.WEB_URL_SOLUNY;
                            }
                            mainIntent.putExtra(Macadamia.Intent.KEY_LINK_URL, strURL);
                        }
                    }
                }
            }
            // PUSH Deep Link
            else if (action.equals(Macadamia.Intent.INTENT_ACTION_PUSH_LINK)) {
                Bundle pushBundle = intent.getExtras();
                if (pushBundle != null) {
                    mainIntent.putExtra(Macadamia.Intent.KEY_LINK_URL, pushBundle.getString(Macadamia.Intent.KEY_LINK_URL));
                }
            } else {
                if (intent.getExtras() != null) {
                    Bundle pushBundle = intent.getExtras();
                    if (pushBundle != null) {
                        action = Macadamia.Intent.INTENT_ACTION_PUSH_LINK;
                        mainIntent.putExtra(Macadamia.Intent.KEY_LINK_URL, pushBundle.getString(Macadamia.Keys.PARAM_KEY_URL));
                    }
                }
            }
            mainIntent.setAction(action);
        }

        return mainIntent;
    }

    /**
     * Permission View Button
     */
    private final View.OnClickListener buttonClickListener = (v) -> {
        if (v.getId() == R.id.button_launch_permission_ok) {
            this.displayPermissionView(false);
            // set Permission Checked
            Macadamia.Functions.setPermissionViewChecked(true);
            eventHandler.processNextEvent(null, DELAY_10);
        }
    };

    /**
     * show/hide 권한 VIEW
     *
     * @param visible visibility flag
     */
    private void displayPermissionView(boolean visible) {
        if (visible) {
            layoutPermission.setVisibility(View.VISIBLE);
        } else {
            layoutPermission.setVisibility(View.GONE);
        }
    }

    /**
     * 필수권한 체크
     */
    private void requestMandatoryPermission() {
        if (!MacadamiaUtils.getPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                !MacadamiaUtils.getPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // 필수
            askPermission(this, PERMISSIONS_LIST_MANDATORY).ask(new PermissionListener() {
                @Override
                public void onAccepted(PermissionResult permissionResult, List<String> accepted) {
                    if (permissionResult.isAccepted()) {
                        eventHandler.processEventById(MSG_LAUNCH_CHECK_PERMISSIONS_OPTIONAL, null, DELAY_10);
                    }
                }

                @Override
                public void onDenied(PermissionResult permissionResult, List<String> denied, List<String> foreverDenied) {
                    if (permissionResult.hasDenied() || permissionResult.hasForeverDenied()) {
                        MacadamiaAlertDialog.showAlert(LaunchActivity.this,
                                false,
                                getString(R.string.dialog_title_01),
                                getString(R.string.app_permission_mandatory),
                                getString(R.string.dialog_button_ok),
                                () -> finishCompletely());
                    }
                }
            });
        } else {
            eventHandler.processEventById(MSG_LAUNCH_CHECK_PERMISSIONS_OPTIONAL, null, DELAY_10);
        }
    }

    /**
     * 선택권한 체크
     */
    private void requestOptionalPermission() {
        if (!Macadamia.Functions.getRuntimePermissionChecked()) {
            askPermission(this, PERMISSIONS_LIST_OPTIONAL).ask(new com.github.florent37.runtimepermission.callbacks.PermissionListener() {
                @Override
                public void onAccepted(PermissionResult permissionResult, List<String> accepted) {
                    for (String permission : accepted) {
                        Macadamia.Functions.setPermissionChecked(permission, true);
                    }
                    Macadamia.Functions.setRuntimePermissionChecked(true);
                    eventHandler.processNextEvent(null, DELAY_10);
                }

                @Override
                public void onDenied(PermissionResult permissionResult, List<String> denied, List<String> foreverDenied) {
                    if (permissionResult.hasDenied()) {
                        for (String permission : denied) {
                            Macadamia.Functions.setPermissionChecked(permission, true);
                        }
                    }
                    MacadamiaAlertDialog.showAlert(LaunchActivity.this,
                            false,
                            "",
                            "선택적 접근권한은 해당 기능을 사용할 때 허용이 필요하며 비허용시에도 해당 기능 외 이용이 가능합니다.",
                            getString(R.string.dialog_button_ok),
                            () -> {
                                Macadamia.Functions.setRuntimePermissionChecked(false);
                                eventHandler.processNextEvent(null, DELAY_10);
                            });
                }
            });
        } else {
            eventHandler.processNextEvent(null, DELAY_10);
        }
    }

    /**
     * API get App Info
     */
    private void getAppInfo() {
        try {
            JSONObject param = new JSONObject();
            param.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
            param.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);
            param.put(Macadamia.Keys.PARAM_KEY_OS_VERSION, Macadamia.Constants.DEVICE_OS_VERSION);
            param.put(Macadamia.Keys.PARAM_KEY_DEVICE_MODEL, Macadamia.Constants.DEVICE_MODEL);

            macadamiaApi.request(MacadamiaRequestType.REQUEST_APP_INFO,
                    param,
                    requestAppInfoCallback);
        } catch (Exception e) {
            Logger.t(TAG).e("MSG_LAUNCH_REQUEST_APP_INFO : %s", e.getMessage());
            networkErrorAlertDialog(LaunchActivity.this,
                    getString(R.string.dialog_button_finish),
                    this::finish);
        }
    }

    /**
     * API get Menus
     */
    private void getMenus() {
        try {
            macadamiaApi.request(MacadamiaRequestType.REQUEST_MENU, new JSONObject(), requestMenuCallback);
        } catch (Exception e) {
            Logger.t(TAG).e("GET MENUS : %s", e.getMessage());
            networkErrorAlertDialog(LaunchActivity.this,
                    getString(R.string.dialog_button_finish),
                    this::finish);
        }
    }

    /**
     * process SSO Login
     */
    private void processSSOLogin() {
        try {
            SSOStatus ssoStatus = authCheck.encLogon(UserCredentialType.EncryptedBasicCredential,
                    getSSOCredential());

            if (ssoStatus == SSOStatus.SSOSuccess) {
                AuthSuccessResult ssoResult = (AuthSuccessResult) authCheck.getResult();
                if (ssoResult.getServerErrorCode() != ServerErrorCode.NoError) {
                    MacadamiaAlertDialog.showAlert(LaunchActivity.this,
                            false,
                            getString(R.string.dialog_title_01),
                            getString(R.string.dialog_message_sso_login_info_changed),
                            getString(R.string.dialog_button_ok),
                            () -> {
                                // 로그인 상태 설정
                                logOff(authCheck);
                                eventHandler.processNextEvent(null, DELAY_10);
                            });
                } else {
                    // 로그인 상태 설정
                    Macadamia.Constants.LOGGED = true;
                    // update push token
                    eventHandler.processEventById(MSG_LAUNCH_UPDATE_PUSH_TOKEN, null, DELAY_10);
                }
            } else if (ssoStatus == SSOStatus.SSOFail) {
                AuthFailResult ssoResult = (AuthFailResult) authCheck.getResult();
                setSSOCredential(Macadamia.Constants.STRING_BLANK);
                // 로그인 상태 설정
                logOff(authCheck);
                try {
                    HashMap<String, String> headerMap = new HashMap<>();
                    headerMap.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);

                    JSONObject params = new JSONObject();
                    params.put(Macadamia.Keys.PARAM_KEY_ERROR_TYPE, "AUTO_LOGON");
                    params.put("errorCode", "AUTO_LOGON");
                    params.put(Macadamia.Keys.PARAM_KEY_USER_ID, Macadamia.Functions.getUserCn());
                    params.put(Macadamia.Keys.PARAM_KEY_ERROR, String.format("[S:%s/A:%s/E:%s]%s(%s)|%s",
                            ssoResult.getServerErrorCode(),
                            ssoResult.getAppErrorCode(),
                            ssoResult.toString(),
                            Macadamia.Constants.DEVICE_MODEL,
                            Macadamia.Constants.DEVICE_OS_VERSION,
                            ""));

                    macadamiaApi.request(MacadamiaRequestType.REQUEST_ERROR_REPORT, headerMap, params, null);
                } catch (Exception e) {
                    Logger.t(TAG).e(e.getMessage());
                }
            }
        } catch (Exception e) {
            Logger.t(TAG).e("auto login Exception %s", e.getMessage());
            setSSOCredential(Macadamia.Constants.STRING_BLANK);
            // 로그인 상태 설정
            logOff(authCheck);
        }

        eventHandler.processNextEvent(null, DELAY_10);
    }

    /**
     * PUSH 토큰 업데이트
     */
    private void updatePushToken() {
        Logger.t(TAG).d("MacadamiaApi updatePushToken");
        try {
            JSONObject param = new JSONObject();
            param.put(Macadamia.Keys.PARAM_KEY_TOKEN, Macadamia.Functions.getPushToken());
            param.put("receivePush", Macadamia.Functions.getPushReceive());
            param.put(Macadamia.Keys.PARAM_KEY_APP_ID, Macadamia.Constants.APP_ID);
            param.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
            param.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);

            macadamiaApi.request(MacadamiaRequestType.UPDATE_PUSH_TOKEN,
                    macadamiaApi.getHeaderMap(getSSOHeader(getDeviceHeader(authCheck))),
                    param,
                    updatePushTokenCallback);
        } catch (Exception e) {
            Logger.t(TAG).e("API UPDATE_PUSH_TOKEN Exception", e);
            networkErrorAlertDialog(LaunchActivity.this,
                    getString(R.string.dialog_button_ok),
                    () -> {
                    });
        }
    }

    /**
     * APP 정보 조회 Callback
     */
    private final Callback<JSONObject> requestAppInfoCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(@NonNull Call<JSONObject> call, @NonNull Response<JSONObject> response) {
            Logger.t(TAG).d("API GET_APP_INFO RESP %s", response);
            if (response.isSuccessful()) {
                JSONObject data = response.body().getJSONObject(Macadamia.Keys.PARAM_KEY_DATA);
                Logger.t(TAG).d("API GET_APP_INFO RESP data : %s", data);
                popups = data.getJSONArray(Macadamia.Keys.PARAM_KEY_POPUPS);
                appVersion = data.getJSONObject(Macadamia.Keys.PARAM_KEY_VERSION);
                if (data.containsKey(Macadamia.Keys.PARAM_KEY_OUTLINK)) {
                    outlinks = data.getJSONArray(Macadamia.Keys.PARAM_KEY_OUTLINK);
                    if (outlinks.size() > 0) {
                        Macadamia.Functions.setOutlinks(outlinks);
                    }
                }
                // popups
                eventHandler.processNextEvent(null, DELAY_10);
            } else {
                dataLoadingErrorAlertDialog(LaunchActivity.this,
                        getString(R.string.dialog_button_ok),
                        () -> eventHandler.processPreviousEvent(null, DELAY_10));
            }
        }

        @Override
        public void onFailure(@NonNull Call<JSONObject> call, Throwable t) {
            Logger.t(TAG).e("[FAILURE] %s", t.getMessage());
            networkErrorAlertDialog(LaunchActivity.this,
                    getString(R.string.dialog_button_close),
                    () -> finish());
        }
    };

    /**
     * Menu Callback
     */
    private final Callback<JSONObject> requestMenuCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(@NonNull Call<JSONObject> call, Response<JSONObject> response) {
            if (response.isSuccessful()) {
                JSONObject data = response.body().getJSONObject(Macadamia.Keys.PARAM_KEY_DATA);
                Logger.t(TAG).d("API GET_MENU RESP data : %s", data);
                if (data != null) {
                    Macadamia.Functions.setMenu(data);
                }
                eventHandler.processNextEvent(null, DELAY_10);
            } else {
                dataLoadingErrorAlertDialog(LaunchActivity.this,
                        getString(R.string.dialog_button_ok),
                        () -> eventHandler.processPreviousEvent(null, DELAY_10));
            }
        }

        @Override
        public void onFailure(@NonNull Call<JSONObject> call, Throwable t) {
            Logger.t(TAG).e("[FAILURE] %s", t.getMessage());
            networkErrorAlertDialog(LaunchActivity.this,
                    getString(R.string.dialog_button_close),
                    () -> finish());
        }
    };

    /**
     * PUSH TOKEN Update Callback
     */
    private final Callback<JSONObject> updatePushTokenCallback = new Callback<JSONObject>() {
        @Override
        public void onResponse(@NonNull Call<JSONObject> call, @NonNull Response<JSONObject> response) {
            Logger.t(TAG).d("API UPDATE_PUSH_TOKEN RESP %s", response);
            if (response.isSuccessful()) {
                JSONObject data = response.body().getJSONObject(Macadamia.Keys.PARAM_KEY_DATA);
                Logger.t(TAG).d("API UPDATE_PUSH_TOKEN RESP data : %s", data);
            } else {
                Logger.t(TAG).e("API UPDATE_PUSH_TOKEN ERR");
                dataLoadingErrorAlertDialog(LaunchActivity.this,
                        getString(R.string.dialog_button_ok),
                        () -> {
                        });
            }
        }

        @Override
        public void onFailure(@NonNull Call<JSONObject> call, Throwable t) {
            Logger.t(TAG).e("[FAILURE] %s", t.getMessage());
            networkErrorAlertDialog(LaunchActivity.this,
                    getString(R.string.dialog_button_close),
                    () -> {
                    });
        }
    };
}
