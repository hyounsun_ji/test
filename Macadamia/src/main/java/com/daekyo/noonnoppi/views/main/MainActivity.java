package com.daekyo.noonnoppi.views.main;

import android.Manifest;
import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.Vibrator;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.JsResult;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.daekyo.noonnoppi.common.Macadamia.Constants;
import com.daekyo.noonnoppi.common.Macadamia.Functions;
import com.daekyo.noonnoppi.common.Macadamia.Urls;
import com.daekyo.noonnoppi.common.UriFragment;
import com.daekyo.noonnoppi.common.api.MacadamiaApi;
import com.daekyo.noonnoppi.common.api.MacadamiaRequestType;
import com.daekyo.noonnoppi.common.crypto.MacaAES;
import com.daekyo.noonnoppi.common.dialog.MacadamiaAlertDialog;
import com.daekyo.noonnoppi.common.utils.MacadamiaCryptoUtils;
import com.daekyo.noonnoppi.common.utils.MacadamiaScreenUtils;
import com.daekyo.noonnoppi.common.utils.MacadamiaUtils;
import com.daekyo.noonnoppi.views.MacadamiaActivity;
import com.daekyo.noonnoppi.views.orangedot.OrangeDotActivity;
import com.daekyo.noonnoppi.views.setting.SettingActivity;
import com.daekyo.noonnoppi.views.subview.SubActivity;
import com.github.florent37.runtimepermission.PermissionResult;
import com.github.florent37.runtimepermission.callbacks.PermissionListener;
import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindInt;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kr.co.nets.sso.appagent.android.ActivityGround;
import kr.co.nets.sso.appagent.android.AppErrorCode;
import kr.co.nets.sso.appagent.android.ArtifactSuccessResult;
import kr.co.nets.sso.appagent.android.AuthCheck;
import kr.co.nets.sso.appagent.android.AuthFailResult;
import kr.co.nets.sso.appagent.android.AuthSuccessResult;
import kr.co.nets.sso.appagent.android.SSOConfig;
import kr.co.nets.sso.appagent.android.SSOStatus;
import kr.co.nets.sso.appagent.android.ServerErrorCode;
import kr.co.nets.sso.appagent.android.UserCredentialType;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;
import static org.apache.commons.lang3.CharEncoding.UTF_8;

public class MainActivity extends MacadamiaActivity implements LocationListener {

  // region 100.Variables
  private static final String TAG = MainActivity.class.getSimpleName();

  private static final int MSG_MAIN_PROCESS_DEEP_LINK = 0x0100;
  private static final int MSG_MAIN_APP_BAR_SHOW = 0x0201;
  private static final int MSG_MAIN_APP_BAR_HIDE = 0x0202;
  private static final int MSG_MAIN_TITLE_BAR_SHOW = 0x0301;
  private static final int MSG_MAIN_TITLE_BAR_HIDE = 0x0302;
  private static final int MSG_MAIN_GET_USER_INFO = 0x0401;
  private static final int MSG_MAIN_AFTER_LOGIN = 0x0501;
  private static final int MSG_MAIN_LOCATION = 0x0601;
  private static final int MSG_MAIN_UPDATE_LOCATION = 0x0602;
  private static final int MSG_MAIN_FINISH = 0x0700;
  private static final int MSG_MAIN_UPDATE_PUSH_TOKEN = 0x0710;
  private static final int MSG_MAIN_LOGIN_ERROR_REPORT = 0x0720;
  private static final int MSG_MAIN_RETURN_TO_LOGIN = 0x0750;
  private static final int MSG_MAIN_RETURN_TO_MAIN = 0x0760;
  private static final int MSG_MAIN_HIDE_LOADING = 0x0999;
  private static final int MSG_MAIN_FINISH_APP = 0x9999;

  // 파일 권한 Request Code
  private static final int REQUEST_CODE_FILE_PERMISSION = 0x4100;
  private static final int REQUEST_CODE_LOCATION_PERMISSION = 0x4200;

  // Progress Dialog Hide Delay time
  private static final int LOADING_VIEW_HIDE_DELAY = 5000;

  // BackKey Flag
  private static boolean backKeyPressedFlag = false;
  private static boolean isTitleBarAnimated = false;
  private static boolean isAppBarAnimated = false;

  private static boolean canAnimateAppBar = true;

  private String mCameraPhotoPath;
  private static final String TYPE_IMAGE = "image/*";
  // File Chooser Result Callback Array
  private ValueCallback<Uri[]> webViewFilePathCallback;
  // Main URL
  private String mainURL;
  private LocationManager locationManager;

  private double longitude = Macadamia.GeoLocation.DK_LONGIGUDE;
  private double latitude = Macadamia.GeoLocation.DK_LATITUDE;
  private List<String> geoLocationProviders;


  @BindString(R.string.app_id)
  String APP_ID;
  @BindInt(R.integer.delay_0)
  int DELAY_0;
  @BindInt(R.integer.delay_10)
  int DELAY_10;
  @BindInt(R.integer.delay_100)
  int DELAY_100;
  @BindInt(R.integer.delay_200)
  int DELAY_200;
  @BindInt(R.integer.delay_500)
  int DELAY_500;
  @BindInt(R.integer.delay_2000)
  int DELAY_2000;

  @BindView(R.id.layout_main)
  View mainLayout;
  @BindView(R.id.webview_main)
  WebView mainWebView;
  @BindView(R.id.layout_main_00)
  View statusBarLayout;
  @BindView(R.id.layout_main_title_container)
  View titleBarLayout;
  @BindView(R.id.layout_main_bottom_container)
  View bottomContainerLayout;
  @BindView(R.id.layout_main_99)
  View navigationBarLayout;
  DrawerLayout drawerLayout;
  @BindView(R.id.button_maca_logo)
  ImageButton buttonMacadamiaLogo;

  final Button[] menuItemButtons = new Button[menuItemIds.length];
  private static final int[] MSG_MAIN_LIST = {
    0
  };
  // endregion

  // region 200.Activity LifeCycle
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Logger.t(TAG).d("[onCreate]");
    setContentView(R.layout.activity_main);
    // set ApplicationContext
    ctx = getApplicationContext();
    // bind ButterKnife
    unbinder = ButterKnife.bind(this);
    // initialize menu
    initMenus();
    // Screen 초기화
    initScreenView();
    // SSO
    authCheck = new AuthCheck();
    // webview setting
    initWebViewSetting();
    // get intent action
    String action = getIntent().getAction();
    Logger.t(TAG).e("main action %s", action);

    if (StringUtils.isEmpty(action)) {
      mainURL = Macadamia.Urls.WEB_URL_MAIN;
    } else {
      if (getIntent().hasExtra(Macadamia.Intent.KEY_LINK_URL)) {
        mainURL = getIntent().getExtras()
          .getString(Macadamia.Intent.KEY_LINK_URL, Macadamia.Urls.WEB_URL_MAIN);
      } else {
        mainURL = Macadamia.Urls.WEB_URL_MAIN;
      }
    }
    // load main URL
    mainWebView.loadUrl(mainURL, getSSOHeader(getDeviceHeader(authCheck)));
  }

  /**
   * initialize webview settings
   */
  private void initWebViewSetting() {
    //mainWebView.setScrollListener(mainWebViewScrollListener);
    WebSettings settings = mainWebView.getSettings();
    // WebView Setting
    settings.setAllowContentAccess(true);
    settings.setAllowFileAccess(true);
    settings.setJavaScriptEnabled(true);
    settings.setLoadsImagesAutomatically(true);
    settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
    settings.setDefaultTextEncodingName(UTF_8);
    settings.setGeolocationEnabled(true);
    settings.setBuiltInZoomControls(false);
    // http resource 호출
    settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
    settings.setAppCacheEnabled(true);
    settings.setLoadWithOverviewMode(true);
    settings.setUseWideViewPort(true);
    settings.setSaveFormData(false);
    settings.setSupportMultipleWindows(true);
    settings.setDisplayZoomControls(false);
    settings.setSupportZoom(false);
    settings.setJavaScriptCanOpenWindowsAutomatically(true);
    // WebView LocalStorage
    settings.setDatabaseEnabled(true);
    // App Cache Path 설정
    settings.setAppCachePath(MacadamiaUtils.getAppCachePath(ctx));
    settings.setDomStorageEnabled(true);
    // ScrollBar
    mainWebView.setVerticalScrollBarEnabled(true);
    mainWebView.setHorizontalScrollBarEnabled(false);

    mainWebView.getSettings().setUserAgentString(Macadamia.Constants.USER_AGENT);
    mainWebView.setWebViewClient(webViewClient);
    mainWebView.getSettings().setGeolocationEnabled(true);
    mainWebView.setWebChromeClient(webChromeClient);
    mainWebView.setOnLongClickListener(v -> (true));
    mainWebView.setNetworkAvailable(true);
    mainWebView.setDownloadListener(downloadListener);
  }

  @Override
  protected void onStart() {
    super.onStart();
    Logger.t(TAG).d("[onStart]");
  }

  @Override
  protected void onResume() {
    super.onResume();
    Logger.t(TAG).d("[onResume] %s", Macadamia.Constants.LOGGED);

    ActivityGround.setactivityResumed();
    // 실행 권한
    if (!MacadamiaUtils.getPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
      !MacadamiaUtils.getPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
      MacadamiaAlertDialog.showAlert(MainActivity.this,
        false,
        getString(R.string.dialog_title_01),
        getString(R.string.app_permission_mandatory),
        getString(R.string.dialog_button_ok),
        () -> {
          resetUserInfo(authCheck);
          finishCompletely();
        });
    }

    // 현재 위치 정보 조회
    eventHandler.processEventById(MSG_MAIN_LOCATION, null, DELAY_10);

    Logger.t(TAG).e("STAYING TIME %d", Macadamia.Constants.APP_BACKGROUND_STAYING_TIME);

    if (Macadamia.Constants.APP_BACKGROUND_STAYING_TIME > Macadamia.Constants.EXPIRE_SECONDS) {
      if (Macadamia.Constants.LOGGED) {
        if (Macadamia.Functions.getAutoLogin()) {
          try {
            SSOStatus status = authCheck
              .encLogon(UserCredentialType.EncryptedBasicCredential, getSSOCredential());
            if (status == SSOStatus.SSOSuccess) {
              Macadamia.Constants.LOGGED = true;
              mainWebView.reload();
            } else {
              MacadamiaAlertDialog.showAlert(MainActivity.this,
                false,
                getString(R.string.dialog_title_01),
                getString(R.string.dialog_message_login_fail_02),
                getString(R.string.dialog_button_ok),
                () -> {
                  logOff(authCheck);
                  this.mainWebView.loadUrl(Macadamia.Urls.WEB_URL_LOGOFF,
                    getSSOHeader(getDeviceHeader(authCheck)));
                });
            }
          } catch (Exception e) {
            e.printStackTrace();
            logOff(authCheck);
            this.mainWebView
              .loadUrl(Macadamia.Urls.WEB_URL_LOGOFF, getSSOHeader(getDeviceHeader(authCheck)));
          }
        } else {
          MacadamiaAlertDialog.showAlert(MainActivity.this,
            false,
            getString(R.string.dialog_title_01),
            getString(R.string.dialog_message_login_expired),
            getString(R.string.dialog_button_ok),
            () -> {
              logOff(authCheck);
              this.mainWebView.loadUrl(Macadamia.Urls.WEB_URL_LOGOFF,
                getSSOHeader(getDeviceHeader(authCheck)));
            });
        }
      } else {
        logOff(authCheck);
        this.mainWebView
          .loadUrl(Macadamia.Urls.WEB_URL_LOGOFF, getSSOHeader(getDeviceHeader(authCheck)));
      }

      Macadamia.Constants.APP_BACKGROUND_STAYING_TIME = 0;
    }
  }

  @Override
  protected void onRestart() {
    super.onRestart();
    Logger.t(TAG).d("[onRestart]");
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    Logger.t(TAG).d("[onDestroy]");
    // ButterKnife Unbind
    if (unbinder != null) {
      unbinder.unbind();
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    Logger.t(TAG).d("[onPause]");
    ActivityGround.setactivityPaused();
  }

  @Override
  public void onBackPressed() {
    doBack();
  }

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    Logger.t(TAG).d("[onNewIntent] %s", intent);

    String action = intent.getAction();

    if (action.equals(Macadamia.Intent.INTENT_ACTION_PUSH_LINK) ||
      action.equals(Intent.ACTION_VIEW)) {
      String pushLinkUrl = intent.getExtras().getString(Macadamia.Intent.KEY_LINK_URL);
      mainWebView.loadUrl(pushLinkUrl, getSSOHeader(getDeviceHeader(authCheck)));
    }
  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    Logger.t(TAG).d("[onWindowFocusChanged] %s", hasFocus);
  }
  // endregion

  // region 300.Activity Delegate
  @Override
  public void initActivity() {
    Logger.t(TAG).d("- initialize Activity");
    // Activity Event Handler
    eventHandler = new MacaEventHandler(this, MSG_MAIN_LIST);
    // MacadamiaAPI
    macadamiaApi = new MacadamiaApi(ctx);
  }
  // endregion

  // region 400.WebViewClient

  /**
   * Custom URL Scheme 처리
   *
   * @param view WebView
   * @param uri  Uri
   * @return true / false
   */
  private boolean handleMacadamiaUrlScheme(@NonNull WebView view, @NonNull Uri uri) {
    Bundle urlSchemeBundle = new Bundle();
    urlSchemeBundle.putString(Macadamia.Keys.LINK_URL, uri.toString());
    urlSchemeBundle.putInt(Macadamia.Keys.PARAM_KEY_TARGET, view.getId());

    Message urlSchemeMessage = new Message();
    urlSchemeMessage.setData(urlSchemeBundle);
    eventHandler.processEventById(MSG_MAIN_PROCESS_DEEP_LINK, urlSchemeMessage, DELAY_0);
    return true;
  }

  /**
   * ISP 결제 URL Handler
   *
   * @param view WebView
   * @param uri  URI
   * @return handle URI result
   */
  private boolean handleISPUrlScheme(@NonNull WebView view, @NonNull Uri uri) {
    Logger.t(TAG_INICIS).d("WebViewClient handleISPUrlScheme : %s", uri);
    Intent payIntent;
    String dataString;
    String packageName;

    try {
      payIntent = Intent.parseUri(uri.toString(), Intent.URI_INTENT_SCHEME);
      dataString = payIntent.getDataString();
      packageName = payIntent.getPackage();
      Logger.t(TAG_INICIS).d("URI_HOST: pay : %s %s", payIntent, packageName);
      // forbid launching activities without BROWSABLE category
      payIntent.addCategory(Intent.CATEGORY_BROWSABLE);
      // forbid explicit call
      payIntent.setComponent(null);
      // forbid Intent with selector Intent
      payIntent.setSelector(null);
    } catch (URISyntaxException e) {
      Logger.t(TAG_INICIS).e("PAY URISyntaxException : %s", e.getMessage());
      return true;
    }

    Uri payUri = Uri.parse(dataString);
    payIntent = new Intent(Intent.ACTION_VIEW, payUri);

    try {
      startActivity(payIntent);
    } catch (ActivityNotFoundException e) {
      Logger.t(TAG_INICIS).e("PayUri ActivityNotFoundException: %s", payUri);
      String PAY_SCHEME = payUri.getScheme();
      Logger.t(TAG_INICIS).e("PayUri ActivityNotFoundException: %s", PAY_SCHEME);
      // ISP
      if (PAY_SCHEME.equals("ispmobile")) {
        showPaymentAppErrorAlert("ISP", packageName, false);
        return true;
      }
      // 현대앱카드
      else if (PAY_SCHEME.equals("hdcardappcardansimclick")) {
        showPaymentAppErrorAlert("현대앱카드", packageName, false);
        return true;
      }
      // 신한앱카드
      else if (PAY_SCHEME.equals("mpocket.online.ansimclick")) {
        showPaymentAppErrorAlert("신한앱카드", packageName, false);
        return true;
      }
      // 삼성앱카드
      else if (PAY_SCHEME.equals("shinhan-sr-ansimclick")) {
        showPaymentAppErrorAlert("삼성앱카드", packageName, false);
        return true;
      }
      // 롯데 모바일결제
      else if (PAY_SCHEME.equals("lottesmartpay")) {
        showPaymentAppErrorAlert("롯데앱카드", packageName, false);
        return true;
      }
      // 롯데앱카드(간편결제)
      else if (PAY_SCHEME.equals("lotteappcard")) {
        showPaymentAppErrorAlert("롯데앱카드(간편결제)", packageName, false);
        return true;
      }
      // KB국민 앱카드
      else if (PAY_SCHEME.equals("kb-acp")) {
        showPaymentAppErrorAlert("KB국민 앱카드", packageName, false);
        return true;
      }
      // 하나SK카드 통합안심클릭앱
      else if (PAY_SCHEME.equals("hanaansim")) {
        showPaymentAppErrorAlert("하나SK카드 통합안심클릭앱", packageName, false);
        return true;
      }
      // 하나1Q페이
      else if (PAY_SCHEME.equals("cloudpay")) {
        showPaymentAppErrorAlert("하나1Q페이 간편결제", packageName, false);
        return true;
      }
      // 시티 스마트 간편결제
      else if (PAY_SCHEME.equals("citimobileapp")) {
        showPaymentAppErrorAlert("시티 스마트 간편결제", packageName, false);
        return true;
      }
      // NH카드 올뤈페이
      else if (PAY_SCHEME.equals("nhallonepayansimclick")) {
        showPaymentAppErrorAlert("NH카드 올원페이", packageName, false);
        return true;
      }
      // 현대카드 백신앱
      else if (PAY_SCHEME.equals("droidxantivirusweb")) {
        /*************************************************************************************/
        Logger.t(TAG_INICIS).e("<INIPAYMOBILE>",
          "ActivityNotFoundException, droidxantivirusweb 문자열로 인입될시 마켓으로 이동되는 예외 처리: ");
        /*************************************************************************************/

        Intent hydVIntent = new Intent(Intent.ACTION_VIEW);
        hydVIntent.setData(Uri.parse("market://search?q=net.nshc.droidxantivirus"));
        startActivity(hydVIntent);
        return true;
      }
      // 계좌이체 - 간편결제
      else if (PAY_SCHEME.equals("kftc-bankpay")) {
        showPaymentAppErrorAlert("뱅크페이", packageName, false);
        return true;
      }
      // 계좌이체 - KB은행
      else if (PAY_SCHEME.equals("kb-bankpay")) {
        showPaymentAppErrorAlert("국민은행", packageName, false);
        return true;
      }
      // 계좌이체 - NH농협
      else if (PAY_SCHEME.equals("nhb-bankpay")) {
        showPaymentAppErrorAlert("NH앱캐시", packageName, false);
        return true;
      }
      // 계좌이체 - 새마을금고
      else if (PAY_SCHEME.equals("mg-bankpay")) {
        showPaymentAppErrorAlert("MG상상뱅크", packageName, false);
        return true;
      }
      // 계좌이체 - 경남은행
      else if (PAY_SCHEME.equals("kn-bankpay")) {
        showPaymentAppErrorAlert("BNK경남은행", packageName, false);
        return true;
      }
      // 페이코
      else if (PAY_SCHEME.equals("payco")) {
        showPaymentAppErrorAlert("페이코", packageName, false);
        return true;
      } else {
        showPaymentAppErrorAlert("페이코", packageName, true);
        Logger.t(TAG_INICIS).e("없음");
        return true;
      }
    }
    return true;
  }

  /**
   * Handle URL
   *
   * @param view Webview
   * @param uri  URI
   * @return handle URI result
   */
  private boolean handleUrlLoading(WebView view, Uri uri) {
    // region
    Logger.t(TAG).d("WebViewClient handleUrlLoading %s", uri);
    Logger.t(TAG).d("URI_SCHEME : %s", uri);
    Logger.t(TAG).d("URI_SCHEME Scheme : %s", uri.getScheme());
    Logger.t(TAG).d("URI_SCHEME Host : %s", uri.getHost());
    Logger.t(TAG).d("URI_SCHEME Port : %d", uri.getPort());

    Logger.t(TAG).d("URI_SCHEME LastPathSegment : %s", uri.getLastPathSegment());
    Logger.t(TAG).d("URI_SCHEME PathSegments : %s", uri.getPathSegments());

    Logger.t(TAG).d("URI_SCHEME EncodedAuthority : %s", uri.getEncodedAuthority());
    Logger.t(TAG).d("URI_SCHEME EncodedFragment : %s", uri.getEncodedFragment());
    Logger.t(TAG).d("URI_SCHEME EncodedPath : %s", uri.getEncodedPath());
    Logger.t(TAG).d("URI_SCHEME EncodedQuery : %s", uri.getEncodedQuery());
    Logger.t(TAG)
      .d("URI_SCHEME EncodedSchemeSpecificPart : %s", uri.getEncodedSchemeSpecificPart());
    Logger.t(TAG).d("URI_SCHEME EncodedUserInfo : %s", uri.getEncodedUserInfo());

    Logger.t(TAG).d("URI_SCHEME Authority : %s", uri.getAuthority());
    Logger.t(TAG).d("URI_SCHEME Fragment : %s", uri.getFragment());
    Logger.t(TAG).d("URI_SCHEME Path : %s", uri.getPath());
    Logger.t(TAG).d("URI_SCHEME Query : %s", uri.getQuery());
    Logger.t(TAG).d("URI_SCHEME SchemeSpecificPart : %s", uri.getSchemeSpecificPart());
    Logger.t(TAG).d("URI_SCHEME UserInfo : %s", uri.getUserInfo());
    // endregion

    if (uri != null) {
      final String URI_SCHEME = uri.getScheme();
      final String URI_HOST = uri.getHost();
      final String URI_SCHEME_SPECIFIC_PART = uri.getSchemeSpecificPart();
      final String URI_FRAGMENT = uri.getFragment();
      final String URI_PATH = uri.getPath();

      if (URI_SCHEME.equals("http") || URI_SCHEME.equals("https") || URI_SCHEME.equals("javascript")) {
        // VGUARD 백신 - 삼성카드
        if (URI_HOST.contains("m.vguard.co.kr")) {
          MacadamiaUtils.goPlayStore(MainActivity.this, "kr.co.shiftworks.vguardweb");
        }
        // 이니시스 결제 에러
        else if (URI_HOST.contains("inicis.com") && URI_PATH.contains("resLogging")) {
          view.loadUrl(Macadamia.Urls.WEB_URL_MAIN, getSSOHeader(getDeviceHeader(authCheck)));
        } else if (URI_HOST.contains(Urls.WEB_URL_NOONNOPPI_LEARNING_HOST)) {
          if (Macadamia.Constants.LOGGED) {
            doOutLinkWithSSO(uri.toString(), true);
            eventHandler.processEventById(MSG_MAIN_RETURN_TO_MAIN, null, DELAY_10);
          }
          return true;
        } else if (URI_HOST.contains("customer.daekyo.com") && URI_PATH.contains("/daekyocom/customer")) {
          doOutLinkWithSSO(uri.toString(), false);
          return true;
        } else if (URI_HOST.contains(Urls.WEB_URL_CS_CENTER_HOST)) {
          doOutLinkWithSSO(uri.toString(), false);
          return true;
        } else {
          view.loadUrl(uri.toString(), getSSOHeader(getDeviceHeader(authCheck)));
        }
        return false;
      } else if (URI_SCHEME.equals("macadamia")) {
        // 간편결제 인앱
        // TODO: paymentreslt host
        if (URI_HOST.equals("paymentresult")) {
          try {
            final String URI_QUERY = URLEncoder.encode(uri.getQuery(), "UTF-8");
            view.postUrl("", Base64.encode(URI_QUERY.getBytes(), Base64.DEFAULT));
            return true;
          } catch (UnsupportedEncodingException e) {
            Logger.t(TAG).e("<INIPAYMOBILE> UnsupportedEncodingException %s", e);
            return false;
          }
        } else {
          return handleMacadamiaUrlScheme(view, uri);
        }
      } else if (URI_SCHEME.equals("intent")) {
        // 카카오톡 공유하기 url 변경에 따른 수정
        // intent:kakaolink://send?appkey=7bfd428e04210eb08b74152b33c1e65c&appver=1 ...
        if (URI_SCHEME_SPECIFIC_PART.startsWith("kakaolink")) {
          try {
            int intentCharactersLength = "intent:".length();
            String kakaoIntentString = uri.toString().substring(intentCharactersLength);
            Intent kakaoIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(kakaoIntentString));
            kakaoIntent.addCategory(Intent.CATEGORY_BROWSABLE);
            kakaoIntent.setComponent(null);
            kakaoIntent.setSelector(null);
            startActivity(kakaoIntent);
          } catch (ActivityNotFoundException ane) {
            Toast.makeText(ctx, getString(R.string.msg_kakao_not_found), Toast.LENGTH_LONG).show();
            Logger.t(TAG).e("KakaoTalk Not Found Exception : %s", ane);
          } catch (Exception e) {
            Toast.makeText(ctx, getString(R.string.msg_kakao_error), Toast.LENGTH_LONG).show();
            Logger.t(TAG).e("KakaoTalk Exception : %s", e);
          }
          return true;
        }
        // 이니시스 간편 결제 - 현대카드
        else if (URI_SCHEME_SPECIFIC_PART.startsWith("hdcardappcardansimclick")) {
          return handleISPUrlScheme(view, uri);
        }
        // 이니시스 간편 결제 - 앱카드
        else if (URI_HOST.equals("pay")) {
          return handleISPUrlScheme(view, uri);
        }
        // etc
        else {
          /*
          본인인증
          SKT  : intent://sktauth?agentTID=NI202002041395105663&appToken=MOcjYzip3dtsaxf5WHni#Intent;scheme=tauthlink;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.sktelecom.tauth;end
          KT   : intent://requestktauth?appToken=21127E38C8230CF74947#Intent;scheme=ktauthexternalcall;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.kt.ktauth;end
          LGU+ : intent://?SID=20020420114649547&appToken=2002041146495713F57A#Intent;scheme=upluscorporation;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.lguplus.smartotp;end
          */
          UriFragment uriFragment = UriFragment.parseFragment(URI_FRAGMENT);
          String packageName = uriFragment.getPackage();

          if (packageName.contains("com.sktelecom.tauth") ||
            packageName.contains("com.kt.ktauth") ||
            packageName.contains("com.lguplus.smartotp")) {
            try {
              Intent authIntent = Intent.parseUri(uri.toString(), Intent.URI_INTENT_SCHEME);
              if (authIntent != null) {
                if (MacadamiaUtils.isPackageInstalled(ctx, packageName)) {
                  startActivity(authIntent);
                } else {
                  MacadamiaUtils.goPlayStore(MainActivity.this, packageName);
                }
              }
            } catch (URISyntaxException e) {
              Logger.t(TAG_NICE_AUTH).e("URISyntaxException %s", e);
            } catch (ActivityNotFoundException e) {
              Logger.t(TAG_NICE_AUTH).e("ActivityNotFoundException %s", e);
              MacadamiaUtils.goPlayStore(MainActivity.this, packageName);
            } catch (Exception e) {
              Logger.t(TAG_NICE_AUTH).e("Exception %s", e);
            }
          } else {
            Intent etcIntent = new Intent(Intent.ACTION_VIEW, uri);
            // forbid launching activities without BROWSABLE category
            etcIntent.addCategory(Intent.CATEGORY_BROWSABLE);
            // forbid explicit call
            etcIntent.setComponent(null);
            // forbid Intent with selector Intent
            etcIntent.setSelector(null);

            try {
              startActivity(etcIntent);
            } catch (ActivityNotFoundException ane) {
              Logger.t(TAG_INICIS).e("INIPAYMOBILE, ActivityNotFoundException INPUT >> %s", uri);
              Logger.t(TAG_INICIS).e("INIPAYMOBILE, uri.getScheme() %s", URI_SCHEME);
              return handleISPUrlScheme(view, uri);
            }
          }
          return true;
        }
      } else if (URI_SCHEME.equals("tel")
        || URI_SCHEME.equals("sms")
        || URI_SCHEME.equals("smsto")
        || URI_SCHEME.equals("mms")
        || URI_SCHEME.equals("mmsto")
        || URI_SCHEME.equals("mailto")
        || URI_SCHEME.equals("geo")
        || URI_SCHEME.equals("whatsapp")
        || URI_SCHEME.equals("market")
        || URI_SCHEME.startsWith("vnd.youtube")) {
        MacadamiaUtils.invokeNativeApp(MainActivity.this, uri.toString());
        return true;
      } else {
        Logger.t(TAG).d("ETC URI_SCHEME : %s", URI_SCHEME);
        return true;
      }
    } else {
      return true;
    }
  }

  /**
   * WebViewClient
   */
  private final WebViewClient webViewClient = new WebViewClient() {
    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
      Logger.t(TAG).d("WebViewClient shouldOverrideUrlLoading request : %s", request.getUrl());
      return handleUrlLoading(view, request.getUrl());
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
      Logger.t(TAG).d("WebViewClient shouldOverrideUrlLoading url : %s", url);
      url = StringUtils.defaultIfEmpty(url, Macadamia.Urls.WEB_URL_MAIN);
      return handleUrlLoading(view, Uri.parse(url));
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
      super.onPageStarted(view, url, favicon);
      Logger.t(TAG).d("WebViewClient onPageStarted : %s", url);
      // show loading view
      startLoading(MainActivity.this, null);
      eventHandler.processEventById(MSG_MAIN_HIDE_LOADING, null, LOADING_VIEW_HIDE_DELAY);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
      //super.onPageFinished(view, url);
      Logger.t(TAG).d("webview onPageFinished %s", url);
      //hideOrangeDotMenu(url);
      stopLoading();
      //eventHandler.processEventById(MSG_MAIN_GET_USER_INFO, null, DELAY_10);
    }

    @Override
    public void onLoadResource(WebView view, String url) {
      super.onLoadResource(view, url);
      Logger.t(TAG).d("WebViewClient onLoadResource : %s [%d]", url, url.getBytes().length);
    }

    @Override
    public void onPageCommitVisible(WebView view, String url) {
      super.onPageCommitVisible(view, url);
      Logger.t(TAG).d("WebViewClient onPageCommitVisible : %s", url);
    }

    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
      super.onReceivedError(view, request, error);
      if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
        Logger.t(TAG).e("WebViewClient onReceivedError( %d ) : %s %s", error.getErrorCode(),
          error.getDescription(), view.getUrl());
      }
      // 2020.09.28 error page 제외
      //view.loadUrl("file:///android_asset/html/400.html");
    }

    @Override
    public void onReceivedHttpError(WebView view, WebResourceRequest request,
                                    WebResourceResponse errorResponse) {
      super.onReceivedHttpError(view, request, errorResponse);
      Logger.t(TAG).e("WebViewClient onReceivedHttpError( %d ) : %s", errorResponse.getStatusCode(),
        view.getUrl());
      // 2020.09.28 error page 제외
      //view.loadUrl("file:///android_asset/html/500.html");
    }

    @Override
    public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
      super.doUpdateVisitedHistory(view, url, isReload);
      Logger.t(TAG).d("WebViewClient doUpdateVisitedHistory %s %s", url, isReload);
    }

    @Override
    public void onFormResubmission(WebView view, Message dontResend, Message resend) {
      super.onFormResubmission(view, dontResend, resend);
      Logger.t(TAG).d("WebViewClient onFormResubmission %s %s", dontResend, resend);
    }

    @Override
    public void onScaleChanged(WebView view, float oldScale, float newScale) {
      super.onScaleChanged(view, oldScale, newScale);
      Logger.t(TAG).d("WebViewClient onScaleChanged %f %f", oldScale, newScale);
    }
  };
  // endregion

  // region 500.WebChromeClient
  /**
   * WebChromeClient
   */
  private final WebChromeClient webChromeClient = new WebChromeClient() {
    @Override
    public void onProgressChanged(WebView view, int newProgress) {
      super.onProgressChanged(view, newProgress);
      Logger.t(TAG).d("WebChromeClient onProgressChanged %d", newProgress);
    }

    @Override
    public void onCloseWindow(WebView window) {
      Logger.t(TAG).e("window close1");
      super.onCloseWindow(window);
      window.destroy();
    }

    @Override
    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture,
                                  Message resultMsg) {
      Logger.t(TAG).d("WebChromeClient onCreateWindow");
      // Dialog Create Code
      WebView newWebView = new WebView(view.getContext());
      newWebView.getSettings().setJavaScriptEnabled(true);
      newWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
      newWebView.getSettings().setSupportMultipleWindows(true);
      newWebView.getSettings().setDomStorageEnabled(true);
      newWebView.setHorizontalScrollBarEnabled(true);
      newWebView.setVerticalScrollBarEnabled(true);

      final Dialog dialog = new Dialog(view.getContext());
      dialog.setContentView(newWebView);

      WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
      params.width = ViewGroup.LayoutParams.MATCH_PARENT;
      params.height = (int) (MacadamiaScreenUtils.getScreenHeight(view.getContext()) * 0.8);
      dialog.getWindow().setAttributes(params);
      dialog.setCancelable(true);
      dialog.show();

      newWebView.setWebChromeClient(new WebChromeClient() {
        @Override
        public void onCloseWindow(WebView window) {
          Logger.t(TAG).e("window close2");
          dialog.dismiss();
          window.destroy();
        }
      });

      dialog.setOnDismissListener(dialogInterface -> {
        Logger.t(TAG).e("window ondismiss");
        newWebView.destroy();
        dialogInterface.dismiss();
        dialogInterface.cancel();
      });

      newWebView.setWebViewClient(new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
          String currentUrl = view.getUrl();
          Logger.t(TAG_SSO).d("onCreateWindow : %s", currentUrl);

          Bundle linkBundle = new Bundle();
          Message linkMessage = new Message();

          if (currentUrl.contains(Urls.WEB_URL_NOONNOPPI_LEARNING_HOST)) {
            if (Macadamia.Constants.LOGGED) {
              dialog.dismiss();
              linkBundle.putBoolean("useSSO", true);
              doOutLinkWithSSO(currentUrl, true);
            } else {
              dialog.dismiss();
              Bundle bundle = new Bundle();
              bundle.putString(Macadamia.Keys.PARAM_KEY_RETURN_URL, currentUrl);
              Message msg = new Message();
              msg.setData(bundle);
              eventHandler.processEventById(MSG_MAIN_RETURN_TO_LOGIN, msg, DELAY_0);
            }
            return true;
          } else if (currentUrl.contains("customer.daekyo.com/daekyocom/customer")) {
            dialog.dismiss();
            linkBundle.putBoolean("useSSO", false);
            doOutLinkWithSSO(currentUrl, false);
            return true;
          } else if (currentUrl.contains("noonnoppi.com/Mobile/WrongProblem_Popup_All.aspx") ||
            currentUrl.contains("kmcert.com/kmcis/mobile_v2/kmcisHp00.jsp") ||
            currentUrl.contains("www.noonnoppi.com") ||
            currentUrl.contains("summit.daekyo.com") ||
            currentUrl.contains("caihong.daekyo.com") ||
            currentUrl.contains("www.soluny.com") ||
            currentUrl.contains("www.teachinglab.co.kr") ||
            currentUrl.contains("www.daekyospeaking.com") ||
            currentUrl.contains("dreammentor1.modoo.at") ||
            currentUrl.contains("clinic.daekyo.com") ||
            currentUrl.contains("vanvo.co.kr") ||
            currentUrl.contains("artimom.noonnoppi.com") ||
            currentUrl.contains("www.hsk-korea.co.kr") ||
            currentUrl.contains("www.sobics.com") ||
            currentUrl.contains("daekyo.co.kr") ||
            (currentUrl.contains("daekyo.com") && !currentUrl.contains("sso3.daekyo.com") && !currentUrl.contains("d-sso6.daekyo.com"))
          ) {
            dialog.dismiss();
            linkBundle.putBoolean("useSSO", true);
            doOutLinkWithSSO(currentUrl, true);
            return true;
          } else if (currentUrl.contains("knowreapi.com") ||
            currentUrl.contains("naver.com") ||
            currentUrl.contains("bbakdok.com") ||
            currentUrl.contains("instagram.com") ||
            currentUrl.contains("facebook.com") ||
            currentUrl.contains("kakao.com") ||
            currentUrl.contains("youtube.com") ||
            currentUrl.contains("teuni.com") ||
            currentUrl.contains("boox.kr") ||
            currentUrl.contains(Urls.WEB_URL_CS_CENTER_HOST) ||
            currentUrl.contains("iniweb.inicis.com/app/publication/apReceipt.jsp")
          ) {
            dialog.dismiss();
            linkBundle.putBoolean("useSSO", false);
            doOutLinkWithSSO(currentUrl, false);
            return true;
          }
          return false;
        }
      });

      ((WebView.WebViewTransport) resultMsg.obj).setWebView(newWebView);
      resultMsg.sendToTarget();
      return true;
    }

    @Override
    public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
      Logger.t(TAG).d("WebChromeClient onJsAlert %s %s", url, message);
      MacadamiaAlertDialog.showAlert(MainActivity.this,
        false,
        getString(R.string.dialog_title_01),
        message,
        getString(R.string.dialog_button_ok), result::confirm);
      return true;
    }

    @Override
    public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
      Logger.t(TAG).d("WebChromeClient onJsConfirm %s %s", url, message);
      MacadamiaAlertDialog.showConfirm(MainActivity.this,
        false,
        getString(R.string.dialog_title_01),
        message,
        getString(R.string.dialog_button_ok), result::confirm,
        getString(R.string.dialog_button_cancel), result::cancel);
      return true;
    }

    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback,
                                     FileChooserParams fileChooserParams) {
      Logger.t(TAG).d("WebChromeClient onShowFileChooser");
      // clear File Chooser Result
      setFileChooserResult(null);
      webViewFilePathCallback = filePathCallback;
      // Image Choose
      showImageChooser();
      return true;
    }
  };
  // endregion

  // 600.Event Handler
  @Override
  public void handleEventMessage(@NonNull Message msg) {
    switch (msg.what) {
      case MSG_MAIN_FINISH_APP:
        backKeyPressedFlag = false;
        break;

      case MSG_MAIN_PROCESS_DEEP_LINK:
        Logger.t(TAG).d("MSG_MAIN_PROCESS_DEEP_LINK %s", msg);

        Bundle data = msg.getData();
        String linkUrl = data.getString(Macadamia.Keys.LINK_URL);
        int targetId = data.getInt(Macadamia.Keys.PARAM_KEY_TARGET, mainWebView.getId());

        Uri uri = Uri.parse(linkUrl);
        String path = uri.getPath();
        String query = uri.getQuery();

        JSONObject json = JSON.parseObject(query);

        if (path.contains(Macadamia.Urls.URI_PATH_SHARE)) {
          String title = StringUtils.defaultIfEmpty(json.getString(Macadamia.Keys.PARAM_KEY_TITLE),
            getString(R.string.app_name));
          String url = StringUtils.defaultIfEmpty(json.getString(Macadamia.Keys.PARAM_KEY_URL),
            Macadamia.Urls.WEB_URL_MAIN);
          Intent paramIntent = new Intent();
          paramIntent.setAction(Intent.ACTION_SEND);
          paramIntent.setType(Macadamia.Constants.HEADER_ACCEPT_PLAIN);
          paramIntent.putExtra(Intent.EXTRA_TEXT, title);
          paramIntent.putExtra(Intent.EXTRA_TEXT, url);

          Intent shareIntent = Intent.createChooser(paramIntent, title);
          startActivity(shareIntent);
        } else if (path.contains(Macadamia.Urls.URI_PATH_RELOAD)) {
          boolean reload = json.getBoolean(Macadamia.Keys.PARAM_KEY_RELOAD);
          String url = StringUtils.defaultIfEmpty(json.getString(Macadamia.Keys.PARAM_KEY_URL),
            Macadamia.Urls.WEB_URL_MAIN);
          String target = StringUtils
            .defaultIfEmpty(json.getString(Macadamia.Keys.PARAM_KEY_TARGET), "self");

          if (reload) {
            mainWebView.reload();
          } else {
            if (target.equals("self")) {
              WebView webView = findViewById(targetId);
              webView.loadUrl(url, getSSOHeader(getDeviceHeader(authCheck)));
            } else if (target.equals("parent")) {
              mainWebView.loadUrl(url, getSSOHeader(getDeviceHeader(authCheck)));
            }
          }
        } else if (path.contains(Macadamia.Urls.URI_PATH_LOGIN)) {
          int userCn = json.getInteger(Macadamia.Keys.PARAM_KEY_CN);
          String cookieValue = StringUtils
            .defaultIfEmpty(json.getString(Macadamia.Keys.PARAM_KEY_COOKIE),
              Macadamia.Constants.STRING_BLANK);
          String returnUrl = StringUtils
            .defaultIfEmpty(json.getString(Macadamia.Keys.PARAM_KEY_URL),
              Macadamia.Urls.WEB_URL_MAIN);

          if (userCn != 0 && StringUtils.isNotEmpty(cookieValue)) {
            Macadamia.Functions.setUserCn(userCn);
            Macadamia.Functions.setCookieValue(cookieValue);

            Logger.t(TAG).d("LOGIN %d", Macadamia.Functions.getUserCn());
            Logger.t(TAG).d("LOGIN %s", Macadamia.Functions.getCookieValue());
          }
          mainWebView.loadUrl(returnUrl, getSSOHeader(getDeviceHeader(authCheck)));
        } else if (path.contains(Macadamia.Urls.URI_PATH_LOGON)) {
          String uid = json.getString("id");
          String upw = json.getString("password");
          String key = json.getString("key");
          String sns = json.getString("sns");
          String snsCode = json.getString("snsCode");
          String ssoKey = json.getString("ssoKey");

          Macadamia.Functions.setAutoLogin(json.getBoolean(Macadamia.Keys.PARAM_KEY_AUTO_LOGIN));
          //Macadamia.Functions.setLogoffUrl(json.getString(Macadamia.Keys.PARAM_KEY_LOGOFF_URL));
          Macadamia.Functions.setLogoffUrl(Macadamia.Urls.WEB_URL_LOGOFF);
          Macadamia.Functions
            .setLoginReturnUrl(json.getString(Macadamia.Keys.PARAM_KEY_RETURN_URL));

          // origin data
          JSONObject tempJson = new JSONObject();
          tempJson.put("a", String.format("%s|%s|%s|%s|%s|%s", uid, upw, key,
            StringUtils.defaultIfEmpty(sns, ""),
            StringUtils.defaultIfEmpty(snsCode, ""),
            StringUtils.defaultIfEmpty(ssoKey, "")));

          processSSOLogin(MacadamiaCryptoUtils.decrypt(key, uid),
            MacadamiaCryptoUtils.decrypt(key, upw),
            StringUtils.isNotEmpty(sns),
            tempJson);

        } else if (path.contains(Macadamia.Urls.URI_PATH_LOCATION)) {
          Logger.t(TAG).d("location");
          eventHandler.processEventById(MSG_MAIN_LOCATION, null, DELAY_10);
        } else if (path.contains(Macadamia.Urls.URI_PATH_SNS_LOGIN)) {
          Logger.t(TAG).d("snslogin");
          String snsUrl = json.getString(Macadamia.Keys.PARAM_KEY_URL);
          String snsTypeName = "카카오";
          if (snsUrl.contains("kakao")) {
            snsTypeName = "카카오";
          } else if (snsUrl.contains("naver")) {
            snsTypeName = "네이버";
          } else if (snsUrl.contains("google")) {
            snsTypeName = "Google";
          } else if (snsUrl.contains("apple")) {
            snsTypeName = "Apple";
          }

          if (StringUtils.isNotEmpty(snsUrl)) {
            Intent intent = new Intent();
            intent.putExtra(Macadamia.Request.REQ_CODE, Macadamia.Request.REQ_SHOW_SUB);
            intent.putExtra(Macadamia.Keys.PARAM_KEY_URL, snsUrl);
            intent.setClass(MainActivity.this, SubActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, Macadamia.Request.REQ_SHOW_SUB);
            overridePendingTransition(R.anim.view_bottom_up, R.anim.view_nothing);
          } else {
            MacadamiaAlertDialog.showAlert(MainActivity.this,
              false,
              getString(R.string.dialog_title_01),
              String.format(getString(R.string.dialog_message_sns_error), snsTypeName),
              getString(R.string.dialog_button_ok),
              this::finish);
          }
        }
        break;

      case MSG_MAIN_APP_BAR_SHOW:
        Logger.t(TAG).d("MSG_MAIN_APP_BAR_SHOW %s", isAppBarAnimated);
        animateAppBar(true);
        break;

      case MSG_MAIN_APP_BAR_HIDE:
        Logger.t(TAG).d("MSG_MAIN_APP_BAR_HIDE %s", isAppBarAnimated);
        animateAppBar(false);
        break;

      case MSG_MAIN_TITLE_BAR_HIDE:
        Logger.t(TAG).d("MSG_MAIN_TITLE_BAR_HIDE %s", isTitleBarAnimated);
        animateTitleBar(false);
        break;

      case MSG_MAIN_TITLE_BAR_SHOW:
        Logger.t(TAG).d("MSG_MAIN_TITLE_BAR_SHOW %s", isTitleBarAnimated);
        animateTitleBar(true);
        break;

      case MSG_MAIN_GET_USER_INFO:
        Logger.t(TAG).d("MSG_MAIN_GET_USER_INFO");
        // 로그인이 되어 있는 경우
        if (Macadamia.Constants.LOGGED) {
          getUserInfo();
        }
        break;

      case MSG_MAIN_AFTER_LOGIN:
        Logger.t(TAG).d("MSG_MAIN_AFTER_LOGIN");
        // get user info
        eventHandler.processEventById(MSG_MAIN_GET_USER_INFO, null, DELAY_10);

        // 로그인 이후 returnUrl 눈높이 학습관인 경우 outlink
        Uri returnUri = Uri.parse(Macadamia.Functions.getLoginReturnUrl());
        if (returnUri.getHost().equals(Urls.WEB_URL_NOONNOPPI_LEARNING_HOST)) {
          doOutLinkWithSSO(Macadamia.Functions.getLoginReturnUrl(), true);
          mainWebView.loadUrl(Macadamia.Urls.WEB_URL_MAIN,
            getSSOHeader(getDeviceHeader(authCheck)));
        } else {
          mainWebView.loadUrl(Macadamia.Functions.getLoginReturnUrl(),
            getSSOHeader(getDeviceHeader(authCheck)));
        }
        break;

      case MSG_MAIN_LOCATION:
        Logger.t(TAG).d("MSG_MAIN_LOCATION");
        getCurrentLocation();
        break;

      case MSG_MAIN_UPDATE_LOCATION:
        Logger.t(TAG).d("MSG_MAIN_UPDATE_LOCATION %f %f", latitude, longitude);
        // set Current Location
        String jsString = String.format(Locale.getDefault(),
          "javascript:window.localStorage.setItem('cLocation', '%s, %f, %f');",
          MacadamiaUtils.getCurrentDateTime("yyyyMMddHHmmss"),
          latitude,
          longitude);
        mainWebView.loadUrl(jsString);
        //mainWebView.reload();
        break;

      case MSG_MAIN_HIDE_LOADING:
        Logger.t(TAG).d("MSG_MAIN_HIDE_LOADING");
        stopLoading();
        break;

      case MSG_MAIN_FINISH:
        Logger.t(TAG).d("MSG_MAIN_FINISH");
        finish();
        break;

      case MSG_MAIN_UPDATE_PUSH_TOKEN:
        this.updatePushToken();
        break;

      case MSG_MAIN_LOGIN_ERROR_REPORT:
        Bundle errorData = msg.getData();
        String errorMessage = errorData.getString("errorMessage");
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);
        JSONObject params = JSONObject.parseObject(errorMessage);
        try {
          macadamiaApi.request(MacadamiaRequestType.REQUEST_ERROR_REPORT, headerMap, params, null);
        } catch (Exception e) {
          Logger.t(TAG).e("EXCEPTION[REQUEST_ERROR_REPORT] : %s", e.getMessage());
        }
        break;

      case MSG_MAIN_RETURN_TO_LOGIN:
        Bundle returnData = msg.getData();
        String returnUrl = returnData.getString(Macadamia.Keys.PARAM_KEY_RETURN_URL);
        String callUrl = String
          .format("%s?%s=%s", Macadamia.Urls.WEB_URL_LOGIN, Macadamia.Keys.PARAM_KEY_RETURN_URL,
            returnUrl);
        mainWebView.loadUrl(callUrl, getSSOHeader(getDeviceHeader(authCheck)));
        break;

      case MSG_MAIN_RETURN_TO_MAIN:
        mainWebView.loadUrl(Macadamia.Urls.WEB_URL_MAIN, getSSOHeader(getDeviceHeader(authCheck)));
        break;

      default:
        break;
    }
  }
  // endregion

  // region 70.LocationManager Delegate
  private void getCurrentLocation() {
    Logger.t(TAG).e("LOCATION : %s", "LOCATION");
    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
      != PackageManager.PERMISSION_GRANTED) {
      if (Macadamia.Functions.getLocationPermissionDialog()) {
        MacadamiaAlertDialog.showConfirm(MainActivity.this,
          false,
          getString(R.string.dialog_title_01),
          getString(R.string.setting_permission_location_disallow),
          getString(R.string.dialog_button_ok),
          () -> ActivityCompat.requestPermissions(MainActivity.this,
            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
              Manifest.permission.ACCESS_FINE_LOCATION},
            REQUEST_CODE_LOCATION_PERMISSION),
          getString(R.string.dialog_button_never_see),
          () -> Macadamia.Functions.setLocationPermissionDialog(false));
      }
      return;
    } else {
      Macadamia.Functions.setLocationPermissionDialog(true);
    }

    Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    if (lastKnownLocation != null) {
      longitude = lastKnownLocation.getLongitude();
      latitude = lastKnownLocation.getLatitude();
    }

    lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
    if (lastKnownLocation != null) {
      longitude = lastKnownLocation.getLongitude();
      latitude = lastKnownLocation.getLatitude();
    }

    lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
    if (lastKnownLocation != null) {
      longitude = lastKnownLocation.getLongitude();
      latitude = lastKnownLocation.getLatitude();
    }

    geoLocationProviders = locationManager.getAllProviders();
    for (int i = 0; i < geoLocationProviders.size(); i++) {
      switch (geoLocationProviders.get(i)) {
        case LocationManager.GPS_PROVIDER:
          locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
          locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
          break;

        case LocationManager.NETWORK_PROVIDER:
          locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
          locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
          break;

        case LocationManager.PASSIVE_PROVIDER:
          locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
          locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, this);
          break;
      }

      Logger.t(TAG).d("GPS : %f / %f", longitude, latitude);
    }
    // remove update
    locationManager.removeUpdates(this);
    // set Location
    eventHandler.processEventById(MSG_MAIN_UPDATE_LOCATION, null, DELAY_10);
  }

  @Override
  public void onLocationChanged(Location location) {
    double latitude;
    double longitude;

    if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
      latitude = location.getLatitude();
      longitude = location.getLongitude();
      Logger.t(TAG).d("GPS : %f / %f", longitude, latitude);
    }

    if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
      latitude = location.getLatitude();
      longitude = location.getLongitude();
      Logger.t(TAG).d("NETWORK : %f / %f", longitude, latitude);
    }

    if (location.getProvider().equals(LocationManager.PASSIVE_PROVIDER)) {
      latitude = location.getLatitude();
      longitude = location.getLongitude();
      Logger.t(TAG).d("PASSIVE : %f / %f", longitude, latitude);
    }
  }

  @Override
  public void onProviderEnabled(@NonNull String provider) {
    Logger.t(TAG).d("LocationManager onProviderEnabled : %s", provider);

    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
      != PackageManager.PERMISSION_GRANTED) {
      return;
    }
    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
    locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, this);
  }

  @Override
  public void onStatusChanged(String provider, int status, Bundle extras) {
    Logger.t(TAG).d("LocationManager onStatusChanged : %s %d %s", provider, status, extras);
  }

  @Override
  public void onProviderDisabled(@NonNull String provider) {
    Logger.t(TAG).d("LocationManager onProviderDisabled : %s", provider);
    MacadamiaAlertDialog.showAlert(MainActivity.this,
      false,
      getString(R.string.dialog_title_01),
      "위치 정보를 조회할 수 없습니다.\n확인 후 다시 시도해주시기 바랍니다.",
      getString(R.string.dialog_button_ok),
      () -> {
      });
  }
  // endregion

  @OnClick(R.id.button_maca_logo)
  public void onClickButtonLogo(View view) {
    mainWebView.loadUrl(Macadamia.Urls.WEB_URL_MAIN, getSSOHeader(getDeviceHeader(authCheck)));
  }

  @OnClick(R.id.button_main_home)
  public void onClickButtonMain(View view) {
    mainWebView.loadUrl(Macadamia.Urls.URL_WEB, getSSOHeader(getDeviceHeader(authCheck)));
    mainWebView.scrollTo(0, 0);
  }

  @OnClick(R.id.button_orange_dot)
  public void onClickButtonOrangeDot(View view) {
    Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    vibe.vibrate(10);
    Intent intent = new Intent();
    intent.putExtra(Macadamia.Request.REQ_CODE, Macadamia.Request.REQ_SHOW_ORANGEDOT);
    intent.setClass(MainActivity.this, OrangeDotActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    startActivityForResult(intent, Macadamia.Request.REQ_SHOW_ORANGEDOT);
    overridePendingTransition(R.anim.view_orangedot_show, R.anim.view_nothing);
  }

  @OnClick(R.id.button_main_myservice)
  public void onclickButtonMyService(View view) {
    mainWebView
      .loadUrl(Macadamia.Urls.WEB_URL_MY_SERVICE, getSSOHeader(getDeviceHeader(authCheck)));
    mainWebView.scrollTo(0, 0);
  }

  @OnClick(R.id.button_menu)
  public void onClickButtonMenu(View view) {
    Logger.t(TAG).d("onClick Menu");
    drawerLayout.openDrawer(GravityCompat.END);
  }

  @OnClick(R.id.button_notification)
  public void onClickButtonNotification(View view) {
    Logger.t(TAG).d("onClick Notification");
    mainWebView
      .loadUrl(Macadamia.Urls.WEB_URL_NOTIFICATION, getSSOHeader(getDeviceHeader(authCheck)));
  }

  @OnClick(R.id.button_menu_setting)
  public void onClickButtonSetting(View view) {
    Logger.t(TAG).d("onClick Setting");
    drawerLayout.closeDrawers();
    Intent intent = new Intent(MainActivity.this, SettingActivity.class);
    intent.putExtra(Macadamia.Request.REQ_CODE, Macadamia.Request.REQ_SHOW_SETTING);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
    //startActivity(intent);
    startActivityForResult(intent, Macadamia.Request.REQ_SHOW_SETTING);
  }

  @OnClick(R.id.button_menu_close)
  public void onClickButtonMenuClose(View view) {
    Logger.t(TAG).d("onClick Menu Close");
    drawerLayout.closeDrawers();
  }

  @OnClick(R.id.button_menu_banner_01)
  public void onClickButtonMenuBanner(View view) {
    drawerLayout.closeDrawers();
    if (Constants.LOGGED) {
      doOutLinkWithSSO(Macadamia.Urls.WEB_URL_NOONNOPPI_LEARNING, true);
    } else {
      String loginUrl = String
        .format("%s?returnUrl=%s", Urls.WEB_URL_LOGIN, Urls.WEB_URL_NOONNOPPI_LEARNING);
      mainWebView.loadUrl(loginUrl, getSSOHeader(getDeviceHeader(authCheck)));
    }
  }

  public void doOutLinkWithSSO(String logonUrl, boolean useSSO) {
    Toast.makeText(ctx, getString(R.string.toast_msg_01), Toast.LENGTH_LONG).show();

    if (useSSO) {
      Uri logonUri = Uri.parse(logonUrl);
      String targetID = logonUri.getHost();

      Logger.t(TAG_SSO).d("Logon URL : %s", logonUrl);
      Logger.t(TAG_SSO).d("TargetID : %s", targetID);

      try {
        SSOStatus status = authCheck.checkLogon();
        if (status == SSOStatus.SSOSuccess) {
          status = authCheck.requestArtifact(logonUrl, targetID);
          if (status == SSOStatus.SSOSuccess) {
            ArtifactSuccessResult artifactSuccessResult = (ArtifactSuccessResult) authCheck
              .getResult();
            String artifactID = artifactSuccessResult.getArtifactID();
            logonUrl = String
              .format("%s?artifactID=%s&targetID=%s", Macadamia.SSO.SSOUrl, artifactID, targetID);
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    Logger.t(TAG_SSO).d("LOGON URL : %s", logonUrl);

    Uri uri = Uri.parse(logonUrl);
    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
    if (intent.resolveActivity(getPackageManager()) != null) {
      startActivity(intent);
    }
  }

  /**
   * 화면 재설정
   */
  private void initScreenView() {
    // StatusBar Background Color
//        MacadamiaScreenUtils.changeStatusBarBackgroundColor(MainActivity.this,
//                getResources().getColor(getResources().getIdentifier("colorFFFFFF_100.0", "color", getPackageName()), null));

    // StatusBarTextColor -> Dark
    MacadamiaScreenUtils.changeStatusBarTextColor(MainActivity.this, true);
    // NavigationBar 있는 경우 S10+
    if (MacadamiaScreenUtils.hasNavigationBar(ctx)) {
      // NavigationBar Icon Color -> Dark
      MacadamiaScreenUtils.changeNavigationBarIconColor(ctx, MainActivity.this, false);
      // NavigationBar Color
      MacadamiaScreenUtils.changeNavigationBarBackgroundColor(MainActivity.this,
        getResources().getColor(
          getResources().getIdentifier("color373737_100.0", "color", getPackageName()), null));
    }
  }

  /**
   * Back Key Event
   */
  private void doBack() {
    String currentUrl = mainWebView.getUrl();
    Logger.t(TAG).d("[onBackPressed] URL : %s", currentUrl);

    // 메뉴 open 되어 있는 경우 닫기
    if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
      drawerLayout.closeDrawers();
    } else {
      // 현재 URL 없는 경우 종료
      if (StringUtils.isEmpty(currentUrl)) {
        Toast.makeText(ctx, getString(R.string.msg_exit_001), Toast.LENGTH_SHORT).show();
        finish();
      } else {
        // 메인 URL 인 경우 종료
        if (currentUrl.equals(Macadamia.Urls.WEB_URL_MAIN) || currentUrl
          .equals(Macadamia.Urls.URL_WEB.concat("/main"))) {
          doFinish();
        } else {
          // 이니시스 오류 URL 인 경우
          if (currentUrl.contains("inicis.com/smart/payError.ini")
            || currentUrl.contains("vbv.samsungcard.co.kr/VbV/mobilev2/MBVTFX400")
            || currentUrl.contains("vbv.samsungcard.co.kr/VbV/mobilev2/MBVTFX421")
            || currentUrl.contains("hyundaicard.com/")) {
            mainWebView.goBackOrForward(-3);
          } else {
            if (mainWebView.canGoBack()) {
              mainWebView.goBack();
            } else {
              mainWebView
                .loadUrl(Macadamia.Urls.WEB_URL_MAIN, getSSOHeader(getDeviceHeader(authCheck)));
            }
          }
        }
      }
    }
  }

  /**
   * APP 종료 처리
   */
  private void doFinish() {
    if (!backKeyPressedFlag) {
      Toast.makeText(ctx, getString(R.string.msg_exit_003), Toast.LENGTH_SHORT).show();
      backKeyPressedFlag = true;
      eventHandler.processEventById(MSG_MAIN_FINISH_APP, null, DELAY_2000);
    } else {
      finishCompletely();
    }
  }

  /**
   * Animate TitleBar
   *
   * @param isShow show flag
   */
  private void animateTitleBar(boolean isShow) {
    int positionY = isShow ? 0 : titleBarLayout.getHeight() * -1;

    if (!isTitleBarAnimated) {
      titleBarLayout.animate()
        .translationY(positionY)
        .setListener(new Animator.AnimatorListener() {
          @Override
          public void onAnimationStart(Animator animator) {
            isTitleBarAnimated = true;
          }

          @Override
          public void onAnimationEnd(Animator animator) {
            isTitleBarAnimated = false;
          }

          @Override
          public void onAnimationCancel(Animator animator) {
          }

          @Override
          public void onAnimationRepeat(Animator animator) {
          }
        });
      mainWebView.animate().translationY(positionY);
    }
  }

  /**
   * Animate App Bar
   *
   * @param isShow show flag
   */
  private void animateAppBar(boolean isShow) {
    if (canAnimateAppBar) {
      int positionY = isShow ? 0
        : bottomContainerLayout.getHeight() + MacadamiaScreenUtils.getNavigationBarHeight(ctx);
      if (!isAppBarAnimated) {
        bottomContainerLayout.animate()
          .translationY(positionY)
          .setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
              isAppBarAnimated = true;
            }

            @Override
            public void onAnimationEnd(Animator animator) {
              isAppBarAnimated = false;
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
          });
      }
    }
  }


  /**
   * Menu Button OnClickListener
   */
  private final View.OnClickListener menuButtonsListener = (view) -> {
    // close drawer
    drawerLayout.closeDrawers();
    for (int i = 0; i < menuItemIds.length; i++) {
      if (menuItemIds[i] == view.getId()) {
        // 고객센터 outlink
        if (menuItemLinks[i].contains(Urls.WEB_URL_CS_CENTER_HOST)) {
          String ccemUrl = menuItemLinks[i];
          if (Constants.LOGGED && Macadamia.Functions.getUserCn() != 0) {
            try {
              String encCn = MacaAES.encrypt(String.format("%d", Functions.getUserCn()), Constants.ZENDESK_ENC_KEY, Constants.ZENDESK_ENC_IV.getBytes());
              ccemUrl = String.format("%s?cn=%s", ccemUrl, URLEncoder.encode(encCn, UTF_8));
            } catch (Exception e) {
              Logger.t(TAG).e("CCEM CN encrypt : %s", e.getStackTrace());
            }
          }
          doOutLinkWithSSO(ccemUrl, false);
        } else {
          mainWebView.loadUrl(menuItemLinks[i], getSSOHeader(getDeviceHeader(authCheck)));
        }
      }
    }
  };

  /**
   * file Download Listener
   */
  private final DownloadListener downloadListener = (url, userAgent, contentDisposition, mimeType, contentLength) -> {
    Logger.t(TAG)
      .e("file download Listener %s %s %s %d", url, contentDisposition, mimeType, contentLength);

    String filename = URLUtil.guessFileName(url, contentDisposition, mimeType);
    Logger.t(TAG).d("file download : %s", filename);

    MacadamiaAlertDialog.showConfirm(MainActivity.this,
      false,
      getString(R.string.dialog_title_01),
      String.format(getString(R.string.dialog_message_04), filename),
      getString(R.string.dialog_button_ok),
      () -> {
        try {
          MacadamiaUtils
            .fileDownload(MainActivity.this, url, userAgent, contentDisposition, mimeType,
              filename);
        } catch (Exception e) {
          if (ContextCompat.checkSelfPermission(MainActivity.this,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
              android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
              // Toast Message
              Toast.makeText(getBaseContext(),
                getString(R.string.dialog_message_05),
                Toast.LENGTH_LONG).show();
              // Request Permission
              ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE_FILE_PERMISSION);
            } else {
              // Toast Message
              Toast.makeText(getBaseContext(),
                getString(R.string.dialog_message_05),
                Toast.LENGTH_LONG).show();
              // Request Permission
              ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE_FILE_PERMISSION);
            }
          }
        }
      },
      getString(R.string.dialog_button_cancel),
      () -> Logger.t(TAG).d("file download cancel"));
  };

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    Logger.t(TAG).d("onActivityResult %X %X %s", requestCode, resultCode, data);
    super.onActivityResult(requestCode, resultCode, data);

    switch (requestCode) {
      case REQUEST_CODE_FILE_PERMISSION:
        Logger.t(TAG).d("REQUEST_CODE_FILE_PERMISSION");
        break;

      case REQUEST_CODE_LOCATION_PERMISSION:
        Logger.t(TAG).d("REQUEST_CODE_LOCATION_PERMISSION");
        break;

      case Macadamia.Request.REQ_SHOW_ORANGEDOT:
        if (resultCode == Macadamia.Result.RES_HIDE_ORANGEDOT) {
          Logger.t(TAG).d("HIDE_ORANGEDOT");
          if (data != null) {
            if (data.hasExtra(Macadamia.Keys.PARAM_KEY_RELOAD)) {
              boolean reload = data.getBooleanExtra(Macadamia.Keys.PARAM_KEY_RELOAD, true);
              if (reload) {
                mainWebView.reload();
              } else {
                if (data.hasExtra(Macadamia.Keys.PARAM_KEY_URL)) {
                  String redirectUrl = data.getStringExtra(Macadamia.Keys.PARAM_KEY_URL);
                  if (StringUtils.isNotEmpty(redirectUrl)) {
                    mainWebView.loadUrl(redirectUrl, getSSOHeader(getDeviceHeader(authCheck)));
                  }
                }
              }
            }
          }
        } else if (resultCode == Macadamia.Result.RES_LOAD_URL) {
          String urlString = StringUtils
            .defaultIfEmpty(data.getStringExtra(Macadamia.Keys.PARAM_KEY_URL),
              Macadamia.Urls.WEB_URL_MAIN);
          urlString =
            urlString.startsWith("/") ? String.format("%s%s", Macadamia.Urls.URL_WEB, urlString)
              : urlString;
          mainWebView.loadUrl(urlString, getSSOHeader(getDeviceHeader(authCheck)));
        }
        break;

      // FILE CHOOSE
      case REQUEST_CHOOSE_IMAGE_FILE:
        Logger.t(TAG).d("REQUEST_CHOOSE_IMAGE_FILE : %d", resultCode);

        if (resultCode == Activity.RESULT_OK) {
          if (data != null) {
            if (webViewFilePathCallback == null) {
              super.onActivityResult(requestCode, resultCode, data);
              return;
            }

            Uri[] results = new Uri[]{getResultUri(data)};
            // set result
            setFileChooserResult(results);
          } else {
            // clear File Chooser Result
            setFileChooserResult(null);
            super.onActivityResult(requestCode, resultCode, data);
          }
        }
        // 파일 선택 취소 시 RESULT_OK 이외 처리 하지 않으면 파일 재선택 불가
        else {
          // clear File Chooser Result
          setFileChooserResult(null);
        }
        break;

      case Macadamia.Request.REQ_SHOW_SETTING:
        Logger.t(TAG).d("REQ_SHOW_SETTING : %d", resultCode);
        switch (resultCode) {
          case Activity.RESULT_CANCELED:
            mainWebView.reload();
            break;

          case Activity.RESULT_OK:
            break;

          case Macadamia.Result.RES_LOGIN:
            mainWebView.loadUrl(
              Macadamia.Urls.WEB_URL_LOGIN);//, getSSOHeader(getDeviceHeader(authCheck)));
            break;

          case Macadamia.Result.RES_LOGOFF:
            mainWebView.loadUrl(Macadamia.Functions.getLogoffUrl());
            break;
        }
        break;

      case Macadamia.Request.REQ_SHOW_SUB:
        Logger.t(TAG).d("REQ_SHOW_SUB : %d", resultCode);
        switch (resultCode) {
          case Macadamia.Result.RES_LOAD_URL:
            mainWebView.loadUrl(data.getStringExtra(Macadamia.Keys.PARAM_KEY_URL),
              getSSOHeader(getDeviceHeader(authCheck)));
            break;
          case Macadamia.Result.RES_RELOAD:
            mainWebView.reload();
            break;
        }
        break;

      default:
        break;
    }
  }

  /**
   * Intent로 부터 결과 URI 리턴
   *
   * @param data Intent
   * @return Result URI
   */
  private Uri getResultUri(Intent data) {
    Uri result = null;

    if (data == null || StringUtils.isEmpty(data.getDataString())) {
      // If there is not data, then we may have taken a photo
      if (mCameraPhotoPath != null) {
        result = Uri.parse(mCameraPhotoPath);
      }
    } else {
      String filePath = data.getDataString();
      result = Uri.parse(filePath);
    }

    return result;
  }

  /**
   * 권한 요청
   *
   * @param permissionType 권한 유형
   */
  private void requestPermission(String permissionType) {
    String[] PERMISSIONS_LIST = null;

    if (permissionType.equals(PERMISSION_CAMERA)) {
      PERMISSIONS_LIST = new String[]{
        Manifest.permission.CAMERA
      };
    } else if (permissionType.equals(PERMISSION_LOCATION)) {
      PERMISSIONS_LIST = new String[]{
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
      };
    }
    askPermission(this, PERMISSIONS_LIST).ask(new PermissionListener() {
      @Override
      public void onAccepted(PermissionResult permissionResult, List<String> accepted) {
        for (String permission : accepted) {
          Logger.t(TAG).e("%s Granted", permission);
        }
      }

      @Override
      public void onDenied(PermissionResult permissionResult, List<String> denied,
                           List<String> foreverDenied) {
        if (permissionResult.hasDenied()) {
          // 권한 미허용시 Dialog 노출
          MacadamiaAlertDialog.showAlert(MainActivity.this,
            false,
            getString(R.string.dialog_title_01),
            getString(R.string.dialog_message_optional_permission),
            getString(R.string.dialog_button_ok),
            () -> {
            });
        }
        // 다시 묻지 않기 선택시
        if (permissionResult.hasForeverDenied()) {
          // 권한 다시 묻지 않기 이후 설정 이동 메시지
          MacadamiaAlertDialog.showAlert(MainActivity.this,
            false,
            getString(R.string.dialog_title_01),
            getString(R.string.dialog_message_permission_gosetting),
            getString(R.string.dialog_button_ok),
            () -> {
            });
        }
      }
    });
  }

  /**
   * 이미지 선택
   */
  private void showImageChooser() {
    // Storage 권한
    if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE)
      != PackageManager.PERMISSION_GRANTED) {
      requestPermission(PERMISSION_STORAGE);
      return;
    }

    Intent chooserIntent = new Intent(Intent.ACTION_GET_CONTENT);
    chooserIntent.addCategory(Intent.CATEGORY_OPENABLE);
    chooserIntent.setType(TYPE_IMAGE);
    startActivityForResult(Intent.createChooser(chooserIntent,
      getString(R.string.main_text_choose_image)),
      REQUEST_CHOOSE_IMAGE_FILE);
  }

  /**
   * set Result on File Chooser
   *
   * @param results File Chooser result
   */
  private void setFileChooserResult(@Nullable Uri[] results) {
    if (webViewFilePathCallback != null) {
      webViewFilePathCallback.onReceiveValue(results);
    }
    webViewFilePathCallback = null;
  }

  /**
   * 사용자 정보 조회
   */
  private void getUserInfo() {
    Logger.t(TAG).d("MacadamiaApi getUserInfo");
    try {
      JSONObject param = new JSONObject();
      param.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
      param.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);

      macadamiaApi.request(MacadamiaRequestType.REQUEST_USER_INFO,
        macadamiaApi.getHeaderMap(getSSOHeader(getDeviceHeader(authCheck))),
        param,
        requestUserInfoCallback);
    } catch (Exception e) {
      Logger.t(TAG).e("API GET_USR_INFO Exception", e);
      networkErrorAlertDialog(MainActivity.this,
        getString(R.string.dialog_button_ok),
        () -> {
        });
    }
  }

  /**
   * 사용자 정보 조회 Callback
   */
  private final Callback<JSONObject> requestUserInfoCallback = new Callback<JSONObject>() {
    @Override
    public void onResponse(@NonNull Call<JSONObject> call, @NonNull Response<JSONObject> response) {
      Logger.t(TAG).d("API GET_USR_INFO RESP %s", response);
      if (response.isSuccessful()) {
        JSONObject data = response.body().getJSONObject(Macadamia.Keys.PARAM_KEY_DATA);
        Logger.t(TAG).d("API GET_USR_INFO RESP data : %s", data);
        if (data != null) {
          Macadamia.Functions.setUserCn(data.getInteger(Macadamia.Keys.PARAM_KEY_CN));
          Macadamia.Functions.setUserName(data.getString(Macadamia.Keys.PARAM_KEY_NAME));
          Macadamia.Functions.setUserEmail(data.getString(Macadamia.Keys.PARAM_KEY_EMAIL));
          int notificationCount = data.getInteger(Macadamia.Keys.PARAM_KEY_NOTIFICATION_COUNT);
          Macadamia.Functions.setPushReceive(data.getJSONObject(Macadamia.Keys.PARAM_KEY_PUSH)
            .getBoolean(Macadamia.Keys.PARAM_KEY_PUSH_RECEIVE));
        }
        // 사용자 CN 있는 경우 Push Token Update
        if (Macadamia.Functions.getUserCn() > 0) {
          updatePushToken();
        }
      } else {
        Logger.t(TAG).e("API GET_USR_INFO ERR");
        dataLoadingErrorAlertDialog(MainActivity.this,
          getString(R.string.dialog_button_ok),
          () -> {
          });
      }
    }

    @Override
    public void onFailure(@NonNull Call<JSONObject> call, Throwable t) {
      Logger.t(TAG).e("[FAILURE] %s", t.getMessage());
      networkErrorAlertDialog(MainActivity.this,
        getString(R.string.dialog_button_close),
        () -> {
        });
    }
  };

  /**
   * PUSH 토큰 업데이트
   */
  private void updatePushToken() {
    Logger.t(TAG).d("MacadamiaApi updatePushToken");
    try {
      JSONObject param = new JSONObject();
      param.put(Macadamia.Keys.PARAM_KEY_TOKEN, Macadamia.Functions.getPushToken());
      param.put("receivePush", Macadamia.Functions.getPushReceive());
      param.put(Macadamia.Keys.PARAM_KEY_APP_ID, Macadamia.Constants.APP_ID);
      param.put(Macadamia.Keys.PARAM_KEY_DEVICE_ID, Macadamia.Constants.DEVICE_ID);
      param.put(Macadamia.Keys.PARAM_KEY_SESSION_ID, Macadamia.Constants.SESSION_ID);

      macadamiaApi.request(MacadamiaRequestType.UPDATE_PUSH_TOKEN,
        macadamiaApi.getHeaderMap(getSSOHeader(getDeviceHeader(authCheck))),
        param,
        updatePushTokenCallback);
    } catch (Exception e) {
      Logger.t(TAG).e("API UPDATE_PUSH_TOKEN Exception", e);
      networkErrorAlertDialog(MainActivity.this,
        getString(R.string.dialog_button_ok),
        () -> {
        });
    }
  }

  /**
   * PUSH TOKEN Update Callback
   */
  private final Callback<JSONObject> updatePushTokenCallback = new Callback<JSONObject>() {
    @Override
    public void onResponse(@NonNull Call<JSONObject> call, @NonNull Response<JSONObject> response) {
      Logger.t(TAG).d("API UPDATE_PUSH_TOKEN RESP %s", response);
      if (response.isSuccessful()) {
        JSONObject data = response.body().getJSONObject(Macadamia.Keys.PARAM_KEY_DATA);
        Logger.t(TAG).d("API UPDATE_PUSH_TOKEN RESP data : %s", data);
      } else {
        Logger.t(TAG).e("API UPDATE_PUSH_TOKEN ERR");
        dataLoadingErrorAlertDialog(MainActivity.this,
          getString(R.string.dialog_button_ok),
          () -> {
          });
      }
    }

    @Override
    public void onFailure(@NonNull Call<JSONObject> call, Throwable t) {
      Logger.t(TAG).e("[FAILURE] %s", t.getMessage());
      networkErrorAlertDialog(MainActivity.this,
        getString(R.string.dialog_button_close),
        () -> {
        });
    }
  };

  /**
   * initialize menu
   */
  private void initMenus() {
    drawerLayout = findViewById(R.id.layout_main);
    for (int i = 0; i < menuItemIds.length; i++) {
      menuItemButtons[i] = findViewById(menuItemIds[i]);
      menuItemButtons[i].setOnClickListener(menuButtonsListener);
    }
  }

  /**
   * SSO Login
   *
   * @param userId     USER ID
   * @param userPasswd USER PASSWD
   * @param sns        isSNS
   * @param query      JSON Object
   */
  private void processSSOLogin(String userId, String userPasswd, boolean sns, JSONObject query) {
    try {
      SSOStatus ssoStatus;
      if (sns) {
        ssoStatus = authCheck.logon(UserCredentialType.PKICertCredential, userId, userId);
      } else {
        ssoStatus = authCheck.logon(UserCredentialType.BasicCredential, userId, userPasswd);
      }
      Logger.t(TAG_SSO).e("UID : %s", userId);
      Logger.t(TAG_SSO).e("UPW : %s", userPasswd);
      Logger.t(TAG_SSO).e("SNS : %s", sns);
      Logger.t(TAG_SSO).e("SSO RESULT : %s", ssoStatus);

      if (ssoStatus == SSOStatus.SSOSuccess) {
        try {
          AuthSuccessResult ssoResult = (AuthSuccessResult) authCheck.getResult();
          //인증 성공시 deviceHeader와 encryptedCredentail 정보를 리턴받는다.
          //encryptedCredentail 로그인 요청시 한번만 제공이 가능하다.
          Logger.t(TAG_SSO).d("AppID : %s", SSOConfig.getInstance().getAppID());
          Logger.t(TAG_SSO).d("UserID : %s", ssoResult.getToken().getUserID());
          Logger.t(TAG_SSO).d("UserID : %s", ssoResult.getUserInfos("CN"));
          authCheck.setUserID(ssoResult.getToken().getUserID());
          String userCn = StringUtils.defaultIfEmpty(ssoResult.getUserInfos("CN"), "0");
          Macadamia.Functions.setUserCn(Integer.parseInt(userCn));

          DateTime idleCheckTime = new DateTime(ssoResult.getToken().getIdleCheckTime());
          DateTime logonTime = new DateTime(ssoResult.getToken().getLogonTime());
          Logger.t(TAG_SSO).d("IdleCheckTime %s / LogonTime : %s",
            idleCheckTime.toString("yyyy:MM:dd:HH:mm:ss"),
            logonTime.toString("yyyy:MM:dd:HH:mm:ss"));

          // 로그인 상태 설정
          Macadamia.Constants.LOGGED = true;
          setSSOCredential(ssoResult.getEncryptedCredential());
          //btnLogon.setEnabled(false);
          // 이제 서버 오류 코드를 확인한다.
          // 인증에 성공했지만, 사용자 비밀번호 만료 정책 관련된 경고가 발생하거나
          // 중복로그온 정책에 따라서, 후입자 우선일 경우에 중복 로그온에 대한 추가적인 정보를 이용하여 사용자에게 알려야 한다.
          if (ssoResult.getServerErrorCode() != ServerErrorCode.NoError) {
            processLogonWarning(ssoResult);
          }
          // 로그인 이후 URL 이동
          eventHandler.processEventById(MSG_MAIN_AFTER_LOGIN, null, DELAY_0);
        } catch (Exception e) {
          Logger.t(TAG).e("SSO Exception : %s", e.getMessage());
          // 로그인 상태 설정
          logOff(authCheck);
        }
      } else if (ssoStatus == SSOStatus.SSOFail) {
        AuthFailResult ssoResult = (AuthFailResult) authCheck.getResult();
        try {
          JSONObject params = new JSONObject();
          params.put(Macadamia.Keys.PARAM_KEY_ERROR_TYPE, "LOGON");
          params.put("errorCode", "LOGON");
          params.put(Macadamia.Keys.PARAM_KEY_USER_ID, userId);
          params.put(Macadamia.Keys.PARAM_KEY_ERROR, String.format("[S:%s/A:%s/E:%s]%s(%s)|%s",
            ssoResult.getServerErrorCode(),
            ssoResult.getAppErrorCode(),
            ssoResult.toString(),
            Macadamia.Constants.DEVICE_MODEL,
            Macadamia.Constants.DEVICE_OS_VERSION,
            query));
          // json to String
          String jsonString = params.toJSONString();
          Message errorMessage = new Message();
          Bundle errorBundle = new Bundle();
          errorBundle.putString("errorMessage", jsonString);
          errorMessage.setData(errorBundle);
          eventHandler.processEventById(MSG_MAIN_LOGIN_ERROR_REPORT, errorMessage, DELAY_0);
        } catch (Exception e) {
          Logger.t(TAG).e(e.getMessage());
        }
        processLogonFailed(authCheck, ssoResult, false);
        clearUserInfoControl();
      }
    } catch (Exception e) {
      Logger.t(TAG).e("SSO Exception : %s", e.getMessage());
      // 로그인 상태 설정
      logOff(authCheck);
    }
  }

  /**
   * 인증 확인 실패 시 alert 메세지를 표시한다.
   *
   * @param authCheck AuthCheck
   * @param ssoResult AuthFailResult
   */
  private void processLogonFailed(AuthCheck authCheck, AuthFailResult ssoResult,
                                  boolean onBackground) {
    if (ssoResult.getAppErrorCode() != AppErrorCode.NoError) {
      String message = getString(R.string.dialog_message_login_fail_01);
      if (onBackground) {
        message = getString(R.string.dialog_message_login_fail_02);
      } else {
        if (ssoResult.getAppErrorCode() == AppErrorCode.InvalidUserCredential ||
          ssoResult.getAppErrorCode() == AppErrorCode.FailedToLogon) {
          message = getString(R.string.dialog_message_login_fail_03);
        } else if (ssoResult.getAppErrorCode() == AppErrorCode.NetworkOffLine ||
          ssoResult.getAppErrorCode() == AppErrorCode.SSOLIveCheckFail ||
          ssoResult.getAppErrorCode() == AppErrorCode.NonExistOffLineToken ||
          ssoResult.getAppErrorCode() == AppErrorCode.InvalidOffLineToken ||
          ssoResult.getAppErrorCode() == AppErrorCode.ValidOffLineToken) {
          message = getString(R.string.dialog_message_login_fail_04);
        }
      }

      MacadamiaAlertDialog.showAlert(MainActivity.this,
        false,
        getString(R.string.dialog_title_01),
        String.format(message, ssoResult.getAppErrorCode()),
        getString(R.string.dialog_button_ok),
        () -> {
          setSSOCredential(Macadamia.Constants.STRING_BLANK);
          logOff(authCheck);
        });
    }
  }

  private void processLogonWarning(AuthSuccessResult ssoResult) {
    Logger.t(TAG_SSO).d("SSO WARNING : %s", ssoResult.getAppErrorCode().toString());
  }

  private void clearUserInfoControl() {
    Logger.t(TAG_SSO).d("SSO CLEAR");
    // 로그인 상태 설정
    logOff(authCheck);
  }

  /**
   * 이니시스 결제 앱 미설치 오류
   *
   * @param cardType    카드 종류
   * @param packageName PackageName
   */
  private void showPaymentAppErrorAlert(@NonNull String cardType, @NonNull String packageName,
                                        boolean etc) {
    if (etc) {
      MacadamiaAlertDialog.showAlert(MainActivity.this,
        false,
        getString(R.string.dialog_title_02),
        getString(R.string.dialog_message_pay_nothing),
        getString(R.string.dialog_button_ok),
        () -> {
        });
    } else {
      if (StringUtils.isNotEmpty(packageName)) {
        MacadamiaAlertDialog.showConfirm(MainActivity.this,
          false,
          getString(R.string.dialog_title_02),
          String.format(getString(R.string.dialog_message_pay_fail_go_playstore), cardType),
          getString(R.string.dialog_button_ok),
          () -> MacadamiaUtils.goPlayStore(MainActivity.this, packageName),
          getString(R.string.dialog_button_cancel),
          () -> {
          });
      } else {
        MacadamiaAlertDialog.showAlert(MainActivity.this,
          false,
          getString(R.string.dialog_title_02),
          String.format(getString(R.string.dialog_message_pay_fail), cardType),
          getString(R.string.dialog_button_ok),
          () -> {
          });
      }
    }
  }
}
