package com.daekyo.noonnoppi.views.guide;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;

public class GuidePagerAdapter extends RecyclerView.Adapter<GuidePagerAdapter.GuideHolder> {

    private static final String TAG = GuidePagerAdapter.class.getSimpleName();

    private final Context context;
    private final ArrayList<Drawable> items;

    private static final int FIRST_VIEW = 0x01;
    private static final int NO_LAST_VIEW = 0x10;
    private static final int LAST_VIEW = 0x20;

    private int currentPosition;

    public GuidePagerAdapter(@NonNull Context context, @NonNull ArrayList<Drawable> items) {
        this.context = context;
        this.items = items;
        this.currentPosition = 0;
    }

    @NonNull
    @Override
    public GuideHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Logger.t(TAG).d("VIEW TYPE : %d", viewType);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_guide_item, parent, false);
        if (viewType == LAST_VIEW) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_guide_last_item, parent, false);
        }
        return new GuideHolder(view, viewType, this.currentPosition);
    }

    @Override
    public int getItemViewType(int position) {
        this.currentPosition = position;

        if (position == 0) {
            return FIRST_VIEW;
        } else if (position == (this.items.size() - 1)) {
            return LAST_VIEW;
        } else {
            return NO_LAST_VIEW;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull GuideHolder holder, int position) {
        Drawable res = items.get(position);
        holder.imageGuideItem.setImageDrawable(res);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class GuideHolder extends RecyclerView.ViewHolder {
        protected final ImageView imageGuideItem;
        protected ImageButton buttonStart;
        protected CheckBox checkBoxNotSee;
        protected TextView textViewNotSee;
        protected final ImageButton buttonPrev;
        protected final ImageButton buttonNext;
        protected final ImageButton buttonClose;
        protected final int viewType;
        protected final int currentPosition;

        public GuideHolder(@NonNull View itemView, int viewType, int position) {
            super(itemView);
            this.imageGuideItem = itemView.findViewById(R.id.img_guide);
            this.viewType = viewType;
            this.buttonPrev = itemView.findViewById(R.id.button_guide_prev);
            this.buttonNext = itemView.findViewById(R.id.button_guide_next);
            this.buttonClose = itemView.findViewById(R.id.button_guide_close);
            this.currentPosition = position;

            if (this.viewType == FIRST_VIEW) {
                this.buttonPrev.setVisibility(View.INVISIBLE);
                this.buttonNext.setVisibility(View.VISIBLE);
            } else if (this.viewType == LAST_VIEW) {
                this.buttonStart = itemView.findViewById(R.id.button_start);
                this.buttonStart.setOnClickListener((v) -> ((GuideActivity) (itemView.getContext())).onClickButtonStart(this.buttonStart));
                this.checkBoxNotSee = itemView.findViewById(R.id.checkbox_not_see);
                this.checkBoxNotSee.setOnCheckedChangeListener((v, c) -> Macadamia.Functions.setGuideChecked(c));
                this.textViewNotSee = itemView.findViewById(R.id.text_not_see);
                this.textViewNotSee.setOnClickListener((v) -> this.checkBoxNotSee.setChecked(!this.checkBoxNotSee.isChecked()));
                this.buttonPrev.setVisibility(View.VISIBLE);
                this.buttonNext.setVisibility(View.INVISIBLE);
            } else {
                this.buttonPrev.setVisibility(View.VISIBLE);
                this.buttonNext.setVisibility(View.VISIBLE);
            }

            this.buttonPrev.setOnClickListener((v) -> ((GuideActivity) (itemView.getContext())).onClickButtonPrev(this.buttonClose, this.currentPosition));
            this.buttonNext.setOnClickListener((v) -> ((GuideActivity) (itemView.getContext())).onClickButtonNext(this.buttonClose, this.currentPosition));
            this.buttonClose.setOnClickListener((v) -> ((GuideActivity) (itemView.getContext())).onClickButtonClose(this.buttonClose));
        }
    }
}
