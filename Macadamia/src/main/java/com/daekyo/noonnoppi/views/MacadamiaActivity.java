package com.daekyo.noonnoppi.views;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.daekyo.noonnoppi.common.Macadamia.Constants;
import com.daekyo.noonnoppi.common.MacadamiaApplication;
import com.daekyo.noonnoppi.common.api.MacadamiaApi;
import com.daekyo.noonnoppi.common.dialog.MacadamiaAlertDialog;
import com.daekyo.noonnoppi.common.dialog.OnAlertButtonListener;
import com.orhanobut.logger.Logger;

import java.lang.ref.WeakReference;
import java.util.HashMap;

import butterknife.Unbinder;
import kr.co.nets.sso.appagent.android.AuthCheck;
import kr.co.nets.sso.appagent.android.SSOConfig;
import kr.co.nets.sso.appagent.android.SSOStatus;

public class MacadamiaActivity extends AppCompatActivity implements MacaEventDelegate {

    // region 100.Variables
    private static final String TAG_MACA = MacadamiaActivity.class.getSimpleName();
    public static final String TAG_API = "API";
    public static final String TAG_SSO = "<SSO>";
    public static final String TAG_INICIS = "<INIPAYMOBILE>";
    public static final String TAG_NICE_AUTH = "<NICE_AUTH>";

    public static final int REQUEST_CHOOSE_IMAGE_FILE = 0x0100;

    public static final String PERMISSION_LOCATION = "location";
    public static final String PERMISSION_STORAGE = "storage";
    public static final String PERMISSION_CAMERA = "camera";


    // 권한 리스트 필수
    public static final String[] PERMISSIONS_LIST_MANDATORY = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    // 권한 리스트 선택
    public static final String[] PERMISSIONS_LIST_OPTIONAL = {
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_PHONE_STATE
    };

    public static final int MSG_RELOAD_ORANGEDOT_ITEMS = 0x5001;

    /**
     * 메뉴 Resources
     */
    protected final int[] menuItemIds = {
            R.id.button_menu_01_01,
            R.id.button_menu_01_02,
            R.id.button_menu_01_03,
            R.id.button_menu_01_04,
            R.id.button_menu_01_05,
            R.id.button_menu_02_01,
            R.id.button_menu_02_02,
            R.id.button_menu_02_03,
            R.id.button_menu_02_04,
            R.id.button_menu_02_05,
            R.id.button_menu_03_01,
            R.id.button_menu_03_02
    };

    /**
     * 메뉴 URL
     */
    protected final String[] menuItemLinks = {
            Macadamia.Urls.WEB_URL_BRAND,
            Macadamia.Urls.WEB_URL_PRODUCT,
            Macadamia.Urls.WEB_URL_PLACE,
            Macadamia.Urls.WEB_URL_EVENT,
            Macadamia.Urls.WEB_URL_COMMUNITY,
            Macadamia.Urls.WEB_URL_MY_SERVICE,
            Macadamia.Urls.WEB_URL_CART,
            Macadamia.Urls.WEB_URL_PICK,
            Macadamia.Urls.WEB_URL_RECENT,
            Macadamia.Urls.WEB_URL_TALK,
            Macadamia.Urls.WEB_URL_CS_CENTER,
            Macadamia.Urls.WEB_URL_NONMBR_ORDER_LIST
    };

    // ButterKnife Binder
    protected Unbinder unbinder = null;
    // Context
    protected Context ctx;
    // Macadamia API
    protected MacadamiaApi macadamiaApi;
    // Activity 이벤트 핸들러
    protected MacaEventHandler eventHandler;

    protected SharedPreferences ssoPref = null;
    private static final String SSO_PREF_KEY = "encCredential";
    protected AuthCheck authCheck = null;
    protected SSOConfig ssoConfig;
    // endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ssoPref = getSharedPreferences(SSO_PREF_KEY, MODE_PRIVATE);
        initActivity();
    }

    protected String getSSOCredential() {
        return ssoPref.getString(SSO_PREF_KEY, Macadamia.Constants.STRING_BLANK);
    }

    /**
     * set SSO credential
     *
     * @param value credential
     */
    protected void setSSOCredential(String value) {
        SharedPreferences.Editor editor = ssoPref.edit();
        editor.putString(SSO_PREF_KEY, value);
        editor.apply();
    }

    /**
     * get SSO Header Map
     *
     * @param deviceHeader device Header
     * @return header map
     */
    protected HashMap<String, String> getSSOHeader(String deviceHeader) {
        HashMap<String, String> headers = new HashMap<>();
        if (Macadamia.Constants.LOGGED && deviceHeader != null) {
            headers.put(Macadamia.Keys.KEY_HEADER_NSSO_TOKEN, deviceHeader);
        }
        return headers;
    }

    /**
     * get Device Header String
     *
     * @param authCheck SSO AuthCheck instance
     * @return deviceHeader String
     */
    protected String getDeviceHeader(AuthCheck authCheck) {
        String deviceHeader = null;
        try {
            if (Macadamia.Constants.LOGGED && authCheck != null) {
                deviceHeader = authCheck.getDeviceHeader();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            Logger.t(TAG_SSO).e("SSO getDeviceHeader Exception : %s", e.getMessage());
            deviceHeader = null;
        }
        return deviceHeader;
    }

    /**
     * 이벤트 처리
     */
    protected static class MacaEventHandler extends Handler {
        private final WeakReference<MacadamiaActivity> mActivity;

        private final int[] messages;
        private int currentEventIndex = 0;

        /**
         * Constructor
         *
         * @param activity Activity
         * @param messages Messages
         */
        public MacaEventHandler(@NonNull MacadamiaActivity activity, int[] messages) {
            mActivity = new WeakReference<>(activity);
            this.messages = messages;
        }

        /**
         * Handle Message
         *
         * @param message Message 파라미터
         */
        @Override
        public void handleMessage(@NonNull Message message) {
            MacadamiaActivity activity;
            activity = mActivity.get();
            if (activity != null) {
                activity.handleEventMessage(message);
            }
        }

        /**
         * start Event
         *
         * @param message Message 파라미터
         */
        public void startEvent(@Nullable Message message) {
            Logger.t(TAG_MACA).d("START EVENT!");
            if (messages.length > 0) {
                if (message != null) {
                    message.what = messages[currentEventIndex++];
                    sendMessage(message);
                } else {
                    sendEmptyMessage(messages[currentEventIndex++]);
                }
            }
        }

        /**
         * process next Event
         *
         * @param message     Message 파라미터
         * @param delayMillis delay time
         */
        public void processNextEvent(@Nullable Message message, long delayMillis) {
            if (messages.length > currentEventIndex && currentEventIndex >= 0) {
                if (message != null) {
                    message.what = currentEventIndex++;
                    sendMessageDelayed(message, delayMillis);
                } else {
                    sendEmptyMessageDelayed(messages[currentEventIndex++], delayMillis);
                }
            }
        }

        /**
         * process previous Event
         *
         * @param message     Message 파라미터
         * @param delayMillis delay time
         */
        public void processPreviousEvent(@Nullable Message message, long delayMillis) {
            int tmepIndex = currentEventIndex - 1;

            if (messages.length > tmepIndex && tmepIndex >= 0) {
                if (message != null) {
                    message.what = messages[tmepIndex];
                    sendMessageDelayed(message, delayMillis);
                    currentEventIndex++;
                } else {
                    sendEmptyMessageDelayed(messages[tmepIndex], delayMillis);
                }
            }
        }

        /**
         * process Event by Message ID
         *
         * @param messageId   Message ID
         * @param message     Message 파라미터
         * @param delayMillis delay time
         */
        public void processEventById(int messageId, @Nullable Message message, long delayMillis) {
            if (message != null) {
                message.what = messageId;
                sendMessageDelayed(message, delayMillis);
            } else {
                sendEmptyMessageDelayed(messageId, delayMillis);
            }
        }

        /**
         * get Current Event Index
         *
         * @return Current Event Index
         */
        public int getCurrentEventIndex() {
            return (currentEventIndex > 0 && currentEventIndex < messages.length) ? (currentEventIndex) : 0;
        }

        /**
         * get Current Event
         *
         * @return Current Event
         */
        public int getCurrentEvent() {
            return messages[getCurrentEventIndex()];
        }
    }

    /**
     * Handle Event Message
     *
     * @param message Message 객체
     */
    @Override
    public void handleEventMessage(@NonNull Message message) {
        Logger.t(TAG_MACA).d("- handleEventMessage");
    }

    /**
     * initialize activity
     */
    protected void initActivity() {
        Logger.t(TAG_MACA).d("initActivity");
    }

    /**
     * show network error dialog
     *
     * @param activity      Activity
     * @param positiveTitle Button Title
     * @param listener      Button Action Listener
     */
    protected void networkErrorAlertDialog(@NonNull Activity activity,
                                           @NonNull String positiveTitle,
                                           @NonNull OnAlertButtonListener listener) {
        MacadamiaAlertDialog.showAlert(activity,
                false,
                getString(R.string.dialog_title_network_error),
                getString(R.string.dialog_message_network_error),
                positiveTitle,
                listener);
    }

    /**
     * show data loading error dialog
     *
     * @param activity      Activity
     * @param positiveTitle Button Title
     * @param listener      Button Action Listener
     */
    protected void dataLoadingErrorAlertDialog(@NonNull Activity activity,
                                               @NonNull String positiveTitle,
                                               @NonNull OnAlertButtonListener listener) {
        MacadamiaAlertDialog.showAlert(activity,
                false,
                getString(R.string.dialog_title_data_error),
                getString(R.string.dialog_message_data_error),
                positiveTitle,
                listener);
    }

    /**
     * APP 종료 dialog
     *
     * @param activity      Activity
     * @param positiveTitle Button Title
     * @param listener      Button Action Listener
     */
    protected void finishAlertDialog(@NonNull Activity activity,
                                     @NonNull String positiveTitle,
                                     @NonNull OnAlertButtonListener listener) {
        MacadamiaAlertDialog.showAlert(activity,
                false,
                getString(R.string.dialog_title_01),
                getString(R.string.dialog_message_finish),
                positiveTitle,
                listener);
    }

    /**
     * Start Loading
     *
     * @param activity Target Activity
     * @param message  Message
     */
    protected void startLoading(@NonNull Activity activity, @Nullable String message) {
        MacadamiaApplication.getInstance().startLoading(activity, message);
    }

    /**
     * Stop Loading
     */
    protected void stopLoading() {
        MacadamiaApplication.getInstance().stopLoading();
    }


    /**
     * Reset User Info
     */
    protected void resetUserInfo(AuthCheck authCheck) {
        Logger.t(TAG_SSO).d("Reset User Info");
        Macadamia.Functions.setAutoLogin(false);
        Macadamia.Functions.setPushReceive(false);
        Macadamia.Functions.setRuntimePermissionChecked(false);
        Macadamia.Functions.setUserCn(0);
        Macadamia.Functions.setUserEmail(Macadamia.Constants.STRING_BLANK);
        Macadamia.Functions.setUserName(Macadamia.Constants.STRING_BLANK);
        Macadamia.Functions.setLogoffUrl(null);
        Macadamia.Functions.setLoginReturnUrl(Macadamia.Constants.STRING_BLANK);
        Macadamia.Constants.LOGGED = false;
        setSSOCredential(null);

        try {
            SSOStatus ssoStatus = authCheck.logOff();
            Macadamia.Functions.setLogoutStatus(ssoStatus == SSOStatus.SSOSuccess);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * APP finish completely
     */
    protected void finishCompletely() {
        moveTaskToBack(true);
        finishAndRemoveTask();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    /**
     * SSO LogOff
     * @param authCheck
     */
    protected void logOff(AuthCheck authCheck) {
        Constants.LOGGED = false;
        try {
            authCheck.logOff();
        }
        catch (Exception e) {
            Logger.t(TAG_SSO).e("LogOff Exception");
        }
    }
}
