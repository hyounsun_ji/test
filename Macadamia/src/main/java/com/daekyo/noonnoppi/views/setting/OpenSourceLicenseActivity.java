package com.daekyo.noonnoppi.views.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import androidx.annotation.NonNull;

import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.daekyo.noonnoppi.common.ui.MacadamiaWebView;
import com.daekyo.noonnoppi.common.utils.MacadamiaScreenUtils;
import com.daekyo.noonnoppi.views.MacadamiaActivity;
import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.StringUtils;

public class OpenSourceLicenseActivity extends MacadamiaActivity {

    // region
    private static final String TAG = OpenSourceLicenseActivity.class.getSimpleName();
    private MacadamiaWebView openLicenseWebView;
    private ImageButton buttonBack;
    // endregion

    // region
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opensourcelicense);
        // set ApplicationContext
        ctx = getApplicationContext();
        openLicenseWebView = findViewById(R.id.webview_opensourcelicense);
        buttonBack = findViewById(R.id.button_opensourcelicense_back);
        buttonBack.setOnClickListener((v) -> {
            Logger.t(TAG).d("onClick Back");
            finish();
        });
        // Screen 초기화
        initScreenView();
        // webview setting
        openLicenseWebView.getSettings().setUserAgentString(Macadamia.Constants.USER_AGENT);
        openLicenseWebView.setOnLongClickListener(v -> (true));
        openLicenseWebView.setWebViewClient(webViewClient);
        openLicenseWebView.loadUrl("file:///android_asset/html/oss_maca_aos.html");
    }

    /**
     * WebViewClient
     */
    private final WebViewClient webViewClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Logger.t(TAG).d("WebViewClient shouldOverrideUrlLoading request : %s", url);
            if (StringUtils.isNotEmpty(url)) {
                if (url.startsWith("https://") || url.startsWith("http://")) {
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                }
                return true;
            } else {
                return false;
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 화면 재설정
     */
    private void initScreenView() {
        // StatusBar Background Color
//        MacadamiaScreenUtils.changeStatusBarBackgroundColor(MainActivity.this,
//                getResources().getColor(getResources().getIdentifier("colorFFFFFF_100.0", "color", getPackageName()), null));

        // StatusBarTextColor -> Dark
        MacadamiaScreenUtils.changeStatusBarTextColor(OpenSourceLicenseActivity.this, true);
        // NavigationBar 있는 경우 S10+
        if (MacadamiaScreenUtils.hasNavigationBar(ctx)) {
            // NavigationBar Icon Color -> Dark
            MacadamiaScreenUtils.changeNavigationBarIconColor(ctx, OpenSourceLicenseActivity.this, false);
            // NavigationBar Color
            MacadamiaScreenUtils.changeNavigationBarBackgroundColor(OpenSourceLicenseActivity.this,
                    getResources().getColor(getResources().getIdentifier("color373737_100.0", "color", getPackageName()), null));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
    // endregion

    @Override
    public void handleEventMessage(@NonNull Message message) {
        super.handleEventMessage(message);
    }

    @Override
    protected void initActivity() {
        super.initActivity();
    }
}
