package com.daekyo.noonnoppi.views.setting;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.alibaba.fastjson.JSONObject;
import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.daekyo.noonnoppi.common.utils.MacadamiaUtils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import kr.co.nets.sso.appagent.android.AuthCheck;

public class SettingListAdapter extends BaseAdapter {

    // region
    private static final String TAG = SettingListAdapter.class.getSimpleName();

    private static final int ITEM_VIEW_TYPE_BASIC = 0;
    private static final int ITEM_VIEW_TYPE_PERMISSION = 1;
    private static final int ITEM_VIEW_TYPE_VERSION = 2;

    private final Context context;
    private ArrayList<String> settingCells;
    private final LayoutInflater layoutInflater;

    private CheckBox checkBoxAutoLogin;
    private CheckBox checkBoxPush;
    private CheckBox checkBoxCamera;
    private CheckBox checkBoxLocation;
    private TextView textViewLogin;
    private TextView textViewLogout;
    private TextView textViewCurrentVersion;
    private TextView textViewNewerVersion;
    private TextView textViewUserName;
    private TextView textViewUserEmail;
    private Button buttonUpdate;
    private ImageButton buttonOpenSourceLicense;
    private ConstraintLayout beforeLogin;
    private ConstraintLayout afterLogin;
    private Button buttonModifyInfo;

    private final Resources resources;
    private JSONObject appVersion;
    private String localVersionString = "0.0.0";
    private String newVersionString = "0.0.0";

    private final AuthCheck authCheck;

    // endregion

    public SettingListAdapter(Context context, ArrayList<String> list, AuthCheck authCheck) {
        this.context = context;
        this.settingCells = list;
        this.resources = context.getResources();
        this.authCheck = authCheck;
        this.layoutInflater = LayoutInflater.from(this.context);
    }

    public void update(ArrayList<String> list) {
        this.settingCells = list;
        notifyDataSetChanged();
    }

    public void setVersion(ArrayList<String> list, JSONObject appVersion) {
        this.settingCells = list;
        this.appVersion = appVersion;

        JSONObject aos = appVersion.getJSONObject(Macadamia.Keys.PARAM_KEY_AOS);
        String currentVersion = aos.getString(Macadamia.Keys.PARAM_KEY_CURRENT);
        localVersionString = Macadamia.Constants.APP_VERSION;
        newVersionString = StringUtils.defaultIfEmpty(aos.getString(Macadamia.Keys.PARAM_KEY_NEWER), localVersionString);

        int localVersionInt = Integer.parseInt(localVersionString.replaceAll("\\.", ""));
        int currentVersionInt = Integer.parseInt(currentVersion.replaceAll("\\.", ""));
        int newerVersionInt = Integer.parseInt(newVersionString.replaceAll("\\.", ""));

        // set version
        textViewCurrentVersion.setText(String.format(resources.getString(R.string.setting_version_current), localVersionString));
        textViewNewerVersion.setText(String.format(resources.getString(R.string.setting_version_newer), newVersionString));

        if (localVersionInt < newerVersionInt || localVersionInt < currentVersionInt) {
            buttonUpdate.setVisibility(View.VISIBLE);
        } else {
            buttonUpdate.setVisibility(View.INVISIBLE);
        }

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return settingCells.size();
    }

    @Override
    public String getItem(int position) {
        return settingCells.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Context context = parent.getContext();
//        if (convertView == null) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Data Set(listViewItemList)에서 position에 위치한 데이터 참조 획득
        String listViewItem = settingCells.get(position);

        switch (position) {
            case ITEM_VIEW_TYPE_BASIC:
                convertView = inflater.inflate(R.layout.layout_listview_cell_basic, parent, false);
                beforeLogin = convertView.findViewById(R.id.layout_setting_basic_01);
                afterLogin = convertView.findViewById(R.id.layout_setting_basic_02);
                textViewLogin = convertView.findViewById(R.id.button_setting_login);
                checkBoxAutoLogin = convertView.findViewById(R.id.checkbox_setting_auto_login);
                textViewLogout = convertView.findViewById(R.id.button_setting_logout);
                checkBoxPush = convertView.findViewById(R.id.checkbox_setting_push);
                textViewUserName = convertView.findViewById(R.id.textview_setting_login_name);
                textViewUserEmail = convertView.findViewById(R.id.textview_setting_login_email);
                buttonModifyInfo = convertView.findViewById(R.id.button_setting_modify_info);

                if (Macadamia.Constants.LOGGED) {
                    beforeLogin.setVisibility(View.GONE);
                    afterLogin.setVisibility(View.VISIBLE);
                } else {
                    beforeLogin.setVisibility(View.VISIBLE);
                    afterLogin.setVisibility(View.GONE);
                }

                // 로그인이 필요합니다.
                textViewLogin.setOnClickListener(((SettingActivity) context)::onClickLogin);
                // 로그아웃
                textViewLogout.setOnClickListener(((SettingActivity) context)::onClickLogout);
                // 자동 로그인
                checkBoxAutoLogin.setChecked(Macadamia.Functions.getAutoLogin());
                checkBoxAutoLogin.setOnCheckedChangeListener((v, checked) -> {
                    v.setChecked(checked);
                    Macadamia.Functions.setAutoLogin(checked);
                });
                // name
                textViewUserName.setText(Macadamia.Functions.getUserName());
                // email
                textViewUserEmail.setText(Macadamia.Functions.getUserEmail());
                // PUSH 설정
                checkBoxPush.setChecked(Macadamia.Functions.getPushReceive());
                checkBoxPush.setOnCheckedChangeListener(((SettingActivity) context)::onCheckPush);
                // 기본정보 수정
                buttonModifyInfo.setOnClickListener(((SettingActivity) context)::onClickModifyInfo);
                break;

            case ITEM_VIEW_TYPE_PERMISSION:
                convertView = inflater.inflate(R.layout.layout_listview_cell_permission, parent, false);

                checkBoxCamera = convertView.findViewById(R.id.checkbox_setting_camera);
                checkBoxLocation = convertView.findViewById(R.id.checkbox_setting_location);

                boolean cameraPermission = MacadamiaUtils.getPermission(context, Manifest.permission.CAMERA);
                boolean locationPermission = MacadamiaUtils.getPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);

                checkBoxCamera.setChecked(cameraPermission);
                checkBoxLocation.setChecked(locationPermission);

                checkBoxCamera.setOnCheckedChangeListener((compoundButton, b) -> ((SettingActivity) context).onClickTogglePermission("camera", b));
                checkBoxLocation.setOnCheckedChangeListener((compoundButton, b) -> ((SettingActivity) context).onClickTogglePermission("location", b));
                break;

            case ITEM_VIEW_TYPE_VERSION:
                convertView = inflater.inflate(R.layout.layout_listview_cell_version, parent, false);

                // 오픈소스라이선스 고지
                buttonOpenSourceLicense = convertView.findViewById(R.id.button_setting_license_next);
                buttonOpenSourceLicense.setOnClickListener((v) -> {
                    Intent intent = new Intent(context, OpenSourceLicenseActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    context.startActivity(intent);
                });

                buttonUpdate = convertView.findViewById(R.id.button_setting_version_update);
                buttonUpdate.setOnClickListener((v) -> ((SettingActivity) context).oClickButtonVersionUpdate());

                textViewCurrentVersion = convertView.findViewById(R.id.textview_setting_version_current);
                textViewNewerVersion = convertView.findViewById(R.id.textview_setting_version_newver);
                localVersionString = Macadamia.Constants.APP_VERSION;
                String currentVersion = "0.0.0";

                int localVersionInt = Integer.parseInt(localVersionString.replaceAll("\\.", ""));
                int currentVersionInt = Integer.parseInt(currentVersion.replaceAll("\\.", ""));
                int newerVersionInt = Integer.parseInt(newVersionString.replaceAll("\\.", ""));

                // set version
                textViewCurrentVersion.setText(String.format(resources.getString(R.string.setting_version_current), localVersionString));
                textViewNewerVersion.setText(String.format(resources.getString(R.string.setting_version_newer), newVersionString));

                if (localVersionInt < newerVersionInt || localVersionInt < currentVersionInt) {
                    buttonUpdate.setVisibility(View.VISIBLE);
                } else {
                    buttonUpdate.setVisibility(View.INVISIBLE);
                }
                break;
        }
//        }

        return convertView;
    }
}
