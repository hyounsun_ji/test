package com.daekyo.noonnoppi.common.utils;


import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.webkit.URLUtil;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.io.File;
import java.util.UUID;

public class MacadamiaUtils {

    // region 00.Variables
    private static final String TAG = MacadamiaUtils.class.getSimpleName();
    // endregion

    /**
     * get Device ID
     *
     * @param context ApplicationContext
     * @return Device ID String
     */
    public static String getDeviceId(@NonNull Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    /**
     * 현재 Version Name 리턴
     *
     * @param context ApplicationContext
     * @return Version Name String (ex. 1.00.00)
     */
    public static String getVersionName(@NonNull Context context) {
        String versionName = null;
        try {
            PackageInfo i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionName = i.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Logger.t(TAG).e("- getVersionName() %s", e.getMessage());
        }
        return versionName;
    }

    /**
     * Custom User Agent 리턴
     *
     * @param context ApplicationContext
     * @return Custom UserAgent String
     */
    public static String getUserAgent(@NonNull Context context) {
        return new WebView(context).getSettings().getUserAgentString()
                .concat("; Macadamia-aOS/")
                .concat(getVersionName(context));
    }

    /**
     * Custom User Agent for API
     *
     * @param context ApplicationContext
     * @return Custom UserAgent String for API
     */
    public static String getUserAgentForApi(@NonNull Context context) {
        return "Macadamia-aOS/".concat(getVersionName(context));
    }

    /**
     * get Version Code
     *
     * @param context Application Context
     * @return Version Code(ex. 1234)
     */
    public static int getVersionCode(@NonNull Context context) {
        int versionCode = 0;
        try {
            PackageInfo i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionCode = i.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Logger.t(TAG).e("- getVersionCode() %s", e.getMessage());
        }
        return versionCode;
    }

    /**
     * UUID 생성 - 대문자
     *
     * @return UUID String
     */
    public static String getUUID() {
        return getUUID(true);
    }

    /**
     * UUID 생성
     *
     * @param isUpperCase 대문자 여부
     * @return UUID String
     */
    public static String getUUID(boolean isUpperCase) {
        String uuid = UUID.randomUUID().toString();
        return isUpperCase ? uuid.toUpperCase() : uuid.toLowerCase();
    }

    /**
     * Package 설치 여부
     *
     * @param context     ApplicationContext
     * @param packageName Package Name
     * @return 설치 여부 boolean
     */
    public static boolean isPackageInstalled(@NonNull Context context, @NonNull String packageName) {
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(packageName.trim(), PackageManager.GET_META_DATA);
            ApplicationInfo appInfo = pi.applicationInfo;

            return appInfo.enabled;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * PlayStore 이동
     *
     * @param activity    Activity
     * @param packageName Package Name
     */
    public static void goPlayStore(@NonNull Activity activity, @NonNull String packageName) {
        if (StringUtils.isNotBlank(packageName)) {
            String appMarketUrl = "market://details?id=" + packageName;
            String webMarketUrl = "https://play.google.com/store/apps/details?id=" + packageName;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(appMarketUrl));
            // forbid launching activities without BROWSABLE category
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            // forbid explicit call
            intent.setComponent(null);
            // forbid Intent with selector Intent
            intent.setSelector(null);

            try {
                activity.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                intent.setData(Uri.parse(webMarketUrl));
                activity.startActivity(intent);
            }
        }
    }

    /**
     * Invoke App
     *
     * @param context ApplicationContext
     * @param url     URL String
     */
    public static void invokeNativeApp(@NonNull Context context, @NonNull String url) {
        try {
            if (StringUtils.isNotEmpty(url)) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * file download
     *
     * @param context            ApplicationContext
     * @param url                file url
     * @param userAgent          UserAgent
     * @param contentDisposition ContentDisposition
     * @param mimeType           MimeType
     * @param description        Description
     */
    public static void fileDownload(@NonNull Context context,
                                    @NonNull String url,
                                    @NonNull String userAgent,
                                    @NonNull String contentDisposition,
                                    @NonNull String mimeType,
                                    @Nullable String description) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

        String fileName = URLUtil.guessFileName(url, contentDisposition, mimeType);

        // set MimeType
        request.setMimeType(mimeType);
        //------------------------COOKIE!!------------------------
        String cookies = android.webkit.CookieManager.getInstance().getCookie(url);
        request.addRequestHeader("cookie", cookies);
        //------------------------COOKIE!!------------------------
        request.addRequestHeader("User-Agent", userAgent);
        request.setDescription(StringUtils.defaultIfEmpty(description, fileName));
        request.setTitle(fileName);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);

        DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        dm.enqueue(request);
    }

    public static boolean getPermission(Context context, String permission) {
        return (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * get App Cache Path String
     *
     * @param context Application Context
     * @return cache path String
     */
    public static String getAppCachePath(Context context) {
        File cacheDir = context.getCacheDir();
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        return cacheDir.getPath();
    }

    /**
     * get Current Datetime with Format
     *
     * @param formatString Format String
     * @return Current DateTime String
     */
    public static String getCurrentDateTime(@Nullable String formatString) {
        DateTime dateTime = new DateTime();
        formatString = StringUtils.defaultIfEmpty(formatString, "yyyyMMddHHmmss");
        return dateTime.toString(formatString);
    }
}
