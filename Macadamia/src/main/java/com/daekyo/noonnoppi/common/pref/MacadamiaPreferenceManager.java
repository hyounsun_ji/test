package com.daekyo.noonnoppi.common.pref;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;

import java.util.Set;

/**
 * MacadamiaPrefencesManager
 */
public class MacadamiaPreferenceManager {

    private static MacadamiaPreferenceManager instance = null;
    private final Context context;

    /* SharedPreferences */
    private static final String PREF_MACADAMIA = "MACADAMIA";
    private final String preferenceKey;
    private final int mode;
    private static final String STRING_BLANK = "";

    /**
     * Private Constructor
     *
     * @param context ApplicationContext
     */
    private MacadamiaPreferenceManager(@NonNull Context context, @Nullable String preferenceKey) {
        this.context = context;
        this.mode = Context.MODE_PRIVATE;
        this.preferenceKey = StringUtils.defaultIfEmpty(preferenceKey, PREF_MACADAMIA);
    }

    /**
     * get OLTPreferenceManager Instance
     *
     * @param context ApplicationContext
     * @return preference manager
     */
    public static MacadamiaPreferenceManager getInstance(@NonNull Context context) {
        return getInstance(context, PREF_MACADAMIA);
    }

    /**
     * get OLTPreferenceManager Instance
     *
     * @param context       ApplicationContext
     * @param preferenceKey Preference Key
     * @return preference manager
     */
    public static MacadamiaPreferenceManager getInstance(@NonNull Context context, @Nullable String preferenceKey) {
        if (instance == null) {
            instance = new MacadamiaPreferenceManager(context, preferenceKey);
        }
        return instance;
    }

    /**
     * get Preferences
     *
     * @param name preference name string
     * @param mode reference mode
     * @return prehfrences
     */
    private SharedPreferences getPreferences(@NonNull String name, int mode) {
        return context.getSharedPreferences(name, mode);
        //return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * get Preferences
     *
     * @return preferences
     */
    private SharedPreferences getPreferences() {
        return this.getPreferences(preferenceKey, mode);
    }

    /**
     * get SharedPreferencesEditor
     *
     * @return editor
     */
    private SharedPreferences.Editor getEditor() {
        return this.getPreferences().edit();
    }

    /**
     * get String
     *
     * @param _key keyString
     * @return String Value
     */
    public String getPrefStringInfo(@NonNull String _key) {
        return getPreferences().getString(_key, STRING_BLANK);
    }

    /**
     * set String
     *
     * @param _key   keyString
     * @param _value value
     */
    public void setPrefStringInfo(@NonNull String _key, @Nullable String _value) {
        SharedPreferences.Editor editor = this.getEditor();
        editor.putString(_key, _value);
        editor.apply();
    }

    /**
     * get Bool
     *
     * @param _key keyString
     * @return BoolValue
     */
    public boolean getPrefBoolInfo(@NonNull String _key) {
        return getPreferences().getBoolean(_key, false);
    }

    /**
     * set Bool
     *
     * @param _key   keyString
     * @param _value value
     */
    public void setPrefBoolInfo(@NonNull String _key, boolean _value) {
        SharedPreferences.Editor editor = this.getEditor();
        editor.putBoolean(_key, _value);
        editor.apply();
    }

    /**
     * get String Value
     *
     * @param _key keyString
     * @return String Value
     */
    public String getString(@NonNull String _key) {
        return getString(_key, STRING_BLANK);
    }

    /**
     * get String Value with Default String
     *
     * @param _key          keyString
     * @param defaultString default value
     * @return String Value
     */
    public String getString(@NonNull String _key, @NonNull String defaultString) {
        return getPreferences().getString(_key, defaultString);
    }

    /**
     * set String Value
     *
     * @param _key   keyString
     * @param _value value
     */
    public void setString(@NonNull String _key, @NonNull String _value) {
        SharedPreferences.Editor editor = this.getEditor();
        editor.putString(_key, _value);
        editor.apply();
    }

    /**
     * get Boolean Value
     *
     * @param _key keyString
     * @return Boolean Value
     */
    public boolean getBoolean(@NonNull String _key) {
        return getPreferences().getBoolean(_key, false);
    }

    /**
     * get Boolean Value
     *
     * @param _key           keyString
     * @param defaultBoolean default value
     * @return Boolean Value
     */
    public boolean getBoolean(@NonNull String _key, boolean defaultBoolean) {
        return getPreferences().getBoolean(_key, defaultBoolean);
    }

    /**
     * set Boolean Value
     *
     * @param _key   keyString
     * @param _value value
     */
    public void setBoolean(@NonNull String _key, boolean _value) {
        SharedPreferences.Editor editor = this.getEditor();
        editor.putBoolean(_key, _value);
        editor.apply();
    }

    /**
     * get Integer Value
     *
     * @param _key keyString
     * @return Integer Value
     */
    public int getInt(@NonNull String _key) {
        return getPreferences().getInt(_key, 0);
    }

    /**
     * set Integer Value
     *
     * @param _key   keyString
     * @param _value value
     */
    public void setInt(@NonNull String _key, int _value) {
        SharedPreferences.Editor editor = this.getEditor();
        editor.putInt(_key, _value);
        editor.apply();
    }

    /**
     * get Long Value
     *
     * @param _key keyString
     * @return Long Value
     */
    public long getLong(@NonNull String _key) {
        return getPreferences().getLong(_key, 0);
    }

    /**
     * set Long Value
     *
     * @param _key   keyString
     * @param _value value
     */
    public void setLong(@NonNull String _key, long _value) {
        SharedPreferences.Editor editor = this.getEditor();
        editor.putLong(_key, _value);
        editor.apply();
    }

    /**
     * get Float Value
     *
     * @param _key keyString
     * @return Float Value
     */
    public float getFloat(@NonNull String _key) {
        return getPreferences().getFloat(_key, 0);
    }

    /**
     * set Float Value
     *
     * @param _key   keyString
     * @param _value value
     */
    public void setFloat(@NonNull String _key, float _value) {
        SharedPreferences.Editor editor = this.getEditor();
        editor.putFloat(_key, _value);
        editor.apply();
    }

    /**
     * get String Set
     *
     * @param _key keyString
     * @return StringSet value
     */
    public Set<String> getStringSet(@NonNull String _key) {
        return getPreferences().getStringSet(_key, null);
    }

    /**
     * set String Set
     *
     * @param _key    keyString
     * @param _values value
     */
    public void setStringSet(@NonNull String _key, Set<String> _values) {
        SharedPreferences.Editor editor = this.getEditor();
        editor.putStringSet(_key, _values);
        editor.apply();
    }

    /**
     * remove value for key
     *
     * @param _key keyString
     */
    public void remove(@NonNull String _key) {
        SharedPreferences.Editor editor = this.getEditor();
        editor.remove(_key);
        editor.apply();
    }
}
