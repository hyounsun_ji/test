package com.daekyo.noonnoppi.common.api;

public enum MacadamiaRequestType {
    REQUEST_APP_INFO,
    REQUEST_USER_INFO,
    REQUEST_MENU,
    REQUEST_ORANGE_DOT,
    UPDATE_ORANGE_DOT,
    UPDATE_PUSH_TOKEN,
    REQUEST_ERROR_REPORT
}
