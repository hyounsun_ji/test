package com.daekyo.noonnoppi.common.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.daekyo.noonnoppi.R;

import org.apache.commons.lang3.StringUtils;

public class MacadamiaAlertDialog {
    private static final String TAG = MacadamiaAlertDialog.class.getSimpleName();

    /**
     * alert dialog
     *
     * @param activity    Activity to show
     * @param hasClose    will have close button
     * @param title       dialog title
     * @param message     dialog message
     * @param buttonTitle button title
     * @param listener    button event listener
     */
    public static void showAlert(@NonNull Activity activity,
                                 boolean hasClose,
                                 @NonNull String title,
                                 @NonNull String message,
                                 @NonNull String buttonTitle,
                                 @NonNull OnAlertButtonListener listener) {
        showAlertDialog(activity, hasClose, title, message, buttonTitle, listener, null, null);
    }

    /**
     * confirm dialog
     *
     * @param activity               Activity to show
     * @param hasClose               will have close button
     * @param title                  dialog title
     * @param message                dialog message
     * @param positiveButtonTitle    positive button title
     * @param positiveButtonListener positive button event listener
     * @param negativeButtonTitle    negative button title
     * @param negativeButtonListener negative button event listener
     */
    public static void showConfirm(@NonNull Activity activity,
                                   boolean hasClose,
                                   @NonNull String title,
                                   @NonNull String message,
                                   @NonNull String positiveButtonTitle,
                                   @NonNull OnAlertButtonListener positiveButtonListener,
                                   @NonNull String negativeButtonTitle,
                                   @NonNull OnAlertButtonListener negativeButtonListener) {
        showAlertDialog(activity, hasClose, title, message, positiveButtonTitle, positiveButtonListener, negativeButtonTitle, negativeButtonListener);
    }

    private static AlertDialog dialog = null;

    private static void showAlertDialog(@NonNull Activity activity,
                                        boolean hasClose,
                                        @NonNull String title,
                                        @NonNull String message,
                                        @NonNull String positiveButton,
                                        @NonNull OnAlertButtonListener positiveButtonListener,
                                        @Nullable String negativeButton,
                                        @Nullable OnAlertButtonListener negativeButtonListener) {

        if (dialog != null && dialog.isShowing()) {
            return;
        }

        if (activity != null && !activity.isFinishing()) {
            View layout = LayoutInflater.from(activity).inflate(R.layout.layout_dialog, null, false);
            View layoutButton1 = layout.findViewById(R.id.layout_dialog_button1);
            View layoutButton2 = layout.findViewById(R.id.layout_dialog_button2);

            Button button01_01 = layout.findViewById(R.id.button_01_01);
            Button button02_01 = layout.findViewById(R.id.button_02_01);
            Button button02_02 = layout.findViewById(R.id.button_02_02);
            ImageButton buttonClose = layout.findViewById(R.id.button_dialog_close);

            TextView textViewTitle = layout.findViewById(R.id.textview_dialog_title);
            textViewTitle.setText(title);
            TextView textViewMessage = layout.findViewById(R.id.textview_dialog_message);
            textViewMessage.setText(message);

            // close button
            if (hasClose) {
                buttonClose.setVisibility(View.VISIBLE);
                buttonClose.setOnClickListener((view) -> dialog.dismiss());
            } else {
                buttonClose.setVisibility(View.GONE);
            }

            button01_01.setText(positiveButton);
            button02_02.setText(positiveButton);

            // alert or confirm
            if (StringUtils.isEmpty(negativeButton)) {
                layoutButton1.setVisibility(View.VISIBLE);
                layoutButton2.setVisibility(View.GONE);

                button01_01.setOnClickListener((view) -> {
                    dialog.dismiss();
                    dialog = null;

                    positiveButtonListener.onButtonClick();
                });
            } else {
                layoutButton1.setVisibility(View.GONE);
                layoutButton2.setVisibility(View.VISIBLE);

                button02_01.setText(negativeButton);

                button02_01.setOnClickListener((view) -> {
                    dialog.dismiss();
                    dialog = null;
                    negativeButtonListener.onButtonClick();
                });

                button02_02.setOnClickListener((view) -> {
                    dialog.dismiss();
                    dialog = null;

                    positiveButtonListener.onButtonClick();
                });

            }

            dialog = new AlertDialog.Builder(activity).create();
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x0000ff00));
            dialog.setContentView(layout);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
        }
    }
}
