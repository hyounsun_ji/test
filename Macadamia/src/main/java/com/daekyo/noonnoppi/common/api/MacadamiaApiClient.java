package com.daekyo.noonnoppi.common.api;

import android.content.Context;

import androidx.annotation.NonNull;

import com.daekyo.noonnoppi.common.Macadamia;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.fastjson.FastJsonConverterFactory;

public class MacadamiaApiClient {

    // region 00.Variables
    private static final String TAG = MacadamiaApiClient.class.getSimpleName();

    private final MacadamiaApiService apiService;
    // endregion

    private static class LazyHolder {
        private static final MacadamiaApiClient INSTANCE = new MacadamiaApiClient();
    }

    /**
     * get MacadamiaApiClient Instance
     *
     * @return MacadamiaApiClient 인스턴스
     */
    public static MacadamiaApiClient getInstance() {
        return LazyHolder.INSTANCE;
    }

    /**
     * Constructor
     */
    private MacadamiaApiClient() {
        // http logging interceptor
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(message -> Logger.t(TAG).i("%s", message));

        // stetho interceptor
        StethoInterceptor stethoInterceptor = new StethoInterceptor();

        // interceptor
        Interceptor interceptor = (chain) -> {
            Request originalRequest = chain.request();

            Request request = originalRequest.newBuilder()
                    .header(Macadamia.Keys.KEY_HEADER_USER_AGENT, Macadamia.Constants.USER_AGENT_FOR_API)
                    .header(Macadamia.Keys.KEY_HEADER_APP_VERSION, Macadamia.Constants.APP_VERSION)
                    .header(Macadamia.Keys.KEY_HEADER_OS, Macadamia.Constants.DEVICE_OS)
                    .header(Macadamia.Keys.PARAM_KEY_APP_ID, Macadamia.Constants.APP_ID)
                    .header(Macadamia.Keys.KEY_HEADER_APP_VERSION, Macadamia.Constants.APP_VERSION)
                    .header(Macadamia.Keys.KEY_HEADER_ACCEPT, Macadamia.Constants.HEADER_ACCEPT_JSON)
                    .header(Macadamia.Keys.KEY_HEADER_CONTENT_TYPE, Macadamia.Constants.HEADER_CONTENT_TYPE_JSON)
                    .method(originalRequest.method(), originalRequest.body())
                    .build();

            return chain.proceed(request);
        };

        // set LogLevel
        httpLoggingInterceptor.level(HttpLoggingInterceptor.Level.BASIC);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(stethoInterceptor)
                .addInterceptor(httpLoggingInterceptor)
                .protocols(new ArrayList<>(Arrays.asList(Protocol.HTTP_2, Protocol.HTTP_1_1)))
                .connectTimeout(Macadamia.Constants.HTTP_CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(Macadamia.Constants.HTTP_READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(Macadamia.Constants.HTTP_WRITE_TIMEOUT, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Macadamia.Urls.URL_API)
                .addConverterFactory(FastJsonConverterFactory.create())
                .client(client)
                .build();

        // ApiService
        apiService = retrofit.create(MacadamiaApiService.class);
    }

    /**
     * get MCApiService
     *
     * @return MCApiServie
     */
    public MacadamiaApiService getApiService() {
        return apiService;
    }

    public static class Builder<T> {
        private final Context context;
        private String contentType = "application/json; charset=UTF-8";
        private String charset = "UTF-8";
        private String userAgent;
        private Class<T> apiService;
        private Class<T> webService;
        private String apiUrl;
        private String webUrl;
        private HttpLoggingInterceptor.Level logLevel = HttpLoggingInterceptor.Level.BODY;
        private int connectTimeout = 30;
        private int readTimeout = 30;
        private int writeTimeout = 30;

        public Builder(@NonNull Context context) {
            this.context = context;
        }

        public MacadamiaApiClient.Builder setApiBaseUrl(String apiUrl) {
            this.apiUrl = apiUrl;
            return this;
        }

        public MacadamiaApiClient.Builder setWebBaseUrl(String webUrl) {
            this.webUrl = webUrl;
            return this;
        }

        public MacadamiaApiClient.Builder setContentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public MacadamiaApiClient.Builder setCharsetEncoding(String charset) {
            this.charset = charset;
            return this;
        }

        public MacadamiaApiClient.Builder setUserAgent(String userAgent) {
            this.userAgent = userAgent;
            return this;
        }

        public MacadamiaApiClient.Builder setLogLevel(HttpLoggingInterceptor.Level logLevel) {
            this.logLevel = logLevel;
            return this;
        }

        public MacadamiaApiClient.Builder setApiService(Class<T> apiService) {
            this.apiService = apiService;
            return this;
        }

        public MacadamiaApiClient.Builder setWebService(Class<T> webService) {
            this.webService = webService;
            return this;
        }

        public MacadamiaApiClient.Builder setConnectTimeout(int connectTimeout) {
            this.connectTimeout = connectTimeout;
            return this;
        }

        public MacadamiaApiClient.Builder setReadTimeout(int readTimeout) {
            this.readTimeout = readTimeout;
            return this;
        }

        public MacadamiaApiClient.Builder setWriteTimeout(int writeTimeout) {
            this.writeTimeout = writeTimeout;
            return this;
        }

        @NonNull
        public MacadamiaApiClient getApiClient() {
            return MacadamiaApiClient.getInstance();
        }

    }
}
