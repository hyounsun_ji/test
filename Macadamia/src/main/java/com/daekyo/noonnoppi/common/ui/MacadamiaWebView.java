package com.daekyo.noonnoppi.common.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.daekyo.noonnoppi.common.utils.MacadamiaUtils;

import static org.apache.commons.lang3.CharEncoding.UTF_8;

public class MacadamiaWebView extends WebView {

    public interface WebViewScrollListener {
        void onScrollUp();

        void onScrollDown();

        void onScrollFinished();

        void onScrollTop();

        void onScrollChanged(int scrollX, int scrollY, int oldScrollX, int oldScrollY);
    }

    private boolean initLoaded = false;
    private boolean observable = true;
    private WebViewScrollListener scrollListener;
    private Context context;

    public MacadamiaWebView(@NonNull Context context) {
        super(context);
    }

    public MacadamiaWebView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initWebView(context);
    }

    public void setObservable(boolean observable) {
        this.observable = observable;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    protected void onScrollChanged(final int scrollX, final int scrollY, final int oldScrollX, final int oldScrollY) {
        if (scrollListener != null && observable) {
            scrollListener.onScrollChanged(scrollX, scrollY, oldScrollX, oldScrollY);
            boolean isTop = false;
            if (scrollY == 0 && oldScrollY >= 0) {
                isTop = true;
            } else {
                if (scrollY > oldScrollY) {
                    scrollListener.onScrollDown();
                } else if (scrollY < oldScrollY) {
                    scrollListener.onScrollUp();
                }
            }
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
            }
            checkScrolling(isTop);
        }
        super.onScrollChanged(scrollX, scrollY, oldScrollX, oldScrollY);
    }

    public boolean isInitLoaded() {
        return this.initLoaded;
    }

    public void setInitLoaded(Boolean initLoaded) {
        this.initLoaded = initLoaded;
    }

    public void setScrollListener(final WebViewScrollListener scrollListener) {
        this.scrollListener = scrollListener;
    }

    public WebViewScrollListener getScrollListener() {
        return this.scrollListener;
    }

    final Handler handler = new Handler();

    /**
     * check WebView Scrolling
     *
     * @param isTop where scroll top or not
     */
    private void checkScrolling(boolean isTop) {
        handler.postDelayed(() -> {
            if (scrollListener != null) {
                if (isTop) {
                    scrollListener.onScrollTop();
                } else {
                    scrollListener.onScrollFinished();
                }
            }
        }, 10);
    }

    /**
     * initialize WebView Setting
     *
     * @param context Context
     */
    private void initWebView(Context context) {
        WebSettings settings = getSettings();
        // WebView Setting
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccess(true);
        settings.setJavaScriptEnabled(true);
        settings.setLoadsImagesAutomatically(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setDefaultTextEncodingName(UTF_8);
        settings.setGeolocationEnabled(true);
        settings.setBuiltInZoomControls(false);
        // http resource 호출
        settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        settings.setAppCacheEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setSaveFormData(false);
        settings.setSupportMultipleWindows(true);
        settings.setDisplayZoomControls(false);
        settings.setSupportZoom(false);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        // WebView LocalStorage
        settings.setDatabaseEnabled(true);
        // App Cache Path 설정
        settings.setAppCachePath(MacadamiaUtils.getAppCachePath(context));
        settings.setDomStorageEnabled(true);
        // ScrollBar
        setVerticalScrollBarEnabled(true);
        setHorizontalScrollBarEnabled(false);
    }
}
