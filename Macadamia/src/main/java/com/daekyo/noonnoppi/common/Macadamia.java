package com.daekyo.noonnoppi.common;

import android.Manifest;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.daekyo.noonnoppi.BuildConfig;
import com.daekyo.noonnoppi.common.pref.MacadamiaPreferenceManager;
import com.daekyo.noonnoppi.common.utils.MacadamiaUtils;

public final class Macadamia {

  public static class Constants {
    public static final String APP_ID = "macadamia";
    public static final String STRING_BLANK = "";
    public static final String STRING_SPACE = " ";
    public static final String DEVICE_OS = "aos";
    public static final String DEVICE_OS_VERSION = Build.VERSION.RELEASE;
    public static final String DEVICE_MODEL = Build.MODEL;
    public static final String APP_VERSION = MacadamiaUtils.getVersionName(MacadamiaApplication.getAppContext());
    public static final String DEVICE_ID = MacadamiaUtils.getDeviceId(MacadamiaApplication.getAppContext());
    public static final String SESSION_ID = Functions.getSessionId();
    public static final int RUN_COUNT = Functions.getRunCount();

    public static final String HEADER_ACCEPT_JSON = "application/json";
    public static final String HEADER_ACCEPT_PLAIN = "text/plain";
    public static final String HEADER_CONTENT_TYPE_JSON = "application/json; charset=UTF-8";

    // UserAgent for WEB SSO
    public static final String USER_AGENT = String.format("%s %s", MacadamiaUtils.getUserAgent(MacadamiaApplication.getAppContext()), SSO.SSO_USER_AGENT);
    // UserAgent for API SSO
    public static final String USER_AGENT_FOR_API = String.format("%s %s", MacadamiaUtils.getUserAgentForApi(MacadamiaApplication.getAppContext()), SSO.SSO_USER_AGENT);

    public static final int HTTP_CONNECT_TIMEOUT = 30;
    public static final int HTTP_READ_TIMEOUT = 30;
    public static final int HTTP_WRITE_TIMEOUT = 30;

    // default notification channel ID
    public static final String MACA_NOTIFICATION_CHANNEL_ID_EVENT = "maca_channel_event_id";
    public static boolean LOGGED = false;
    public static long APP_BACKGROUND_TIMESTAMP = 0;
    public static long APP_FOREGROUND_TIMESTAMP = 0;
    public static int APP_BACKGROUND_STAYING_TIME = 0;

    // 60s * 30m ==> 1800s
    public static final int EXPIRE_SECONDS = 1800;

    // Zendesk cn enc key
    public static final String ZENDESK_ENC_KEY = "80Q2AMWWWV2JTX5QGOM6IGY1RCQVRQFX";
    public static final String ZENDESK_ENC_IV = "Y47CW3BABPY0D3X0";
  }

  public static class Functions {
    public static int getRunCount() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getInt(Macadamia.Keys.PREF_RUN_COUNT);
    }

    public static void incRunCount() {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setInt(Macadamia.Keys.PREF_RUN_COUNT, Constants.RUN_COUNT + 1);
    }

    /**
     * get Session ID
     *
     * @return returns Session ID String if not generate new Session ID(UUID)
     */
    public static String getSessionId() {
      String sessionId = MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getString(Keys.PREF_KEY_SESSION_ID, MacadamiaUtils.getUUID(true));
      setSessionId(sessionId);
      return sessionId;
    }

    /**
     * set Session ID
     *
     * @param sessionId Session ID String
     */
    public static void setSessionId(String sessionId) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setString(Keys.PREF_KEY_SESSION_ID, sessionId);
    }

    public static boolean getPermissionViewChecked() {
      boolean permissionChecked = MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getBoolean(Keys.PREF_KEY_PERMISSION_CHECK, false);
      setPermissionViewChecked(permissionChecked);
      return permissionChecked;
    }

    public static void setPermissionViewChecked(boolean checked) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setBoolean(Keys.PREF_KEY_PERMISSION_CHECK, checked);
    }

    public static void setUserCn(int userCn) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setInt(Keys.PREF_KEY_USER_CN, userCn);
    }

    public static int getUserCn() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getInt(Keys.PREF_KEY_USER_CN);
    }

    public static void setUserEmail(String userEmail) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setString(Keys.PREF_KEY_USER_EMAIL, userEmail);
    }

    public static String getUserEmail() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getString(Keys.PREF_KEY_USER_EMAIL, Constants.STRING_BLANK);
    }

    public static void setUserName(String userName) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setString(Keys.PREF_KEY_USER_NAME, userName);
    }

    public static String getUserName() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getString(Keys.PREF_KEY_USER_NAME, Constants.STRING_BLANK);
    }

    public static void setCookieValue(String cookieValue) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setString(Keys.PREF_KEY_COOKIE, cookieValue);
    }

    public static String getCookieValue() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getString(Keys.PREF_KEY_COOKIE);
    }

    public static JSONObject getMenu() {
      String menuString = MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getString(Keys.PREF_KEY_MENU);
      return JSON.parseObject(menuString);
    }

    public static void setMenu(JSONObject menu) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setString(Keys.PREF_KEY_MENU, menu.toJSONString());
    }

    public static void setRuntimePermissionChecked(boolean checked) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setBoolean(Keys.PREF_KEY_RUNTIME_PERMISSION, checked);
    }

    public static boolean getRuntimePermissionChecked() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getBoolean(Keys.PREF_KEY_RUNTIME_PERMISSION, false);
    }

    public static void setPushToken(String registrationId) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setString(Keys.PREF_KEY_FCM_TOKEN, registrationId);
    }

    public static String getPushToken() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getString(Keys.PREF_KEY_FCM_TOKEN);
    }

    public static void setPushReceive(boolean receive) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setBoolean(Keys.PREF_KEY_PUSH_RECEIVE, receive);
    }

    public static boolean getPushReceive() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getBoolean(Keys.PREF_KEY_PUSH_RECEIVE);
    }

    public static void setAutoLogin(boolean autologin) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setBoolean(Keys.PREF_KEY_AUTO_LOGIN, autologin);
    }

    public static boolean getAutoLogin() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getBoolean(Keys.PREF_KEY_AUTO_LOGIN, false);
    }

    public static void setMainLoadUrl(String urlString) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setString(Keys.PREF_KEY_MAIN_REDIRECT_URL, urlString);
    }

    public static String getMainLoadUrl() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getString(Keys.PREF_KEY_MAIN_REDIRECT_URL, Constants.STRING_BLANK);
    }

    public static void setLoginReturnUrl(String returnUrl) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setString(Keys.PREF_KEY_RETURN_URL, returnUrl);
    }

    public static String getLoginReturnUrl() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getString(Keys.PREF_KEY_RETURN_URL, Urls.WEB_URL_MAIN);
    }

    public static void setLogoffUrl(String logoffUrl) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setString(Keys.PREF_KEY_LOGOFF_URL, logoffUrl);
    }

    public static String getLogoffUrl() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getString(Keys.PREF_KEY_LOGOFF_URL, Urls.WEB_URL_MAIN);
    }

    public static void setLogoutStatus(boolean logout) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setBoolean(Keys.PREF_KEY_LOGOUT, logout);
    }

    public static boolean getLogoutStatus() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getBoolean(Keys.PREF_KEY_LOGOUT, false);
    }

    public static void setLocationPermissionDialog(boolean show) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setBoolean(Keys.PREF_LOCATION_PERMISSION_DIALOG, show);
    }

    public static boolean getLocationPermissionDialog() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getBoolean(Keys.PREF_LOCATION_PERMISSION_DIALOG, true);
    }

    /**
     * 선택적 권한 별 체크 여부
     *
     * @param permission Permission String
     * @param checked    checked or not
     */
    public static void setPermissionChecked(String permission, boolean checked) {
      String key = null;
      if (permission.equals(Manifest.permission.CAMERA)) {
        key = Keys.PREF_KEY_LAUNCH_PERMISSION_CAMERA;
      } else if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
        key = Keys.PREF_KEY_LAUNCH_PERMISSION_LOCATION;
      }
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setBoolean(key, checked);
    }

    /**
     * 선택적 권한 별 체크 여부
     *
     * @param permission Permission String
     * @return return checked or not
     */
    public static boolean getPermissionChecked(String permission) {
      String key = null;
      if (permission.equals(Manifest.permission.CAMERA)) {
        key = Keys.PREF_KEY_LAUNCH_PERMISSION_CAMERA;
      } else if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
        key = Keys.PREF_KEY_LAUNCH_PERMISSION_LOCATION;
      }
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getBoolean(key, false);
    }

    public static void setLaunchPushChecked(boolean checked) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setBoolean(Keys.PREF_LAUNCH_PUSH, checked);
    }

    public static boolean getLaunchPushChecked() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getBoolean(Keys.PREF_LAUNCH_PUSH, false);
    }

    public static void setGuideChecked(boolean checked) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setBoolean(Keys.PREF_GUIDE_CHECKED, checked);
    }

    public static boolean getGuideChecked() {
      return MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getBoolean(Keys.PREF_GUIDE_CHECKED, false);
    }

    public static void setOutlinks(@NonNull JSONArray outlinks) {
      MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .setString(Keys.PREF_KEY_OUTLINKS, outlinks.toJSONString());
    }

    public static JSONArray getOutlinks() {
      String links = MacadamiaPreferenceManager.getInstance(MacadamiaApplication.getAppContext())
        .getString(Keys.PREF_KEY_OUTLINKS);
      return JSONArray.parseArray(links);
    }
  }

  public static class SSO {
    private static final String devTargetID = "d-sso6.daekyo.com";
    private static final String realTargetID = "sso3.daekyo.com";
    public static final String targetID = BuildConfig.IS_DEV ? devTargetID : realTargetID;

    private static final String devArtifactID = "https://d-sso6.daekyo.com/nsso-agent/default.jsp";
    private static final String realArtifactID = "https://sso3.daekyo.com/nsso-agent/default.jsp";
    public static final String artifactID = BuildConfig.IS_DEV ? devArtifactID : realArtifactID;

    private static final String devSSOUrl = "https://d-sso6.daekyo.com/WebSSO/AuthWeb/logonService.do";
    private static final String realSSOUrl = "https://sso3.daekyo.com/WebSSO/AuthWeb/logonService.do";
    public static final String SSOUrl = BuildConfig.IS_DEV ? devSSOUrl : realSSOUrl;

    public static final String SSO_USER_AGENT = "os:android_app";
  }

  public static class Intent {
    public static final String INTENT_ACTION_SHORTCUT = "android.intent.action.SHORTCUT_VIEW";
    public static final String INTENT_ACTION_PUSH_LINK = "android.intent.action.PUSH_LINK_VIEW";
    public static final String KEY_LINK_URL = "urlLink";
  }

  public static class Urls {
    private static final String URL_WEB_DEV = "https://d3.macadamia.kr";
    private static final String URL_WEB_STAGE = "https://s1.macadamia.kr";
    private static final String URL_WEB_REAL = "https://www.macadamia.kr";
    public static final String URL_WEB = BuildConfig.IS_DEV ? URL_WEB_DEV : (BuildConfig.IS_STAGE ? URL_WEB_STAGE : URL_WEB_REAL);

    private static final String URL_API_DEV = "https://d3-api.macadamia.kr";
    private static final String URL_API_STAGE = "https://s1-api.macadamia.kr";
    private static final String URL_API_REAL = "https://api.macadamia.kr";
    public static final String URL_API = BuildConfig.IS_DEV ? URL_API_DEV : (BuildConfig.IS_STAGE ? URL_API_STAGE : URL_API_REAL);

    public static final String WEB_URL_NOONNOPPI = URL_WEB.concat("/brand/intro?brandId=179&brandCd=NOOP");
    public static final String WEB_URL_SUMMIT_MATH = URL_WEB.concat("/brand/intro?brandId=298&brandCd=SUMM");
    public static final String WEB_URL_CAIHONG = URL_WEB.concat("/brand/intro?brandId=200&brandCd=CHAI");
    public static final String WEB_URL_SOLUNY = URL_WEB.concat("/brand/intro?brandId=181&brandCd=SOLU");
    public static final String WEB_URL_LOGIN = URL_WEB.concat("/user/login");
    public static final String WEB_URL_MODIFY_INFO = URL_WEB.concat("/my/user/modify-user");
    public static final String WEB_URL_LOGOUT = URL_WEB.concat("/logout");
    public static final String MACA_LINK_URL = "";
    public static final String WEB_URL_SEARCH = URL_WEB.concat("/?appSearchYn=Y");
    public static final String WEB_URL_MAIN = URL_WEB.concat("/");
    public static final String WEB_URL_MY_SERVICE = URL_WEB.concat("/my/main");
    public static final String WEB_URL_BRAND = URL_WEB.concat("/brand/main");
    public static final String WEB_URL_PRODUCT = URL_WEB.concat("/products/main");
    public static final String WEB_URL_PLACE = URL_WEB.concat("/place/main");
    public static final String WEB_URL_EVENT = URL_WEB.concat("/event/list");
    public static final String WEB_URL_COMMUNITY = URL_WEB.concat("/community/main");
    public static final String WEB_URL_CART = URL_WEB.concat("/my/cart");
    public static final String WEB_URL_PICK = URL_WEB.concat("/my/setup/feed");
    public static final String WEB_URL_RECENT = URL_WEB.concat("/recent-view");
    public static final String WEB_URL_TALK = URL_WEB.concat("/chat/main");
    //public static final String WEB_URL_CS_CENTER = URL_WEB.concat("/cs");
    public static final String WEB_URL_CS_CENTER = "https://macadamia-ccm.zendesk.com/hc/ko";
    public static final String WEB_URL_CS_CENTER_HOST = "macadamia-ccm.zendesk.com";
    public static final String WEB_URL_NONMBR_ORDER_LIST = URL_WEB.concat("/nonmbr/order-list");
    public static final String WEB_URL_NOTIFICATION = URL_WEB.concat("/my/notification");

    private static final String WEB_URL_LOGOFF_DEV = "https://d-sso6.daekyo.com/WebSSO/AuthWeb/logoffService.do?ssosite=d3.macadamia.kr&returnURL=https%3A%2F%2Fd3.macadamia.kr";
    private static final String WEB_URL_LOGOFF_REAL = "https://sso3.daekyo.com/WebSSO/AuthWeb/logoffService.do?ssosite=www.macadamia.kr&returnURL=https%3A%2F%2Fwww.macadamia.kr";
    public static final String WEB_URL_LOGOFF = BuildConfig.IS_DEV ? WEB_URL_LOGOFF_DEV : WEB_URL_LOGOFF_REAL;

    public static final String API_URI_APP_INFO = "/v1/app/info";
    public static final String API_URI_USER_INFO = "/v1/app/user";
    public static final String API_URI_UPDATE_TOKEN = "/v1/app/token/update";
    public static final String API_URI_UPDATE_ORANGEDOT = "/v1/app/orangedot/updat;e";
    public static final String API_URI_ORANGEDOT = "/v1/app/orangedot";
    public static final String API_URI_MENUS = "/v1/app/menus";

    public static final String URI_PATH_RELOAD = "/reload";
    public static final String URI_PATH_SHARE = "/share";
    public static final String URI_PATH_LOGIN = "/login";
    public static final String URI_PATH_LOGON = "/logon";
    public static final String URI_PATH_LOGOUT = "/logout";
    public static final String URI_PATH_SUBVIEW = "/subview";
    public static final String URI_PATH_CLOSE = "/close";
    public static final String URI_PATH_LOCATION = "/location";
    public static final String URI_PATH_SNS_LOGIN = "/snslogin";

    public static final String WEB_URL_NOONNOPPI_LEARNING_DEV = "https://d-ml.noonnoppi.com/default_maca.aspx";
    public static final String WEB_URL_NOONNOPPI_LEARNING_REAL = "https://ml.noonnoppi.com/default_maca.aspx";
    public static final String WEB_URL_NOONNOPPI_LEARNING = BuildConfig.IS_DEV ? WEB_URL_NOONNOPPI_LEARNING_DEV : WEB_URL_NOONNOPPI_LEARNING_REAL;

    public static final String WEB_URL_NOONNOPPI_LEARNING_HOST = Uri.parse(WEB_URL_NOONNOPPI_LEARNING).getHost();

    // VGUARD
    public static final String VGUARD_URL = "http://m.vguard.co.kr/card/vguard_webstandard.apk";
  }

  public static class Keys {
    public static final String PARAM_KEY_APP_ID = "appId";
    public static final String PARAM_KEY_DEVICE_ID = "deviceId";
    public static final String PARAM_KEY_SESSION_ID = "sessionId";
    public static final String PARAM_KEY_DEVICE_OS = "deviceOs";
    public static final String PARAM_KEY_OS_VERSION = "osVersion";
    public static final String PARAM_KEY_APP_VERSION = "appVersion";
    public static final String PARAM_KEY_USER_ID = "userId";
    public static final String PARAM_KEY_DEVICE_MODEL = "deviceModel";
    public static final String PARAM_KEY_CN = "cn";
    public static final String PARAM_KEY_STATUS = "status";
    public static final String PARAM_KEY_MESSAGE = "message";
    public static final String PARAM_KEY_TIMESTAMP = "timestamp";
    public static final String PARAM_KEY_DATA = "data";
    public static final String PARAM_KEY_POPUPS = "popups";
    public static final String PARAM_KEY_OUTLINK = "outlink";
    public static final String PARAM_KEY_SEQ = "seq";
    public static final String PARAM_KEY_TITLE = "title";
    public static final String PARAM_KEY_CONTENTS = "contents";
    public static final String PARAM_KEY_ORDER = "order";
    public static final String PARAM_KEY_MY_ORDER = "myorder";
    public static final String PARAM_KEY_CODE = "code";
    public static final String PARAM_KEY_VERSION = "version";
    public static final String PARAM_KEY_AOS = "aos";
    public static final String PARAM_KEY_IOS = "ios";
    public static final String PARAM_KEY_CURRENT = "current";
    public static final String PARAM_KEY_NEWER = "newer";
    public static final String PARAM_KEY_FORCED = "forced";
    public static final String PARAM_KEY_NAME = "name";
    public static final String PARAM_KEY_EMAIL = "email";
    public static final String PARAM_KEY_NOTIFICATION_COUNT = "notificationCount";
    public static final String PARAM_KEY_JOIN_TYPE = "joinType";
    public static final String PARAM_KEY_ROLE_TYPE = "roleType";
    public static final String PARAM_KEY_PUSH = "push";
    public static final String PARAM_KEY_PUSH_RECEIVE = "receive";
    public static final String PARAM_KEY_PUSH_TIMESTAMP = PARAM_KEY_TIMESTAMP;
    public static final String PARAM_KEY_TOKEN = "token";
    public static final String PARAM_KEY_ITEMS = "items";
    public static final String PARAM_KEY_PUSH_CODE = "code";
    public static final String PARAM_KEY_ORANGEDOT = "orangedot";
    public static final String PARAM_KEY_MENU_ADDED_COUNT = "menu_added_count";
    public static final String PARAM_KEY_MYSERVICE_ADDED_COUNT = "myservice_added_count";
    public static final String PARAM_KEY_MENU = "menu";
    public static final String PARAM_KEY_MYSERVICE = "myservice";
    public static final String PARAM_KEY_URL = "url";
    public static final String PARAM_KEY_ICON = "icon";
    public static final String PARAM_KEY_HEADER = "header";
    public static final String PARAM_KEY_TYPE = "type";
    public static final String PARAM_KEY_RETURN_URL = "returnUrl";
    public static final String PARAM_KEY_LOGOFF_URL = "returnOffUrl";
    public static final String PARAM_KEY_SID = "sid";
    public static final String PARAM_KEY_APP = "app";

    public static final String PARAM_KEY_RELOAD = "reload";
    public static final String PARAM_KEY_TARGET = "target";
    public static final String PARAM_KEY_TAG = "tag";
    public static final String PARAM_KEY_COOKIE = "cookie";
    public static final String PARAM_KEY_IS_LINE = "isLine";
    public static final String PARAM_KEY_ACTION = "action";
    public static final String PARAM_KEY_LINK = "link";
    public static final String PARAM_KEY_IS_MEMBER = "member";
    public static final String PARAM_KEY_IS_PARENTS = "parents";
    public static final String PARAM_KEY_ADDED = "added";
    public static final String PARAM_KEY_AUTO_LOGIN = "login_auto";

    public static final String PARAM_KEY_ERROR_TYPE = "errorType";
    public static final String PARAM_KEY_ERROR = "error";


    public static final String KEY_HEADER_USER_AGENT = "User-Agent";
    public static final String KEY_HEADER_ACCEPT = "Accept";
    public static final String KEY_HEADER_CONTENT_TYPE = "Content-Type";
    public static final String KEY_HEADER_APP_VERSION = "appVersion";
    public static final String KEY_DEVICE_ID = "deviceId";
    public static final String KEY_HEADER_OS = "os";
    public static final String KEY_HEADER_MACADAMIA_USER_AGENT = "Macadamia-User-Agent";

    public static final String KEY_HEADER_AUTHORIZATION = "Authorization";

    public static final String KEY_HEADER_NSSO_TOKEN = "NSSO-TOKEN";

    public static final String PREF_KEY_SESSION_ID = "MACA_SESSION_ID";
    public static final String PREF_KEY_FCM_TOKEN = "MACA_FCM_TOKEN";
    public static final String PREF_KEY_USER_CN = "MACA_USER_CN";
    public static final String PREF_KEY_USER_EMAIL = "MACA_USER_EMAIL";
    public static final String PREF_KEY_USER_TYPE = "MACA_USER_TYPE";
    public static final String PREF_KEY_USER_ID = "MACA_USER_ID";
    public static final String PREF_KEY_USER_INFO = "MACA_USER_INFO";
    public static final String PREF_KEY_MENUS = "MACA_MENU";
    public static final String PREF_KEY_PERMISSION_CHECK = "MACA_PERMISSION_CHECKED";
    public static final String PREF_RUN_COUNT = "MACA_RUN_COUNT";
    public static final String PREF_KEY_COOKIE = "MACA_COOKIE";
    public static final String PREF_KEY_MENU = "MACA_MENU";
    public static final String PREF_KEY_RUNTIME_PERMISSION = "MACA_RUNTIME_PERMISSION";
    public static final String PREF_KEY_PUSH_RECEIVE = "MACA_PUSH_RECEEIVE";
    public static final String PREF_KEY_AUTO_LOGIN = "MACA_AUTO_LOGIN";
    public static final String PREF_KEY_MAIN_REDIRECT_URL = "MACA_MAIN_REDIRECT_URL";
    public static final String PREF_KEY_USER_NAME = "MACA_USER_NAME";
    public static final String PREF_KEY_RETURN_URL = "MACA_RETURN_URL";
    public static final String PREF_KEY_LOGOFF_URL = "MACA_LOGOFF_URL";
    public static final String PREF_KEY_LOGOUT = "MACA_LOGOUT";

    public static final String PREF_FCM_TOKEN_REFRESHED = "MACA_TOKEN_REFRESHED";
    public static final String PREF_LAUNCH_PERMISSION_VIEW = "MACA_LAUNCH_PERMISSION_VIEW";
    public static final String PREF_LAUNCH_PUSH = "MACA_LAUNCH_PUSH";

    public static final String PREF_GUIDE_CHECKED = "MACA_GUIDE_CHECKED";


    public static final String LINK_URL = "linkUrl";

    // 권한 관련
    public static final String PREF_KEY_LAUNCH_PERMISSION_CAMERA = "MACA_LAUNCH_PER_CAMEARA";
    public static final String PREF_KEY_LAUNCH_PERMISSION_LOCATION = "MACA_LAUNCH_PER_LOCATION";
    public static final String PREF_LOCATION_PERMISSION_DIALOG = "MACA_PERMISSION_DIALOG";

    public static final String PREF_KEY_OUTLINKS = "MACA_OUTLINKS";
  }

  /**
   * URL Shortcuts
   */
  public static class Shortcuts {
    public static final String SHORTCUT_NOONNOPPI = "shortcut_noonnoppi";
    public static final String SHORTCUT_SUMMIT_MATH = "shortcut_summit_math";
    public static final String SHORTCUT_CAIHONG = "shortcut_caihong";
    public static final String SHORTCUT_SOLUNY = "shortcut_soluny";
  }

  /**
   * default GeoLocation
   */
  public static class GeoLocation {
    public static final float DK_LONGIGUDE = 126.925525f;
    public static final float DK_LATITUDE = 37.491052f;
  }

  public static class Request {
    public static final String REQ_CODE = "requestCode";
    public static final int REQ_SHOW_ORANGEDOT = 0x0010;
    public static final int REQ_SHOW_ORANGEDOT_SETTING = 0x0020;
    public static final int REQ_SHOW_SETTING = 0x0030;
    public static final int REQ_SHOW_SUB = 0x0040;
  }

  public static class Result {
    public static final int RES_SUCCESS = 0x0000;
    public static final int RES_RELOAD_ITEMS = 0x0200;
    public static final int RES_LOAD_URL = 0x0300;
    public static final int RES_RELOAD = 0x0310;
    public static final int RES_HIDE_ORANGEDOT = 0x0400;
    public static final int RES_HIDE_ORANGEDOT_SETTING = 0x0410;
    public static final int RES_LOGIN = 0x0500;
    public static final int RES_LOGOFF = 0x0510;
  }
}
