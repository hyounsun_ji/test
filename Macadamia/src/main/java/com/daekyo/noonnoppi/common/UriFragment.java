package com.daekyo.noonnoppi.common;

import androidx.annotation.NonNull;

import java.util.HashMap;

public class UriFragment {
    private final HashMap<String, String> map;

    private UriFragment instance;

    public static UriFragment parseFragment(@NonNull String fragment) {
        return new UriFragment(fragment);
    }

    private UriFragment(@NonNull String fragment) {
        map = new HashMap<>();
        String[] items = fragment.split(";");

        for (String item : items) {
            if (item.contains("=")) {
                String[] arr = item.split("=");
                map.put(arr[0], arr[1]);
            }
        }
    }

    public String getScheme() {
        return map.get("scheme");
    }

    public String getAction() {
        return map.get("action");
    }

    public String getCategory() {
        return map.get("category");
    }

    public String getPackage() {
        return map.get("package");
    }
}
