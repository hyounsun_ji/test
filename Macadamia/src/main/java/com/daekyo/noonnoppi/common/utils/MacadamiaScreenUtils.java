package com.daekyo.noonnoppi.common.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.orhanobut.logger.Logger;

public class MacadamiaScreenUtils {

    private static final String TAG = MacadamiaScreenUtils.class.getSimpleName();

    private static final String STATUS_BAR_HEIGHT = "status_bar_height";
    private static final String DIMEN = "dimen";
    private static final String ANDROID = "android";

    private static final String SHOW_NAVIGATION_BAR = "config_showNavigationBar";
    private static final String BOOL = "bool";

    private static final String NAVIGATION_BAR_HEIGHT = "navigation_bar_height";

    /**
     * NagivationBar 높이 조정
     *
     * @param context Application Context
     * @param layout  Layout
     */
    public static void resizeNavigationBarLayout(@NonNull Context context, @NonNull ViewGroup layout) {
        int navigationBarHeight;

        int resId = context.getResources().getIdentifier(NAVIGATION_BAR_HEIGHT, DIMEN, ANDROID);

        if (resId > 0) {
            navigationBarHeight = context.getResources().getDimensionPixelSize(resId);

            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) layout.getLayoutParams();
            layoutParams.height = layoutParams.height + navigationBarHeight;
            layout.setLayoutParams(layoutParams);
        }
    }

    /**
     * StatusBar 영역 확장
     *
     * @param context         Application Context
     * @param activity        Activity
     * @param stretched       확장 flag
     * @param statusBarLayout StatusBar Layout
     */
    public static void stretchScreenWithPadding(@NonNull Context context,
                                                @NonNull Activity activity,
                                                boolean stretched,
                                                @NonNull ConstraintLayout statusBarLayout) {
        if (stretched) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            ViewGroup.LayoutParams statusBarParams = statusBarLayout.getLayoutParams();
            statusBarParams.height = getStatusBarHeight(context);
            statusBarLayout.setLayoutParams(statusBarParams);
        }
    }

    public static void stretchScreenWithPadding(@NonNull Context context,
                                                @NonNull Activity activity,
                                                boolean stretched) {
        if (stretched) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    /**
     * Layout 크기 변경
     *
     * @param context    Application Context
     * @param menuLayout Layout
     * @param view       View
     */
    public static void resizeLayout(Context context, ConstraintLayout menuLayout, View view) {
        // status bar height
        int statusBarHeight = getStatusBarHeight(context);

        // Layout
        ViewGroup.LayoutParams webviewLayoutParams = view.getLayoutParams();
        Logger.t(TAG).e("height : %d", webviewLayoutParams.height);
        view.setLayoutParams(webviewLayoutParams);

        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) menuLayout.getLayoutParams();
        layoutParams.height = layoutParams.height + statusBarHeight;
        menuLayout.setLayoutParams(layoutParams);
    }

    /**
     * Device Metrics 출력
     *
     * @param activity Activity
     */
    public static void printDeviceMetrics(@NonNull Activity activity) {
        DisplayMetrics outMetrics = new DisplayMetrics();

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        Logger.t(TAG).d("WIDTH x HEIGHT : %d x %d", size.x, size.y);

        activity.getWindowManager().getDefaultDisplay().getMetrics(outMetrics);
        int dpi = outMetrics.densityDpi;
        float density = outMetrics.density;

        Logger.t(TAG).d("DPI : %d", dpi);
        Logger.t(TAG).d("DENSITY : %f", density);
    }

    /**
     * 스크린 Width
     *
     * @param context Application Context
     * @return Display Width(px) int
     */
    public static int getScreenWidth(@NonNull Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    /**
     * 스크랜 Height
     *
     * @param context Application Context
     * @return Display Height(px) int
     */
    public static int getScreenHeight(@NonNull Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    /**
     * NavigationBar 유무
     *
     * @param context Application Context
     * @return NavigationBar 영역 유무 boolean
     */
    public static boolean hasNavigationBar(@NonNull Context context) {
        boolean useSoftNavigation;

        int resourceId = context.getResources().getIdentifier(SHOW_NAVIGATION_BAR, BOOL, ANDROID);

        if (resourceId > 0) {
            useSoftNavigation = context.getResources().getBoolean(resourceId);
        } else {
            boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
            boolean hasHomeKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_HOME);
            useSoftNavigation = (!(hasBackKey && hasHomeKey));
        }

        return useSoftNavigation;
    }

    /**
     * StatusBar 높이
     *
     * @param context Application Context
     * @return StatusBar Height int
     */
    public static int getStatusBarHeight(@NonNull Context context) {

        int statusBarHeight = 0;
        int resourceId = context.getResources().getIdentifier(STATUS_BAR_HEIGHT, DIMEN, ANDROID);

        if (resourceId > 0) {
            statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);
        }

        return statusBarHeight;
    }

    /**
     * NavigationBar 높이
     *
     * @param context Application Context
     * @return NavigationBar Height int
     */
    public static int getNavigationBarHeight(@NonNull Context context) {

        int navigationBarHeight = 0;

        if (hasNavigationBar(context)) {
            int resourceId = context.getResources().getIdentifier(NAVIGATION_BAR_HEIGHT, DIMEN, ANDROID);

            if (resourceId > 0) {
                navigationBarHeight = context.getResources().getDimensionPixelSize(resourceId);
            }
        }

        return navigationBarHeight;
    }

    /**
     * dp를 px로 변환 (dp를 입력받아 px을 리턴)
     *
     * @param context Application Context
     * @param dp      DP
     * @return dp to px
     */
    public static float convertDpToPixel(@NonNull Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    /**
     * px을 dp로 변환 (px을 입력받아 dp를 리턴)
     *
     * @param context Applicatiosn Context
     * @param px      pixel
     * @return px to dp
     */
    public static float convertPixelsToDp(@NonNull Context context, float px) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    /**
     * set StatusBar Color
     *
     * @param activity Activity
     * @param color    Color ResId
     */
    public static void setStatusBarColor(@NonNull Activity activity, int color) {
        Window window = activity.getWindow();

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(color);

        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }

    /**
     * StatusBar Icon Color 변경
     *
     * @param activity Activity
     * @param isDark   isDar flag
     */
    public static void changeStatusBarTextColor(@NonNull Activity activity, boolean isDark) {
        View decorView = activity.getWindow().getDecorView();

        if (isDark) {
            activity.getWindow().getDecorView().setSystemUiVisibility(decorView.getSystemUiVisibility() | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    /**
     * NavigationBar Icon Color 변경
     *
     * @param activity Activity
     * @param isDark   isDark flag
     */
    public static void changeNavigationBarIconColor(@NonNull Context context, @NonNull Activity activity, boolean isDark) {
        View decorView = activity.getWindow().getDecorView();

        // Navigation Bar color
        if (isDark) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                activity.getWindow().getDecorView().setSystemUiVisibility(decorView.getSystemUiVisibility() | View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() & ~View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            }
        }
    }

    /**
     * NavigationBar Background Color 변경
     *
     * @param activity Activity
     * @param color    Color Resources Id
     */
    public static void changeNavigationBarBackgroundColor(@NonNull Activity activity, int color) {
        activity.getWindow().setNavigationBarColor(color);
    }

    /**
     * StatusBar Background Color 변경
     *
     * @param activity Activity
     * @param color    Color Resource Id
     */
    public static void changeStatusBarBackgroundColor(@NonNull Activity activity, int color) {
        activity.getWindow().setStatusBarColor(color);
    }

    /**
     * get RealScreen Size (w x h)
     *
     * @param context Application Context
     * @return Screen Size Point
     */
    public static Point getScreenSize(@NonNull Context context) {
        Point screenSize = new Point();
        try {
            WindowManager wm = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE));
            Display display = wm.getDefaultDisplay();
            display.getRealSize(screenSize);
        } catch (Exception e) {
            Logger.t(TAG).e("getScreenSize Exception : %s", e.getMessage());
        }
        return screenSize;
    }
}
