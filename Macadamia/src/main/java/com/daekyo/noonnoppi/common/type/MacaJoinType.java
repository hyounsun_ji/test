package com.daekyo.noonnoppi.common.type;

public enum MacaJoinType {
    // 대교
    Daekyo("DAEKYO")
    // 네이버
    , Naver("NAVER")
    // 카카오
    , Kakao("KAKAO")
    // facebook
    , Facebook("FACEBOOK")
    // 라인
    , Line("LINE")
    // Google
    , Google("GOOGOE")
    // Apple
    , Apple("APPLE");

    private final String value;

    MacaJoinType(String v) {
        this.value = v;
    }

    public String value() {
        return value;
    }

    public static MacaJoinType fromValue(String v) {

        for (MacaJoinType c : MacaJoinType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
