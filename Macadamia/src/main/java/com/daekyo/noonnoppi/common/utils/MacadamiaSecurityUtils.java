package com.daekyo.noonnoppi.common.utils;

import com.orhanobut.logger.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class MacadamiaSecurityUtils {
    private static final String TAG = MacadamiaSecurityUtils.class.getSimpleName();

    /**
     * ROOTED 파일 체크
     *
     * @return rooted 파일 체크 여부 boolean
     */
    private static boolean checkRootedFiles() {
        final String[] files = {
                "/sbin/su",
                "/system/su",
                "/system/bin/su",
                "/system/sbin/su",
                "/system/xbin/su",
                "/system/xbin/mu",
                "/system/bin/.ext/.su",
                "/system/usr/su-backup",
                "/data/data/com.noshufou.android.su",
                "/system/app/Superuser.apk",
                "/system/app/su.apk",
                "/system/bin/.ext",
                "/system/xbin/.ext",
                "/data/local/xbin/su",
                "/data/local/bin/su",
                "/system/sd/xbin/su",
                "/system/bin/failsafe/su",
                "/data/local/su",
                "/su/bin/su"};

        for (String filename : files) {
            File file = new File(filename);
            if (file.exists()) {
                Logger.t(TAG).d("Rooted File : %s : %s", file.getAbsolutePath(), file.getName());
                return true;
            }
        }
        return false;
    }

    /**
     * Build.TAGS 체크(루팅이 된 기기는 일반적으로 Build.TAGS 값이 제조사 키값이 아닌 test 키 값을 가지고 있음)
     *
     * @return Tag 체크 결과 boolean
     */
    private static boolean checkTags() {
        String buildTags = android.os.Build.TAGS;
        Logger.t(TAG).d("Build TAG : %s", buildTags.contains("test-keys"));
        return buildTags.contains("test-keys");
    }

    /**
     * SU 체크
     *
     * @return SU 체크 결과 boolean
     */
    private static boolean checkSuperUserCommand() {
        try {
            Runtime.getRuntime().exec("su");
            Logger.t(TAG).d("SUPER USER!!");
            return true;
        } catch (Error error) {
            Logger.t(TAG).e("checkSuperUserCommand Error : %s", error.getMessage());
        } catch (Exception e) {
            Logger.t(TAG).e("checkSuperUserCommand Exception : %s", e.getMessage());
        }

        return false;
    }

    /**
     * SU 체크
     *
     * @return SU 체크 결과 boolean
     */
    private static boolean checkSuperUserCommand2() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su"});
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));

            return in.readLine() != null;
        } catch (Throwable t) {
            Logger.t(TAG).e("checkSuperUserCommand2 Throwable : %s", t.getMessage());
            return false;
        } finally {
            if (process != null) {
                process.destroy();
            }
        }
    }

    /**
     * 루팅 체크
     *
     * @return 루팅 체크 여부 boolean
     */
    public static boolean isRooted() {
        boolean isRooted = (checkRootedFiles() || checkSuperUserCommand() || checkSuperUserCommand2() || checkTags());
        Logger.t(TAG).e("ROOTING CHECK : %s", isRooted ? "YES" : "NO");
        return isRooted;
    }
}
