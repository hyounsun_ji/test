package com.daekyo.noonnoppi.common.api;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface MacadamiaApiService {

    /**
     * App 기본 정보 조회
     *
     * @param param 파라미터
     * @return result
     */
    @POST("/v1/app/info")
    Call<JSONObject> getAppInfo(@Body JSONObject param);

    /**
     * 사용자 정보 조회
     *
     * @param header additional Header
     * @param param  파라미터
     * @return result
     */
    @POST("/v1/app/user")
    Call<JSONObject> getUserInfo(@HeaderMap HashMap<String, String> header,
                                 @Body JSONObject param);

    /**
     * 오렌지닷 조회
     *
     * @param header additional Header
     * @param param  파라미터
     * @return result
     */
    @POST("/v1/app/orangedot")
    Call<JSONObject> getOrangeDot(@HeaderMap HashMap<String, String> header,
                                  @Body JSONObject param);

    /**
     * 오렌지닷 업데이트
     *
     * @param header additional Header
     * @param param  파라미터
     * @return result
     */
    @POST("/v1/app/orangedot/update")
    Call<JSONObject> updateOrangeDot(@HeaderMap HashMap<String, String> header,
                                     @Body JSONObject param);

    /**
     * 메뉴 조회
     *
     * @param param 파라미터
     * @return result
     */
    @POST("/v1/app/menus")
    Call<JSONObject> getMenus(@Body JSONObject param);

    /**
     * PUSH Token 업데이트
     *
     * @param header additional Header
     * @param param  파라미터
     * @return result
     */
    @POST("/v1/app/token/update")
    Call<JSONObject> updatePushToken(@HeaderMap HashMap<String, String> header,
                                     @Body JSONObject param);

    /**
     * Error 리포트
     *
     * @param header additional Header
     * @param param  파라미터
     * @return result
     */
    @POST("/v1/app/report")
    Call<JSONObject> requestErrorReport(@HeaderMap HashMap<String, String> header,
                                        @Body JSONObject param);
}
