package com.daekyo.noonnoppi.common.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.Macadamia;
import com.daekyo.noonnoppi.common.utils.MacadamiaUtils;
import com.daekyo.noonnoppi.views.launch.LaunchActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class MCFirebaseMessagingService extends FirebaseMessagingService {

    // region 00.Variables
    private static final String TAG = MCFirebaseMessagingService.class.getSimpleName();

    private static final int NOTIFICATION_REQUEST_CODE = 0x0000;

    private static final String PUSH_TITLE = "title";
    private static final String PUSH_BODY = "body";
    private static final String PUSH_IMAGE_URL = "urlImage";
    private static final String PUSH_LINK_URL = "url";
    private static final String PUSH_REG_ID = "regid";
    private static final String PUSH_POPUP_YN = "popupYn";
    private static final String PUSH_APP_ID = "appId";

    // PUSH START TIME
    private static final int PUSH_START_TIME = 8;
    // PUSH END TIME
    private static final int PUSH_END_TIME = 21;
    // endregion

    /**
     * Constructor
     */
    public MCFirebaseMessagingService() {
        super();
        Logger.t(TAG).d("- MCFirebaseMessagingService");
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();

        Logger.t(TAG).d("From: " + remoteMessage.getFrom());
        Logger.t(TAG).d("MessageId : " + remoteMessage.getMessageId());
        Logger.t(TAG).d("MessageType : " + remoteMessage.getMessageType());
        Logger.t(TAG).d("CollapseKey : " + remoteMessage.getCollapseKey());
        Logger.t(TAG).d("Ttl : " + remoteMessage.getTtl());
        Logger.t(TAG).d("data : " + remoteMessage.getData());

        if (data != null) {
            String appId = StringUtils.defaultIfEmpty(data.get(PUSH_APP_ID), "macadamia");
            String title = StringUtils.defaultIfEmpty(data.get(PUSH_TITLE), "마카다미아"); // Notification 발생시 아래로 내렸을 때 첫번째 줄
            String body = StringUtils.defaultIfEmpty(data.get(PUSH_BODY), "마카다미아에서 알림이 도착했습니다."); // Notification 발생시 아래로 내렸을 때 두번째 줄
            String linkURL = StringUtils.defaultIfEmpty(data.get(PUSH_LINK_URL), Macadamia.Urls.WEB_URL_MAIN);
            String imgURL = data.get(PUSH_IMAGE_URL);
            String regID = StringUtils.defaultIfEmpty(data.get(PUSH_REG_ID), MacadamiaUtils.getUUID());

            if (StringUtils.isEmpty(imgURL)) {
                notificationWithBigText(MCFirebaseMessagingService.this, regID, title, body, linkURL);
            } else {
                notificationWithBigPicture(MCFirebaseMessagingService.this, regID, title, body, imgURL, linkURL);
            }
        }

        //MacadamiaWakeLock.aquireWakeLock(this);
//        TimerTask task = new TimerTask() {
//            @Override
//            public void run() {
//                MacadamiaWakeLock.releaseWakeLock();
//            }
//        };
//
//        Timer timer = new Timer();
//        timer.schedule(task, getResources().getInteger(R.integer.delay_500));
    }

    /**
     * Notification UI 중 BigTextStyle 입니다.
     * 노출된 notification에서 두 손가락을 이용해 아래로 드래그 하면 expand 되면서 긴 텍스트가 노출됩니다.
     * Android 4.1에서 부터 추가된 기능으로 하위 버젼에서는 expand 되지 않습니다, 하위 버젼에서는 일반적인 Notification UI가 노출 됩니다.
     *
     * @param context Application Context
     * @param regid   Push Token
     * @param title   타이틀
     * @param message 메시지
     * @param link    URL 링크
     */
    public static void notificationWithBigText(Context context, String regid, String title, String message, String link) {
        Logger.t(TAG).d("- notification");

        Notification.Builder builder = makeNotificationEx(context, regid, title, message, link);
        Notification.BigTextStyle style = new Notification.BigTextStyle();
        style.setBigContentTitle(title);
        style.bigText(message);
        builder.setStyle(style);

        // Android O 이상 channel ID 추가
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(Macadamia.Constants.MACA_NOTIFICATION_CHANNEL_ID_EVENT);
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(100, builder.build());
    }

    /**
     * Notification UI 중 BigPictureStyle 입니다.
     * 노출된 notification에서 두 손가락을 이용해 아래로 드래그 하면 expand 되면서 banner 이미지가 노출됩니다.
     * Android 4.1에서 부터 추가된 기능으로 하위 버젼에서는 expand 되지 않습니다, 하위 버젼에서는 일반적인 Notification UI가 노출 됩니다.
     *
     * @param context Application Context
     * @param regid   Push Token
     * @param title   타이틀
     * @param message 메시지
     * @param imglink 이미지 링크
     * @param link    URL 링크
     */
    public static void notificationWithBigPicture(Context context, String regid, String title, String message, String imglink, String link) {
        Logger.t(TAG).d("notification");
        Bitmap banner = null;

        if (imglink != null && imglink.startsWith("http")) {
            try {
                FutureTarget<Bitmap> target = Glide.with(context)
                        .asBitmap()
                        .load(imglink)
                        .submit();
                banner = target.get();
            } catch (Exception e) {
                Logger.t(TAG).e("ImageLoad Exception : %s", e.getMessage());
            }
        }

        if (banner == null) {
            notificationWithBigText(context, regid, title, message, link);
        } else {
            Notification.Builder builder = makeNotificationEx(context, regid, title, message, link);
            Notification.BigPictureStyle style = new Notification.BigPictureStyle();
            style.setBigContentTitle(title);
            style.setSummaryText(message);
            style.bigPicture(banner);
            builder.setStyle(style);

            // Android O 이상 channel ID 추가
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.setChannelId(Macadamia.Constants.MACA_NOTIFICATION_CHANNEL_ID_EVENT);
            }

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(100, builder.build());
        }
    }

    /**
     * Push Message 표시
     *
     * @param context Application Context
     * @param regid   Push Token
     * @param title   타이틀
     * @param message 메시지
     * @param link    URL 링크
     * @return Push Notification
     */
    public static Notification.Builder makeNotificationEx(Context context, String regid, String title, String message, String link) {
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_notification_02_large);

        Notification.Builder builder = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_notification_02)
                .setLargeIcon(largeIcon)
                .setTicker(title)
                .setContentTitle(title)
                .setContentText(message)
                .setColor(Color.parseColor("#FF24AAE1"))
                .setAutoCancel(true);

        // Android O 이상 channel ID 추가
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(Macadamia.Constants.MACA_NOTIFICATION_CHANNEL_ID_EVENT);
        }

        Intent intent = new Intent(context, LaunchActivity.class);
        intent.setAction(Macadamia.Intent.INTENT_ACTION_PUSH_LINK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        intent.putExtra(Macadamia.Intent.KEY_LINK_URL, link);
        //intent.putExtra(MainActivity.OTINTENT_MAINACTIVITY_PUSH_URL, link);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                NOTIFICATION_REQUEST_CODE,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        builder.setDefaults(Notification.DEFAULT_VIBRATE);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        return builder;
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(@NonNull String s) {
        super.onMessageSent(s);
    }

    @Override
    public void onSendError(@NonNull String s, @NonNull Exception e) {
        super.onSendError(s, e);
    }

    @Override
    public void onNewToken(@NonNull String token) {
        Logger.t(TAG).d("- onNewToken : %s", token);
        Macadamia.Functions.setPushToken(token);
    }

    // TODO
    private void sendRegistrationIdToServer(String registratoinId) {
        Logger.t(TAG).d("[REGISTRATION_ID] - %s", registratoinId);
        Macadamia.Functions.setPushToken(registratoinId);
    }
}
