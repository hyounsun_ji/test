package com.daekyo.noonnoppi.common.type;

/**
 * MacaAppStatus
 */
public enum MacaAppStatus {
    BACKGROUND,
    RETURNED_TO_FOREGROUND,
    FOREGROUND
}