package com.daekyo.noonnoppi.common;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.daekyo.noonnoppi.BuildConfig;
import com.daekyo.noonnoppi.R;
import com.daekyo.noonnoppi.common.api.MacadamiaApiClient;
import com.daekyo.noonnoppi.common.api.MacadamiaApiService;
import com.daekyo.noonnoppi.common.type.MacaAppStatus;
import com.daekyo.noonnoppi.views.MacadamiaActivity;
import com.daekyo.noonnoppi.views.main.MainActivity;
import com.facebook.stetho.Stetho;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseAppLifecycleListener;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;

import okhttp3.logging.HttpLoggingInterceptor;

public class MacadamiaApplication extends Application {

  // region 00.Variables
  private static final String TAG = MacadamiaApplication.class.getSimpleName();

  // Firebase App
  private FirebaseApp firebaseApp = null;
  // Firebase Analytics
  private static FirebaseAnalytics firebaseAnalytics = null;

  private static Context appContext;
  private MacadamiaActivity activity;

  // Logger
  private static final int LOGGER_METHOD_PRINT_STACK_COUNT = 0;
  private static final int LOGGER_METHOD_OFFSET = 7;
  private static final String LOGGER_TAG = "MACA_LOGGER";
  // Status
  private MacaAppStatus appStatus = MacaAppStatus.FOREGROUND;

  public static Context getAppContext() {
    return macadamiaApplication;
  }

  // endregion

  private static MacadamiaApplication macadamiaApplication;
  private static AppCompatDialog progressDialog;

  public MacadamiaApplication() {
    super();
    macadamiaApplication = this;
  }

  private static class LazyHolder {
    private static final MacadamiaApplication INSTANCE = new MacadamiaApplication();
  }

  public static MacadamiaApplication getInstance() {
    return LazyHolder.INSTANCE;
  }

  // region 10. Application LifeCycle
  @Override
  public void onCreate() {
    super.onCreate();
    Logger.t(TAG).d("- onCreate()");
    // init Application
    this.initApplication();
  }

  @Override
  public void onTerminate() {
    super.onTerminate();
    Logger.t(TAG).d("- onTerminate()");
  }

  @Override
  public void onLowMemory() {
    super.onLowMemory();
    Logger.t(TAG).d("- onLowMemory()");
  }

  @Override
  public void onConfigurationChanged(@NonNull Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
  }
  // endregion

  // region Private Methods

  /**
   * Initialize Application
   */
  private void initApplication() {
    // set ApplicationContext
    appContext = getApplicationContext();

    // register activity lifecycle
    registerActivityLifecycleCallbacks(new MacaActivityLifeCycleCallbacks());

    // init Logger
    this.initLogger();
    // init Stetho
    this.initStetho();
    // init Firebase
    this.initFirebase();

    // init API Client
    MacadamiaApiClient.Builder builder = new MacadamiaApiClient.Builder(getApplicationContext());
    builder.setApiBaseUrl(Macadamia.Urls.URL_API)
      .setUserAgent(Macadamia.Constants.USER_AGENT_FOR_API)
      .setContentType(Macadamia.Constants.HEADER_CONTENT_TYPE_JSON)
      .setApiService(MacadamiaApiService.class)
      .setLogLevel(HttpLoggingInterceptor.Level.BODY)
      .getApiClient();

    // 실행 카운트 증가
    Macadamia.Functions.incRunCount();
    ProcessLifecycleOwner.get()
      .getLifecycle()
      .addObserver(new LifeCycleObserver());
    // webview static method
    WebView.setWebContentsDebuggingEnabled(BuildConfig.IS_DEV);
  }

  /**
   * Initialize Logger
   */
  private void initLogger() {
    // Logger Stack trace을 PRINT_STACK_COUNT 개로 조절
    PrettyFormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
      .showThreadInfo(false)
      .methodOffset(LOGGER_METHOD_OFFSET)
      .methodCount(LOGGER_METHOD_PRINT_STACK_COUNT)
      .tag(LOGGER_TAG)
      .build();

    Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy) {
      @Override
      public boolean isLoggable(int priority, String tag) {
        return BuildConfig.LOGGABLE;
      }
    });
  }

  /**
   * Initialize Stetho
   *
   * @link https://facebook.github.io/stetho/
   */
  private void initStetho() {
    if (BuildConfig.IS_DEV) {
      Stetho.initializeWithDefaults(this);
    }
  }

  /**
   * Initialize Firebase
   *
   * @link https://firebase.google.com
   */
  private void initFirebase() {
    // firebase app
    FirebaseOptions firebaseOptions = FirebaseOptions.fromResource(getApplicationContext());

    if (firebaseOptions != null) {
      firebaseApp = FirebaseApp.initializeApp(getApplicationContext(),
        firebaseOptions,
        getResources().getString(R.string.app_name));

      firebaseApp.addLifecycleEventListener(firebaseAppLifecycleListener);

      firebaseApp.addBackgroundStateChangeListener(b ->
        Logger.t(TAG).i("FIREBASE onBackgroundStateChanged : %s", b)
      );

      // firebase analytics
      getFirebaseAnalytics(this);
    }
  }

  /**
   * get Firebase Analytics Instance
   *
   * @param context Application Context
   * @return FirebaseAnalytics instance
   */
  public static synchronized FirebaseAnalytics getFirebaseAnalytics(@NonNull Context context) {
    if (firebaseAnalytics == null) {
      firebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    return firebaseAnalytics;
  }

  /**
   * get AppStatus
   *
   * @return MacaAppStatus
   */
  public MacaAppStatus getAppStatus() {
    return appStatus;
  }

  // endregion

  // region 30. Firebase Event Listener
  final FirebaseAppLifecycleListener firebaseAppLifecycleListener = (s, firebaseOptions) -> Logger.t(TAG).e("FIREBASE onDeleted %s : %s", s, firebaseOptions.toString());
  // endregion

  // region 40. Activity Lifecycle Callback

  /**
   * Activity LifeCycle Callback
   */
  public class MacaActivityLifeCycleCallbacks implements ActivityLifecycleCallbacks {
    private int running = 0;

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
      Logger.t(TAG).e("[onActivityCreated] %s %s", activity.getClass().getSimpleName(), savedInstanceState);
      String activityName = activity.getClass().getSimpleName();
      // Logging Firebase Analytics Event
      Bundle data = new Bundle();
      data.putString(FirebaseAnalytics.Param.ITEM_ID, activityName);
      data.putString(FirebaseAnalytics.Param.ITEM_NAME, activityName);
      data.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "VIEW");
      getFirebaseAnalytics(getApplicationContext()).logEvent(FirebaseAnalytics.Event.VIEW_ITEM, data);
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
      Logger.t(TAG).e("[onActivityStarted] %s", activity.getClass().getSimpleName());

      if (++running == 1) {
        appStatus = MacaAppStatus.RETURNED_TO_FOREGROUND;
      } else if (running > 1) {
        appStatus = MacaAppStatus.FOREGROUND;
      }
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
      Logger.t(TAG).e("[onActivityResumed] %s", activity.getClass().getSimpleName());
      String classString = activity.getClass().getSimpleName();

      if (classString.equals("MainActivity")) {
        MainActivity mainActivity = (MainActivity) activity;
      }
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
      Logger.t(TAG).e("[onActivityPaused] %s", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {
      Logger.t(TAG).e("[onActivityStopped] %s", activity.getClass().getSimpleName());

      if (--running == 0) {
        appStatus = MacaAppStatus.BACKGROUND;
      }
    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {
      Logger.t(TAG).e("[onActivitySaveInstanceState] %s", activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
      Logger.t(TAG).e("[onActivityDestroyed] %s", activity.getClass().getSimpleName());
    }
  }
  // endregion

  /**
   * start loading
   *
   * @param activity Activity
   * @param message  message string
   */
  public void startLoading(@NonNull Activity activity, @Nullable String message) {
    if (activity.isFinishing()) {
      return;
    }

    if (progressDialog == null || !progressDialog.isShowing()) {
      progressDialog = new AppCompatDialog(activity);
      progressDialog.setContentView(R.layout.layout_progressbar);
      progressDialog.setCancelable(false);
      progressDialog.setCanceledOnTouchOutside(false);
      progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
      progressDialog.show();
    }
  }

  /**
   * stop Loading
   */
  public void stopLoading() {
    if (progressDialog != null && progressDialog.isShowing()) {
      progressDialog.dismiss();
      progressDialog = null;
    }
  }

  public static class LifeCycleObserver implements DefaultLifecycleObserver {
    private Context context;

    public LifeCycleObserver() {
    }

    @Override
    public void onCreate(@NonNull LifecycleOwner owner) {
      Logger.t(TAG).d("LifeCycleObserver - onCreate");
      Macadamia.Constants.APP_BACKGROUND_TIMESTAMP = 0;
      Macadamia.Constants.APP_FOREGROUND_TIMESTAMP = 0;
      Macadamia.Constants.APP_BACKGROUND_STAYING_TIME = 0;
    }

    @Override
    public void onStart(@NonNull LifecycleOwner owner) {
      Logger.t(TAG).d("LifeCycleObserver - onStart");
      Macadamia.Constants.APP_FOREGROUND_TIMESTAMP = System.currentTimeMillis();

      if (Macadamia.Constants.APP_FOREGROUND_TIMESTAMP > 0 && Macadamia.Constants.APP_BACKGROUND_TIMESTAMP > 0) {
        Macadamia.Constants.APP_BACKGROUND_STAYING_TIME = (int) ((Macadamia.Constants.APP_FOREGROUND_TIMESTAMP - Macadamia.Constants.APP_BACKGROUND_TIMESTAMP) / 1000);
      }
    }

    @Override
    public void onResume(@NonNull LifecycleOwner owner) {
      Logger.t(TAG).d("LifeCycleObserver - onResume");
    }

    @Override
    public void onPause(@NonNull LifecycleOwner owner) {
      Logger.t(TAG).d("LifeCycleObserver - onPause");
      Macadamia.Constants.APP_BACKGROUND_TIMESTAMP = System.currentTimeMillis();
    }

    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
      Logger.t(TAG).d("LifeCycleObserver - onStop");
    }

    @Override
    public void onDestroy(@NonNull LifecycleOwner owner) {
      Logger.t(TAG).d("LifeCycleObserver - onDestroy");

    }
  }
}
