package com.daekyo.noonnoppi.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

public class MacadamiaCookieUtils {

    private static final String TAG = MacadamiaCookieUtils.class.getSimpleName();

    /**
     * 쿠키 Map 리턴
     *
     * @param url Cookie Domain
     * @return Cookie Map
     */
    public static HashMap<String, String> getCookieMap(String url) {
        android.webkit.CookieManager cookieManager = android.webkit.CookieManager.getInstance();
        String cookieString = cookieManager.getCookie(url);

        HashMap<String, String> cookieMap = new HashMap<>();

        if (StringUtils.isNotEmpty(cookieString)) {
            String[] cookies = cookieString.split(";");

            for (String cookie : cookies) {
                String[] items = cookie.trim().split("=");

                if (items.length == 2) {
                    cookieMap.put(items[0].trim(), items[1].trim());
                }
            }
        }

        return cookieMap;
    }

    /**
     * 로그인 인증 쿠키 리턴
     *
     * @param url          Cookie Domain
     * @param authTokenKey Login Token Name
     * @return Login Token String
     */
    public static String getLoginAuthTokenFromCookie(String url, String authTokenKey) {
        HashMap<String, String> map = getCookieMap(url);

        String cookieValue = null;

        if (map.containsKey(authTokenKey)) {
            cookieValue = map.get(authTokenKey);
        }

        return cookieValue;
    }

    /**
     * 쿠키 맵 스트링 변환
     *
     * @param cookieMap Cookie Map
     * @return Cookie String
     */
    private static String cookieMapToString(HashMap<String, String> cookieMap) {
        StringBuilder sb = new StringBuilder();

        for (String key : cookieMap.keySet()) {
            String value = cookieMap.get(key);
            sb.append(key).append("=").append(value).append("; ");
        }

        return sb.toString();
    }

    /**
     * 쿠키 삭제
     *
     * @param url        URL
     * @param cookieName Cookie Name
     */
    public static void removeCookieByName(String url, String cookieName) {
        android.webkit.CookieManager cookieManager = android.webkit.CookieManager.getInstance();
        HashMap<String, String> cookieMap = getCookieMap(url);

        if (cookieMap.containsKey(cookieName)) {
            // 해당 쿠키 삭제
            cookieMap.remove(cookieName);
            String cookieString = cookieMapToString(cookieMap);
            // 전체 쿠키 삭제
            cookieManager.removeAllCookies(null);
            // 나머지 쿠키 저장
            cookieManager.setCookie(url, cookieString);
        }
    }

}


