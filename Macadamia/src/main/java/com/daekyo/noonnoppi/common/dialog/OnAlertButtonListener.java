package com.daekyo.noonnoppi.common.dialog;

public interface OnAlertButtonListener {
    void onButtonClick();
}
