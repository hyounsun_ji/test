package com.daekyo.noonnoppi.common.utils;

import androidx.annotation.NonNull;

import com.daekyo.noonnoppi.common.crypto.MacaAES;

import org.joda.time.DateTime;

public class MacadamiaCryptoUtils {

    private static final String salt1 = "yyyy";
    private static final String salt2 = "MMdd";

    public static String decrypt(@NonNull String k, @NonNull String encData) {
        String descData = null;
        try {
            descData = MacaAES.decrypt(encData, genKey(k));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return descData;
    }

    private static String genKey(@NonNull String key) {
        return getDatetime(salt1).concat(key).concat(getDatetime(salt2));
    }

    private static String getDatetime(@NonNull String format) {
        DateTime dateTime = new DateTime();
        return dateTime.toString(format);
    }
}
