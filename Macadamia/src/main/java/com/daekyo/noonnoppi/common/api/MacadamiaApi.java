package com.daekyo.noonnoppi.common.api;

import android.content.Context;

import androidx.annotation.NonNull;

import com.alibaba.fastjson.JSONObject;
import com.daekyo.noonnoppi.common.Macadamia;
import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.daekyo.noonnoppi.views.MacadamiaActivity.TAG_API;

public class MacadamiaApi {
    // Application Context
    private final Context ctx;
    private final MacadamiaApiClient apiClient;

    /**
     * Constructor
     *
     * @param context Application Context
     */
    public MacadamiaApi(Context context) {
        this.ctx = context;
        apiClient = MacadamiaApiClient.getInstance();
    }

    /**
     * request API
     *
     * @param type      Request Type
     * @param param     파라미터
     * @param callback  Callback
     * @param headerMap 추가 Header
     * @throws Exception exception
     */
    public void request(MacadamiaRequestType type,
                        @Nullable HashMap<String, String> headerMap,
                        @NonNull JSONObject param,
                        @Nullable Callback<JSONObject> callback) throws Exception {
        Logger.t(TAG_API).d("TYPE : %s", type);
        Logger.t(TAG_API).d("HEADER : %s", headerMap);
        Logger.t(TAG_API).d("PARAM : %s", param);

        switch (type) {
            case REQUEST_APP_INFO:
                requestAppInfo(param, callback);
                break;

            case REQUEST_ORANGE_DOT:
                requestOrangeDot(headerMap, param, callback);
                break;

            case REQUEST_USER_INFO:
                requestUserInfo(headerMap, param, callback);
                break;

            case REQUEST_MENU:
                requestMenus(param, callback);
                break;

            case UPDATE_ORANGE_DOT:
                updateOrangeDot(headerMap, param, callback);
                break;

            case UPDATE_PUSH_TOKEN:
                updatePushToken(headerMap, param, callback);
                break;

            case REQUEST_ERROR_REPORT:
                requestErrorReport(headerMap, param);

            default:
                break;
        }
    }

    /**
     * request API
     *
     * @param type     Request Type
     * @param param    파라미터
     * @param callback Callback
     * @throws Exception exception
     */
    public void request(MacadamiaRequestType type,
                        @NonNull JSONObject param,
                        @Nullable Callback<JSONObject> callback) throws Exception {
        request(type, null, param, callback);
    }

    /**
     * App 기본 정보 조회
     *
     * @param param    파라미터
     * @param callback Callback
     */
    private void requestAppInfo(JSONObject param, Callback<JSONObject> callback) {
        Call<JSONObject> call = apiClient.getApiService().getAppInfo(param);
        call.enqueue(callback);
    }

    /**
     * 사용자 정보 조회
     *
     * @param headerMap header
     * @param param     파라미터
     * @param callback  Callback
     */
    private void requestUserInfo(HashMap<String, String> headerMap,
                                 JSONObject param,
                                 Callback<JSONObject> callback) {
        Call<JSONObject> call = apiClient.getApiService().getUserInfo(headerMap, param);
        call.enqueue(callback);
    }

    /**
     * 오렌지닷 조회
     *
     * @param headerMap header
     * @param param     파라미터
     * @param callback  Callback
     */
    private void requestOrangeDot(HashMap<String, String> headerMap,
                                  JSONObject param,
                                  Callback<JSONObject> callback) {
        Call<JSONObject> call = apiClient.getApiService().getOrangeDot(headerMap, param);
        call.enqueue(callback);
    }

    /**
     * 오렌지닷 업데이트
     *
     * @param headerMap header
     * @param param     파라미터
     * @param callback  Callback
     */
    private void updateOrangeDot(HashMap<String, String> headerMap,
                                 JSONObject param,
                                 Callback<JSONObject> callback) {
        Call<JSONObject> call = apiClient.getApiService().updateOrangeDot(headerMap, param);
        call.enqueue(callback);
    }

    /**
     * 메뉴 조회
     *
     * @param param    파라미터
     * @param callback Callback
     */
    private void requestMenus(JSONObject param,
                              Callback<JSONObject> callback) {
        Call<JSONObject> call = apiClient.getApiService().getMenus(param);
        call.enqueue(callback);
    }

    /**
     * PUSH 토큰 업데이트
     *
     * @param headerMap Header
     * @param param     파라미터
     * @param callback  Callback
     */
    private void updatePushToken(HashMap<String, String> headerMap,
                                 JSONObject param,
                                 Callback<JSONObject> callback) {
        Call<JSONObject> call = apiClient.getApiService().updatePushToken(headerMap, param);
        call.enqueue(callback);
    }

    /**
     * Error 리포트
     *
     * @param headerMap Header
     * @param param     파라미터
     * @throws Exception exception
     */
    private void requestErrorReport(HashMap<String, String> headerMap,
                                    JSONObject param) throws Exception {
        Call<JSONObject> call = apiClient.getApiService().requestErrorReport(headerMap, param);
        call.execute();
    }

    /**
     * make Header
     *
     * @param additional 추가 header
     * @return add HeaderMap
     */
    public HashMap<String, String> getHeaderMap(@Nullable HashMap<String, String> additional) {
        String headerAuthorization = StringUtils.isEmpty(Macadamia.Functions.getCookieValue()) ?
                Macadamia.Constants.STRING_BLANK : String.format("Bearer %s", Macadamia.Functions.getCookieValue());
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Macadamia.Keys.KEY_HEADER_AUTHORIZATION, headerAuthorization);

        if (additional != null && !additional.isEmpty()) {
            headerMap.putAll(additional);
        }

        return headerMap;
    }
}
