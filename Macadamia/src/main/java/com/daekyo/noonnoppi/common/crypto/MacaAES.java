package com.daekyo.noonnoppi.common.crypto;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class MacaAES {

  private static final byte[] IV = {0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E, 0x7F};

  public static String encrypt(String text, String key) throws NoSuchAlgorithmException,
    NoSuchPaddingException,
    InvalidKeyException,
    IllegalBlockSizeException,
    BadPaddingException,
    InvalidAlgorithmParameterException {
    return encrypt(text, key, IV);
  }

  public static String encrypt(String text, String key, byte[] iv) throws NoSuchAlgorithmException,
    NoSuchPaddingException,
    InvalidKeyException,
    IllegalBlockSizeException,
    BadPaddingException,
    InvalidAlgorithmParameterException {
    if (text == null || text.length() == 0) {
      return text;
    }
    byte[] source = text.getBytes(StandardCharsets.UTF_8);
    SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
    AlgorithmParameterSpec IVspec = new IvParameterSpec(iv);
    cipher.init(Cipher.ENCRYPT_MODE, skeySpec, IVspec);
    int mod = source.length % 16;
    if (mod != 0) {
      text = String.format(text + "%" + (16 - mod) + "s", " ");
    }
    return byteArrayToHex(cipher.doFinal(text.getBytes(StandardCharsets.UTF_8)));
  }

  public static String decrypt(String s, String key) throws NoSuchAlgorithmException,
    NoSuchPaddingException,
    InvalidKeyException,
    IllegalBlockSizeException,
    BadPaddingException,
    InvalidAlgorithmParameterException {
    if (s == null || s.length() == 0) {
      return s;
    }

    SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "AES");
    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
    AlgorithmParameterSpec IVspec = new IvParameterSpec(IV);
    cipher.init(Cipher.DECRYPT_MODE, skeySpec, IVspec);
    String decrypted = new String(cipher.doFinal(hexToByteArray(s)), StandardCharsets.UTF_8);
    return decrypted.trim();
  }

  private static byte[] hexToByteArray(String s) {
    byte[] retValue = null;
    if (s != null && s.length() != 0) {
      retValue = new byte[s.length() / 2];
      for (int i = 0; i < retValue.length; i++) {
        retValue[i] = (byte) Integer.parseInt(s.substring(2 * i, 2 * i + 2), 16);
      }
    }
    return retValue;
  }

  private static String byteArrayToHex(byte[] buf) {
    StringBuilder sb = new StringBuilder();
    for (byte b : buf) {
      sb.append(String.format("%02x", b));
    }
    return sb.toString();
  }
}
