# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-dontwarn java.awt.**
-dontwarn javax.swing.**
-dontwarn javax.ws.**
-dontwarn org.conscrypt.**
-dontwarn org.glassfish.**
-dontwarn org.springframework.**

-keep class okhttp3.*
-keep class okio.*
-keep class retrofit2.*
-keep class dagger.*
-keep class butterknife.*
-keep class com.alibaba.fastjson.*
-keep class com.bumptech.*
-keep class com.facebook.*
-keep class com.github.florent37.*
-keep class com.orhanobut.*
-keep class kr.co.nets.*
-keep class org.apache.commons.*
-keep class com.google.*
-keep class android.*
